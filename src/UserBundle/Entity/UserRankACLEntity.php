<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

class UserRankACLEntity extends Entity
{
	protected	$user_id;
	protected	$rank_id;

	const		USER_ID_EMPTY 	= "Utilisateur requis";
	const		RANK_ID_LEN 	= "Rang requis";

	// =================================================================
	// =============================SETTERS=============================

	function setUser_id($pUser_id){
		$this->user_id = $pUser_id;
	}

	function setRank_id($pRank_id){
		$this->rank_id = $pRank_id;
	}

	// =================================================================
	// =============================GETTERS=============================

	function getUser_id(){
		return $this->user_id;
	}
	
	function getRank_id(){
		return $this->rank_id;
	}

}
