<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

use Platypus\Cache\Cache;

class UserEntity extends Entity
{
	protected	$id; // on place l'id en premier (override celui du parent) afin d'y accéder depuis les setters ci-dessous
	protected	$username;
	protected	$email;
	protected	$password;
	protected	$last_ip;
	protected	$registration_date;
	protected	$autologin_key	= null;
	protected	$activation_key	= null;

	const		USERNAME_EMPTY 	= "Le nom d'utilisateur ne peut pas être vide";
	const		USERNAME_LEN 	= "Le nom d'utilisateur doit faire entre 4 et 18 caractères";
	const		PASSWORD_EMPTY 	= "Le mot de passe ne peut pas être vide";
	const		PASSWORD_LEN 	= "Le mot de passe doit faire au moins 6 caractères";
	const		EMAIL_EMPTY 	= "Attention à bien spécifier une addresse mail";
	const		EMAIL_INVALID 	= "L'adresse mail spécifiée n'est pas valide";




	/**
	 * Placer le user dans le cache des users à refresh
	 */
	function needRefreshSession()
	{
		if(!$this->isNew()) {
			$usersToRefreshList = [];
			Cache::read($usersToRefreshList, 'usersToRefreshList', false);
			$usersToRefreshList[$this->id] = null;
			Cache::write($usersToRefreshList, 'usersToRefreshList');
		}
	}

	// =================================================================
	// =============================SETTERS=============================
	
	function setUsername($pUsername){
		if (empty($pUsername) || ctype_space($pUsername)) $this->errors[] = self::USERNAME_EMPTY;
		elseif (!empty($pUsername) && strlen($pUsername) < 4 || strlen($pUsername) > 18) $this->errors[] = self::USERNAME_LEN;

		$this->username = $pUsername;
	}
	
	function setEmail($pEmail){
		if (empty($pEmail) || ctype_space($pEmail)) $this->errors[] = self::MAIL_EMPTY;
		elseif (!filter_var($pEmail, FILTER_VALIDATE_EMAIL)) $this->errors[] = self::EMAIL_INVALID;

		$this->email = $pEmail;
	}
	
	function setPassword($pPassword){
		if ($this->isNew() && (empty($pPassword) || ctype_space($pPassword)) ) $this->errors['PASSWORD_EMPTY'] = self::PASSWORD_EMPTY;
		elseif (!(empty($pPassword) || ctype_space($pPassword)) && strlen($pPassword) < 6) $this->errors['PASSWORD_LEN'] = self::PASSWORD_LEN;

		$this->password = $pPassword;
	}

	function setLast_ip($pLast_ip){
		$this->last_ip = $pLast_ip;
	}
	
	function setRegistration_date($pRegistration_date){
		$this->registration_date = $pRegistration_date;
	}
	
	function setAutologin_key($pAutologin_key){
		$this->autologin_key = $pAutologin_key;
	}
	
	function setActivation_key($pActivation_key){
		$this->activation_key = $pActivation_key;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getUsername(){
		return $this->username;
	}
	
	function getEmail(){
		return $this->email;
	}
	
	function getPassword(){
		return $this->password;
	}

	function getLast_ip(){
		return $this->last_ip;
	}
	
	function getRegistration_date(){
		return $this->registration_date;
	}
	
	function getAutologin_key(){
		return $this->autologin_key;
	}
	
	function getActivation_key(){
		return $this->activation_key;
	}
}
