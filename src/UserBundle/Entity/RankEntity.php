<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

class RankEntity extends Entity
{
	protected	$name;
	protected	$name_tag;
	protected	$own		= null;
	protected	$type		= 1;

	const		NAME_EMPTY 		= "Le rang doit posséder un nom identifiant";
	const		NAMETAG_EMPTY 	= "Le rang doit posséder une dénomination";

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		if (empty($pName) || ctype_space($pName)) $this->errors[] = self::NAME_EMPTY;

		$this->name = $pName;
	}
	
	function setName_tag($pName_tag){
		if (empty($pName_tag) || ctype_space($pName_tag)) $this->errors[] = self::NAMETAG_EMPTY;
		
		$this->name_tag = $pName_tag;
	}
	
	function setOwn($pOwn){
		$this->own = serialize($pOwn);
	}
	
	function setType($pType){
		$this->type = $pType;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getName_tag(){
		return $this->name_tag;
	}
	
	function getOwn(){
		return unserialize($this->own);
	}
	
	function getType(){
		return $this->type;
	}
}
