<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

class BannedIpEntity extends Entity
{
	protected	$ip;
	protected	$reason;
	protected	$until = null;

	// =================================================================
	// =============================SETTERS=============================
	
	function setIp($pIp){
		$this->ip = $pIp;
	}
	
	function setReason($pReason){
		$this->reason = $pReason;
	}
	
	function setUntil($pUntil){
		$this->until = $pUntil;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getIp(){
		return $this->ip;
	}
	
	function getReason(){
		return $this->reason;
	}
	
	function getUntil(){
		return $this->until;
	}
}
