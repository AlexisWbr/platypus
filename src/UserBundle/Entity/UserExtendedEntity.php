<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

class UserExtendedEntity extends Entity
{
	protected	$user_id;
	protected	$firstname;
	protected	$lastname;
	protected	$last_connexion;
	protected	$birthday;
	protected	$twitter_account;

	// =================================================================
	// =============================SETTERS=============================
	
	function setUser_id($pUser_id){
		$this->user_id = $pUser_id;
	}
	
	function setFirstname($pFirstname){
		$this->firstname = $pFirstname;
	}
	
	function setLastname($pLastname){
		$this->lastname = $pLastname;
	}

	function setLast_connexion($pLast_connexion){
		$this->last_connexion = $pLast_connexion;
	}
	
	function setBirthday($pBirthday){
		$this->birthday = $pBirthday;
	}
	
	function setTwitter_account($pTwitter_account){
		$this->twitter_account = $pTwitter_account;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getUser_id(){
		return $this->user_id;
	}
	
	function getFirstname(){
		return $this->firstname;
	}
	
	function getLastname(){
		return $this->lastname;
	}

	function getLast_connexion(){
		return $this->last_connexion;
	}
	
	function getBirthday(){
		return $this->birthday;
	}
	
	function getTwitter_account(){
		return $this->twitter_account;
	}
}
