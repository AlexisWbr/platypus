<?php
namespace src\UserBundle\Entity;
use Platypus\ORM\Entity;

class BanlistEntity extends Entity
{
	protected	$user_id;
	protected	$reason;
	protected	$until = null;

	// =================================================================
	// =============================SETTERS=============================
	
	function setUser_id($pUser_id){
		$this->user_id = $pUser_id;
	}
	
	function setReason($pReason){
		$this->reason = $pReason;
	}
	
	function setUntil($pUntil){
		$this->until = $pUntil;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getUser_id(){
		return $this->user_id;
	}
	
	function getReason(){
		return $this->reason;
	}
	
	function getUntil(){
		return $this->until;
	}
}
