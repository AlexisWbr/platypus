<?php
namespace src\UserBundle\Controller;
use Platypus\Controller\Controller;

trait SaltPwdTrait
{
	/**
	 * Saler le mot de passe d'un User
	 */
	function saltPwd($pPwd)
	{
		return ($pPwd.'%'.strtoupper(substr($pPwd, 0, 2)).'!');
	}


	/**
	 * Hasher mot de passe d'un User
	 */
	function hashPwd($pPwd)
	{
		return (password_hash($pPwd, PASSWORD_BCRYPT, ['cost' => 11]));
	}
}
