<?php
namespace src\UserBundle\Controller;
use Platypus\Controller\Controller;

class UserRanksUtility
{
	/**
	 * Récupérer les rangs compris dans d'autres rangs
	 * 
	 */
	static function getOwnedRanks($pId, $pUserRankACLModel, $pRankModel)
	{
		$userRanks	= $pRankModel->getManyByUserId($pId, 'r.id, r.name, r.own');

		if (!empty($userRanks)) {
			$ranks			= $pRankModel->getAll();
			$ranksArray		= [];
			$userRanksArray	= [];

			foreach ($ranks as $value)
				$ranksArray[$value['id']] = $value;
			foreach ($userRanks as $rank)
				$userRanksArray[] = $rank['id'];

			return (self::recursiveOwnRanks($ranksArray, $userRanksArray));
		} else {
			return null;
		}
	}


	/**
	 * Fonction récursive pour aller chercher les rangs compris dans d'autres rangs
	 * @param $pRanks Array of RankEntity Tous les rangs existants
	 * @param $pUserRanks Array Les rangs détenus par l'utilisateur (rang 'root')
	 * @param $pRanksExhaustive Array Rangs avec rangs 'enfants' /!\ NE PAS REMPLIR
	 * @return $pRanksExhaustive Array Tous les rangs que l'utilisateur possède au final
	 */
	private static function recursiveOwnRanks($pRanks, $pUserRanks, &$pRanksExhaustive = [])
	{
		foreach ($pUserRanks as $key => $value) {
			if (!in_array($pRanks[$value]['name'], $pRanksExhaustive)) {
				$pRanksExhaustive[$pUserRanks[$key]] = $pRanks[$value]['name'];
				if (!is_null($pRanks[$value]['own'])) {
					self::recursiveOwnRanks($pRanks, $pRanks[$value]['own'], $pRanksExhaustive);
				}
			}
		}
		return $pRanksExhaustive;
	}
}
