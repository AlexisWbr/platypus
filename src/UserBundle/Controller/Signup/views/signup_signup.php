<div id="userBundlelogContainer">
	<div class="title">Inscription</div>
		<?php foreach($this->Helper['User']->getFlash('errorSignup') as $errorSignup): ?>
			<span class="errorMsg"><?= $errorSignup ?></span>
		<?php endforeach ?>
		<?php foreach($this->Helper['User']->getFlash('errorSignup') as $errorSignup): ?>
			<span class="successMsg"><?= $errorLogin ?></span>
		<?php endforeach ?>

		<form method="post" action="<?= $this->Helper['Routing']->url('user_signup_signuppost') ?>">
			<label for="username">
				<span>Nom d'utilisateur</span>
				<input type="text" name="username" pattern=".{4,18}" placeholder="4-18 caractères" value="<?= $this->getRequest()->postData('username'); ?>" required >
			</label>
			<label for="password">
				<span>Mot de passe</span>
				<input type="password" name="password" pattern=".{6,}" placeholder="6 caractères minimum" value="<?= $this->getRequest()->postData('password'); ?>" required >
			</label>
			<label for="passwordbis">
				<span>Confirmation du mot de passe</span>
				<input type="password" name="passwordbis" value="<?= $this->getRequest()->postData('passwordbis'); ?>" required >
			</label>
			<label for="email">
				<span>Adresse mail</span>
				<input type="email" name="email" value="<?= $this->getRequest()->postData('email'); ?>" required >
			</label>
			<input type="submit" value="S'inscrire" />
		</form>

		<div class="links">
			<a href="<?= $this->Helper['Routing']->url('user_log_login') ?>">Se connecter</a>
		</div>

	</div>
</div>
