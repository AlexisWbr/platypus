<?php
namespace src\UserBundle\Controller\Signup;
// use app\component\Mailing\PHPMail;
use app\component\Mailing\MandrillMail;

class SignupMailing extends MandrillMail
{
	/**
	 * Envoi d'un mot de passe par mail pour confirmer l'inscription d'un membre
	 */
	function activateAccount($pUsername, $pEmail, Array $pLinks)
	{
		$messageContent = "
		<style type=\"text/css\">
		body{
			font-family: 'Arial', sans-serif;
			font-size: 15px;
		}
		</style>

		<html><body><p>Bonjour $pUsername, merci pour votre inscription.<br />
		Votre compte a bien été créé sur ".$_SERVER['HTTP_HOST'].". Celui-ci est pour le moment innactif, une fois activé, vous pourrez vous y connecter .<br />
		Pour ce faire, cliquez simplement sur le lien suivant. Attention, ce lien n'est valide que durant 7 jours.</p>

		<p><a href=\"".$pLinks['root'].$pLinks['activateAccount']."\" title=\"Activer mon compte\">Activer mon compte maintenant</a></p>
		
		<p><br /><br />Si vous n'avez jamais demandé à créer un compte, ignorez simplement ce mail.</p>
		</body></html>";

		$this->to(["" => $pEmail])
			->subject($_SERVER['HTTP_HOST']." - Activation de votre compte")
			->message($messageContent);

		$this->send();
	}
}
