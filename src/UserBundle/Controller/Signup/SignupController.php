<?php
namespace src\UserBundle\Controller\Signup;
use Platypus\Controller\Controller;

use src\UserBundle\Controller\SaltPwdTrait;
use src\UserBundle\Entity\UserEntity;
use Symfony\Component\Yaml\Parser;

class SignupController extends Controller
{
	use SaltPwdTrait;

	function before()
	{
		// si déjà authentifié redirection vers l'accueil
		if ($this->getSession()->isAuthenticated())
			$this->getResponse()->redirect($this->getRequest()->root());
	}


	/**
	 * GET - Formulaire d'inscription d'un utilisateur
	 */
	function signupAction() {}


	/**
	 * POST - Inscription d'un utilisateur
	 */
	function signuppostAction()
	{
		$yamlParser		= new Parser();
		$userModel		= $this->getModel('User');
		$bannedIpModel	= $this->getModel('BannedIp');

		$newUser				= new UserEntity($_POST, true);
		$config					= $yamlParser->parse(file_get_contents(__DIR__.'/../../Ressources/config/config.yaml'));
		$bannedIpList			= $bannedIpModel->getOneByIp($this->getRequest()->getIp(), 'ip', 'AND (until > NOW() || until IS NULL)');
		$usernameAlreadyUsed	= $config['preventMultipleUsername'] ? $userModel->getOneByUsername($newUser['username']) : false;
		$emailAlreadyUsed		= $config['preventMultipleAccount'] ? $userModel->getOneByEmail($newUser['email']) : false;

		if ($newUser->isValid())
		{
			// si adresse IP non bannie
			if (!$bannedIpList) {
			// si username libre
			if ($this->isAccountFree($usernameAlreadyUsed, $config['accountValidationMaxTime'])) {
			// si adresse mail pas déjà utilisée pour un compte
			if ($this->isAccountFree($emailAlreadyUsed, $config['accountValidationMaxTime'])) {
			// si le username respecte bien les caractères autorisés
			if (preg_match($config['usernameCharactersAllowed'], $newUser['username'])) {
			// si adresse mail non bannie
			if (!$this->isEmailBanned($yamlParser->parse(file_get_contents(__DIR__.'/../../Ressources/config/bannedMails.yaml')), $_POST['email'])) {
			// si les deux pwd correspondent
			if ($newUser['password'] == $this->getRequest()->postData('passwordbis'))
			{
				$newUser->setLast_ip($this->getRequest()->getIp());
				// on génère une clef unique pour valider le compte
				$newUser->setActivation_key(md5($this->Tool['Text']->generateRandomString('64', '+*#!%').$newUser['username']));
				$newUser->setPassword($this->hashPwd($this->saltPwd($newUser['password'])));

				// on enregistre le nouvel user dans la bdd
				$userModel->insertUser($newUser);

				// on envoie l'email de confirmation
				$logMailing = new SignupMailing;
				$logMailing->activateAccount($newUser['username'], $newUser['email'], ['root' => $this->getRequest()->root(), 'activateAccount' => $this->getRouter()->generatePath('user_signup_activateAccount', ['key' => $newUser['activation_key']])]);

				$_SESSION['user']['waitingAccountActivation'] = $newUser['activation_key'];

				$this->Tool['User']->setFlash("Merci pour votre inscription ! Un email de confirmation vous a été envoyé.<br /> Vous pourrez vous connecter une fois cette étape validée.", 'successLogin');
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
			}
			else $this->Tool['User']->setFlash('Les deux mots de passe ne correspondent pas', 'errorSignup'); }
			else $this->Tool['User']->setFlash('Adresse mail invalide', 'errorSignup'); }
			else $this->Tool['User']->setFlash('Seuls les caractères alpha-numériques, et les caractères spéciaux: ()\'- _ sont autorisés pour le nom d\'utilisateur', 'errorSignup'); }
			else $this->Tool['User']->setFlash('Cette adresse mail est déjà utilisée. Le multi-compte est interdit.', 'errorSignup'); }
			else $this->Tool['User']->setFlash('Ce nom d\'utilisateur est déjà existant', 'errorSignup'); }
			else {
				$this->Tool['User']->setFlash('Votre adresse IP est bannie, inscription impossible.<br /> Si vous pensez qu\'il sagit d\'une erreur, contactez l\'administrateur de ce site.', 'error');
				$this->getResponse()->redirect($this->getRequest()->root());
			}
		}
		else 
			foreach ($newUser->getErrors() as $key => $value)
				$this->Tool['User']->setFlash($value, 'errorSignup');

		$this->setView('signup');
	}


	/**
	 * Activer le compte d'un User
	 */
	function activateAccountAction()
	{
		$userModel		= $this->getModel('User');
		$userActivated	= $userModel->activateUserAccount($_GET['key']);

		if ($userActivated) {
			unset($_SESSION['user']['waitingAccountActivation']);
			$this->Tool['User']->setFlash("Votre compte a bien été validé.<br /> Vous pouvez dès à présent vous connecter", 'successLogin');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
		} else
			$this->getResponse()->redirect($this->getRequest()->root());
	}


	/**
	 * Renvoyer le mail d'actication
	 */
	function resendAccountActivationMailAction()
	{
		if (isset($_SESSION['user']['waitingAccountActivation'])) {
			$userModel	= $this->getModel('User');
			$newUser	= $userModel->getOneByActivationKey($_SESSION['user']['waitingAccountActivation']);

			if ($newUser) {
				$logMailing = new SignupMailing;
				$logMailing->activateAccount($newUser['username'], $newUser['email'], ['root' => $this->getRequest()->root(), 'activateAccount' => $this->getRouter()->generatePath('user_signup_activateAccount', ['key' => $newUser['activation_key']])]);
				$this->Tool['User']->setFlash("Merci pour votre inscription ! Un email de confirmation vous a été envoyé.<br /> Vous pourrez vous connecter une fois cette étape validée.", 'successLogin');
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
			} else
				$this->getResponse()->redirect($this->getRequest()->root());
		}
	}


	// =================================================================


	/**
	 * Vérifie qu'une adresse mail ne soit pas bannie (email jetables par exemple)
	 */
	function isEmailBanned($pMailsList, $pMail)
	{
		$explodedEmail = explode('@', $pMail);
		// si pas de 2nd retour c'est que le mail ne possède même pas d'arobase. Donc adresse email bidon
		if (!isset($explodedEmail[1])) return false;
		// parcours le tableau pour savoir si mail banni
		return in_array($explodedEmail[1], $pMailsList['mails']);
	}


	/**
	 * Vérifier qu'un username ou un mail ne soient pas déjà utilisés
	 * Si oui mais que le compte existant n'a pas été validé dans les temps, supprime le vieux compte
	 */
	function isAccountFree($pAccount, $pAccountValidationMaxTime)
	{
		// si libre ou que compte périmé alors ok
		if ( !$pAccount || (!is_null($pAccount['activation_key']) && strtotime($pAccount['registration_date'])+$pAccountValidationMaxTime < time()) ) {
			// si compte déjà existant mais périmé
			if ($pAccount) {
				$userModel = $this->getModel('User');
				$userModel->delAccountById($pAccount['id']);
			}
			return true;
		}
		else return false;
	}
}
