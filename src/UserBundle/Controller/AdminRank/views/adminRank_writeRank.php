<h1>Gestion des rangs</h1>
<div class="containerMain">
	<?php if($oldRank['id']) : ?>
		<h2>Modifier le rang <?= $oldRank['name_tag'] ?></h2>
	<?php else : ?>
		<h2>Ajouter un rang</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('rank_adminRank_index') ?>" class="backLink">Retour</a>

	<div class="containerSimple u-container-fluid">
		<div class="u-row">
			<div class="col-lg-24">
				<form class="u-form formClassic" method="post" action="<?= $this->Helper['Routing']->url('rank_adminRank_addRankToUser') ?>">
						<label class="u-inline" for="username">
							<span class="text-right">Ajouter un utilisateur</span>
							<input type="text" id="username" name="username" placeholder="nom du compte utilisateur" required />
						</label>

						<input type="hidden" name="id" value="<?= $oldRank['id'] ?>" >
						<button type="submit" class="btnColor1 btnSmall"><i class="fa fa-plus-circle"></i></button>
				</form>
				<br />
				<br />

				<table class="table-autoscroll tableClassic" style="width: 100%">
					<thead>
						<tr>
							<th colspan="3">Liste des utilisateurs assignés à ce rang</th>
						</tr>
						<tr>
							<th class="text-left">ID</th>
							<th class="text-left">Compte utilisateur</th>
							<th>Supprimer</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($userList as $user) : ?>
							<tr>
								<td style="width: 10%">
									<?= $user['id'] ?>
								</td>
								<td style="width: 90%">
									<?= $user['username'] ?>
								</td>
								<td class="text-center">
									<a href="<?= $this->Helper['Routing']->url('rank_adminRank_removeRankToUser', ['idRank' => $_GET['id'], 'id' => $user['id']]) ?>" title="Retirer le rang à cet utilisateur"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						<?php endforeach?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
