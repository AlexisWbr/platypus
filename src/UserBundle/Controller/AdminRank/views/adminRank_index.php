<h1>Gestion des rangs</h1>

<div class="containerMain">
	<h2>Liste des rangs</h2>
	<?php if(empty($rankList)) : ?>
		<div class="blockDashed">Aucun rang</div>
	<?php else : ?>
			<div>
				<table class="table-autoscroll tableClassic" style="width: 100%">
					<thead>
						<tr>
							<th class="text-left">Rang</th>
							<th class="text-left">Identifiant</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($rankList as $rank) : ?>
							<tr>
								<td style="width: 30%">
									<a href="<?= $this->Helper['Routing']->url('rank_adminRank_writeRank',['id'=>$rank['id']]) ?>" title="Modifier le rang"><?= $rank['name_tag'] ?></a>
								</td>
								<td style="width: 70%">
									<?= $rank['name'] ?>
								</td>
							</tr>
						<?php endforeach?>
					</tbody>
				</table>
			</div>
	<?php endif ?>
</div>
