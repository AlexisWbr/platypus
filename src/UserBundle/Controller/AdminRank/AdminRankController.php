<?php
namespace src\UserBundle\Controller\AdminRank;
use Platypus\Controller\Controller;

use src\UserBundle\Entity\UserRankACLEntity;

class AdminRankController extends Controller
{
	function indexAction()
	{
		$rankModel	= $this->getModel('Rank');

		$rankList	= $rankModel->getAll();

		$this->addToPage('rankList', $rankList);
	}


	function writeRankAction()
	{
		$userModel	= $this->getModel('User');
		$rankModel	= $this->getModel('Rank');
		
		// récupère le rang en édition
		$oldRank	= $rankModel->getOneById($_GET['id']);
		// récupère les utilisateurs possédant ce rang
		$pgto		= $this->Tool['Paginate']->paginate(30, $userModel->count('*'), $this->getRequest()->getData('p'));
		$userList	= $userModel->getManyByRankIdWithLimit($oldRank['id'], $pgto['entitiesPerPage'], $pgto['entryBegin'], 'u.id, u.username');

		$this->addToPage('oldRank', $oldRank);
		$this->addToPage('userList', $userList);
	}


	function addRankToUserAction()
	{
		$userModel	 		= $this->getModel('User');
		$userRankACLModel	= $this->getModel('UserRankACL');

		$user		 = $userModel->getOneByUsername($this->getRequest()->postData('username'), 'id');
		$rankIdToAdd = $this->getRequest()->postData('id');

		if ($user && $rankIdToAdd) {
			$userRankACLEntity = new UserRankACLEntity(['user_id'=> $user['id'], 'rank_id'=> $rankIdToAdd]);
			$userRankACLModel->insertOne($userRankACLEntity);
			$this->Tool['User']->setFlash("Rang assigné à l'utilisateur", 'success');
		}
		else {
			$this->Tool['User']->setFlash("Aucun utilisateur de ce nom", 'error');
		}

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('rank_adminRank_writeRank', ['id' => $_POST['id']]));
	}


	function removeRankToUserAction()
	{
		$userModel	 		= $this->getModel('User');
		$userRankACLModel	= $this->getModel('userRankACL');
		$user		 		= $userModel->getOneById($_GET['id'], 'id');

		if ($user) {
			$userRankACLModel->deleteOneByUserIdAndRankId($user['id'], $_GET['idRank']);
			$this->Tool['User']->setFlash("Rang retiré à cet utilisateur", 'success');
		}
		else {
			$this->Tool['User']->setFlash("Cet utilisateur n'existe pas", 'error');
		}

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('rank_adminRank_writeRank', ['id' => $_GET['idRank']]));
	}
}
