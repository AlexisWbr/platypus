<?php
namespace src\UserBundle\Controller\AdminUser;
use Platypus\Controller\Controller;

use Platypus\Cache\Cache;
use Symfony\Component\Yaml\Parser;
use src\UserBundle\Controller\SaltPwdTrait;
use src\UserBundle\Entity\UserEntity;

class AdminUserController extends Controller
{
	use SaltPwdTrait;

	function indexAction()
	{
		$userModel	= $this->getModel('User');

		$pgto		= $this->Tool['Paginate']->paginate(30, $userModel->count('*'), $this->getRequest()->getData('p'));
		$userList	= $userModel->getManyWithLimit($pgto['entitiesPerPage'], $pgto['entryBegin'], '*');

		$this->addToPage('userList', $userList);
	}


	function writeUserAction()
	{
		$userModel	= $this->getModel('User');
		$rankModel	= $this->getModel('Rank');

		// récupère un potentiel user en édition
		$oldUser = $userModel->getOneById($this->getRequest()->getData('id'));
		// récupérer ses rangs
		if ($oldUser)
			$oldUser->ranks = $rankModel->getManyByUserId($oldUser['id']) ?? null;

		$this->addToPage('oldUser', $oldUser);
	}


	function writeUserPOSTAction()
	{
		$yamlParser	= new Parser();
		$userModel	= $this->getModel('User');

		$user					= new UserEntity($_POST, true);
		$config					= $yamlParser->parse(file_get_contents(__DIR__.'/../../Ressources/config/config.yaml'));
		$usernameAlreadyUsed	= $config['preventMultipleUsername'] ? $userModel->getOneByUsername($user['username'], 'id') : false;
		$emailAlreadyUsed		= $config['preventMultipleAccount'] ? $userModel->getOneByEmail($user['email'], 'id') : false;
		$user->setActivation_key($user['activation_key'] ? null : 'desactivated');

		if ($user->isValid())
		{
			// si username libre
			if (!$usernameAlreadyUsed || $usernameAlreadyUsed['id'] == $user['id']) {
			// si adresse mail pas déjà utilisée pour un compte
			if (!$emailAlreadyUsed || $emailAlreadyUsed['id'] == $user['id'])
			{
				if ($user->isNew())
				{
					$user->setLast_ip($this->getRequest()->getIp());
					$user->setPassword($this->hashPwd($this->saltPwd($user['password'])));

					$userModel->insertUser($user);
					$this->Tool['User']->setFlash("Utilisateur créé", 'success');
				}
				else
				{
					$userModel->inAdminUpdateUser($user);
					if($user['password'] != '')
						$userModel->updatePasswordById($this->hashPwd($this->saltPwd($user['password'])), $user['id']);
					$this->Tool['User']->setFlash("Utilisateur modifié", 'success');

					// on demande au système de mettre à jour l'utilisateur
					$user->needRefreshSession();
				}

				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_adminUser_index'));
			}
			else $this->Tool['User']->setFlash('Cette adresse mail est déjà utilisée. Le multi-compte est interdit.', 'error'); }
			else $this->Tool['User']->setFlash('Ce nom d\'utilisateur est déjà existant', 'error');
		}
		else
		{
			foreach ($user->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
		}

		$this->addToPage('oldUser', $user);
		$this->setView('writeUser');
	}
}
