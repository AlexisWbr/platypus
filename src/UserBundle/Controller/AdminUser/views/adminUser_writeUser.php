<h1>Gestion des utilisateurs</h1>
<div class="containerMain">
	<?php if($oldUser['id']) : ?>
		<h2>Modifier un utilisateur</h2>
	<?php else : ?>
		<h2>Ajouter un utilisateur</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('user_adminUser_index') ?>" class="backLink">Retour</a>

	<form class="u-form formClassic" method="post" action="">
		<div class="containerSimple u-container-fluid">
			<div class="u-row">
				<div class="col-lg-17">
					<div class="u-onGrid grid-w30">
						<label class="u-w100" for="username">
							<span class="text-right">Nom du compte utilisateur *</span>
							<input type="text" id="username" name="username" value="<?= $oldUser['username'] ?>" maxlength="24" placeholder="username" required />
						</label>
						<label class="u-w100" for="email">
							<span class="text-right">Adresse mail *</span>
							<input type="email" id="email" name="email" value="<?= $oldUser['email'] ?>" maxlength="44" placeholder="adresse mail" required />
						</label>
						<label class="u-w100" for="password">
							<span class="text-right">Mot de passe <?= !$this->getRequest()->getData('id') ? '*':'' ?></span>
							<input type="password" id="password" name="password" maxlength="44" placeholder="laisser vide pour ne pas le modifier" <?= !$this->getRequest()->getData('id') ? 'required':'' ?> autocomplete="new-password" />
						</label>
						<label for="activation_key">
							<span class="text-right">Compte activé</span>
							<label for="activation_key">
								<input type="checkbox" name="activation_key" value="1" id="activation_key" <?= !$oldUser['activation_key'] ? 'checked' : '' ?> >
							</label>
						</label>
						<?php if($oldUser['id']) : ?>
							<br />
							<br />
							<label>
								<span class="text-right">Rangs attribués : </span>
								<?php if ($oldUser->ranks) : ?>
									<?php foreach($oldUser->ranks as $rank) : ?>
										<?= $rank['name_tag'] ?> /
									<?php endforeach ?>
								<?php else : ?>
									Aucun
								<?php endif ?>
							</label>
						<?php endif ?>
						<br />
						<br />

						<input type="hidden" name="id" value="<?= $oldUser['id'] ?>" >
						<input type="submit" class="btnColor1 btnMedium" value="Enregistrer">
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
