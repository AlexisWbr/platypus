<h1>Gestion des utilisateurs</h1>

<div class="containerMain">
	<h2>Liste des utilisateurs</h2>
	<?php if(empty($userList)) : ?>
		<div class="blockDashed">Aucun utilisateur... Mais... Comment pouvez-vous être ici en ce cas ?</div>
	<?php else : ?>
			<div>
				<table class="table-autoscroll tableClassic">
					<thead>
						<tr>
							<th class="text-left">Compte utilisateur</th>
							<th class="text-left">Mail</th>
							<th><span class="u-help" title="Dernière adresse IP utilisée par l'utilisateur après connexion">Adresse IP</span></th>
							<th>Compte activé</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($userList as $user) : ?>
							<tr>
								<td style="width: 20%">
									<a href="<?= $this->Helper['Routing']->url('user_adminUser_writeUser',['id'=>'-'.$user['id']]) ?>"><?= $user['username'] ?></a>
								</td>
								<td style="width: 30%">
									<?= $user['email'] ?>
								</td>
								<td class="text-center" style="width: 30%">
									<?= $user['last_ip'] ?>
								</td>
								<td class="text-center" style="width: 20%">
									<?php if(!$user['activation_key']) : ?>
										<span class="greenMark"><i class="fa fa-check"></i> oui</span>
									<?php else : ?>
										<span class="redMark"><i class="fa fa-times"></i> non</span>
									<?php endif ?>
								</td>
								<td></td>
							</tr>
						<?php endforeach?>
					</tbody>
				</table>
			</div>
	<?php endif ?>

	<a href="<?= $this->Helper['Routing']->url('user_adminUser_writeUser') ?>" class="btnColor1 btnSmall">
		<i class="fa fa-plus-circle"></i> Ajouter un utilisateur
	</a>

	<div class="pagination">
		<?= $this->cell('Pagination', array($currentPage, $totalPage, $this->Helper['Routing']->url('user_adminUser_index'), array('<<', '<', '>', '>>'))) ?>
	</div>
</div>
