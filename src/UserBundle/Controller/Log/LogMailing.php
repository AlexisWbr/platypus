<?php
namespace src\UserBundle\Controller\Log;
// use app\component\Mailing\PHPMail;
use app\component\Mailing\MandrillMail;

class LogMailing extends MandrillMail
{
	/**
	 * Envoyer un nouveau mot de passe par mail
	 */
	function newPwd($pNewPwd, $pEmail, Array $pLinks)
	{
		$messageContent = "
		<style type=\"text/css\">
		body{
			font-family: 'Arial', sans-serif;
			font-size: 15px;
		}
		</style>

		<html><body><p>Une demande de réinitialisation du mot de passe de votre compte a été formulée. Pour réinitialiser votre mot de passe, suivez les instructions ci-dessous.<br />
		Si vous recevez ce message sans avoir fait cette demande de réinitialisation, il est possible qu'un autre utilisateur ait saisi votre adresse mail par erreur.
		Dans ce cas supprimez simplement ce mail, aucune modification de votre compte ne sera effectuée.</p>

		<p>Votre nouveau de mot de passe: $pNewPwd</p>
		<p>Cliquez sur le lien suivant pour confirmer la réinitialisation:</p>
		<p><a href=\"".$pLinks['root'].$pLinks['activateNewPwd']."\" title=\"Réinitialiser mon mot de passe\">".$pLinks['root'].$pLinks['activateNewPwd']."</a></p>
		</body></html>";

		$this->to(["" => $pEmail])
			->subject($_SERVER['HTTP_HOST']." - Réinitialisation de votre mot de passe")
			->message($messageContent);

		$this->send();
	}
}
