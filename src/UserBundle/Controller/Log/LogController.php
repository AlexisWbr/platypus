<?php
namespace src\UserBundle\Controller\Log;
use Platypus\Controller\Controller;

use src\UserBundle\Controller\SaltPwdTrait;
use src\UserBundle\Controller\UserRanksUtility;
use Symfony\Component\Yaml\Parser;

class LogController extends Controller
{
	use SaltPwdTrait;
	static $autologinCookieSalt = 'fsd5!!5FXs1';





	/**
	 * GET - Formulaire de connexion d'un utilisateur
	 */
	function loginAction()
	{
		// si déjà authentifié redirection vers l'accueil
		if($this->getSession()->isAuthenticated())
			$this->getResponse()->redirect($this->getRequest()->root());
	}

	/**
	 * POST - Connexion d'un utilisateur
	 */
	function loginpostAction()
	{
		// si déjà authentifié redirection vers l'accueil
		if($this->getSession()->isAuthenticated())
			$this->getResponse()->redirect($this->getRequest()->root());

		$yamlParser	= new Parser();
		$config		= $yamlParser->parse(file_get_contents(__DIR__.'/../../Ressources/config/config.yaml'));

		$userModel	= $this->getModel('Log');
		$connexion	= $userModel->loginByForm($_POST['username'], $this->saltPwd($_POST['password']), $config['connectPwdOrEmail']);

		self::executeLogin($connexion, $this->getRequest()->postData('autologin'));
	}


	/**
	 * Déconnecter l'utilisateur
	 */
	function logoutAction()
	{
		// si pas déjà authentifié redirection vers l'accueil
		if(!$this->getSession()->isAuthenticated()) 
			$this->getResponse()->redirect($this->getRequest()->root());

		session_unset();
		$this->getResponse()->setcookie('atlg');
		$this->getResponse()->redirect($this->getRequest()->referer());
	}


	/**
	 * GET - Envoyer une nouveau mot de passe par mail
	 */
	function generateNewPwdAction(){}


	/**
	 * POST - Envoyer une nouveau mot de passe par mail
	 */
	function generateNewPwdpostAction()
	{
		// si les champs sont valides
		if (!empty($_POST['username']) && !empty($_POST['email']))
		{
			$userModel = $this->getModel('User');

			if ($user = $userModel->getOneByUserNameAndEmail($_POST['username'], $_POST['email'], 'id, username, email')) {
				// nouveau mot de passe en clair
				$newPwd		= $this->Tool['Text']->generateRandomString('10', '+*!%');
				// nouveau mot de passe hashé
				$newPwdHash	= $this->hashPwd($this->saltPwd($newPwd));
				// clef de sécurité
				$newPwdKey	= $this->Tool['Text']->generateRandomString('64');

				$_SESSION['user']['newPwd']['pwd']		= $newPwdHash;
				$_SESSION['user']['newPwd']['key']		= $newPwdKey;
				$_SESSION['user']['newPwd']['idUser']	= $user['id'];

				$logMailing = new LogMailing;
				$logMailing->newPwd($newPwd, $user['email'], ['root' => $this->getRequest()->root(), 'activateNewPwd' => $this->getRouter()->generatePath('user_log_activateNewPwd', ['key' => $newPwdKey])]);

				$this->Tool['User']->setFlash('Un nouveau mot de passe vous a été envoyé par mail', 'successLogin');
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
			} else {
				$this->Tool['User']->setFlash("Le nom d'utilisateur et l'adresse mail ne correspondent pas", 'errorLogin');
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_generateNewPwd'));
			}
		} else {
			$this->Tool['User']->setFlash('Un des champs est manquant', 'errorLogin');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_generateNewPwd'));
		}
	}


	/**
	 * Modifier un ancien mot de passe oublié par un nouveau envoyé par mail
	 */
	function activateNewPwdAction()
	{
		if ($_GET['key'] == $_SESSION['user']['newPwd']['key']) {
			$userModel = $this->getModel('User');
			$userModel->updatePasswordById($_SESSION['user']['newPwd']['pwd'], $_SESSION['user']['newPwd']['idUser']);

			session_unset('user');

			$this->Tool['User']->setFlash("Votre mot de passe a bien été modifié", 'successLogin');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
		} else {
			$this->Tool['User']->setFlash("Impossible de changer le mot de passe. Clef de sécurité erronée ou périmée.<br /> Gardez bien votre navigateur ouvert le temps de procéder à la réinitialisation du mot de passe.", 'errorLogin');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_generateNewPwd'));
		}
	}


	// =================================================================


	/**
	 * S'occupe de oui ou non créer la session de log si cette connexion est réussie par l'internaute
	 * @param Array $pConnexion Retour de la requête SQL. Égal à false si erreur lors de la connexion
	 * @param $pAutologinCookie Si la varriable existe c'est que la coche pour retenir la connexion en cookie a été cochée
	 */
	function executeLogin($pConnexion, $pAutologinCookie = null)
	{
		if ($pConnexion != false)
		{
			$bannedIpModel		= $this->getModel('BannedIp');
			$bannedlistModel	= $this->getModel('Banlist');
			$bannedIpList		= $bannedIpModel->getOneByIp($this->getRequest()->getIp(), 'ip', 'AND (until > NOW() || until IS NULL)');
			$banlist			= $bannedlistModel->getOneById($pConnexion['id'], 'user_id', 'AND (until > NOW() || until IS NULL)');

			// si adresse IP bannie
			if ($bannedIpList) {
				$this->Tool['User']->setFlash("Votre adresse IP est bannie, connexion impossible.<br />
				Si vous pensez qu'il sagit d'une erreur, contactez l'administrateur de ce site.", 'error');
			}
			// si membre banni
			elseif ($banlist) {
				$this->Tool['User']->setFlash("Votre compte a été banni, connexion impossible", 'error');
			}
			// si connexion autorisée
			else {
				$userModel			= $this->getModel('User');
				$userRankACLModel	= $this->getModel('UserRankACL');
				$rankModel			= $this->getModel('Rank');
				// va chercher l'intégralité des rangs hérités
				$pConnexion->ranks	= UserRanksUtility::getOwnedRanks($pConnexion['id'], $userRankACLModel, $rankModel);
				// met à jour si besoin l'adresse IP (utilisée à titre informatif, la vraie IP utilisée pour la sécurité est l'IP courante du client)
				if ($this->getRequest()->getIp() != $pConnexion['last_ip'])
					$userModel->updateLastIpById($this->getRequest()->getIp(), $pConnexion['id']);

				self::createLoginSession($pConnexion);

				// si reçoit par le fomulaire une demande de connexion automatique
				// on génère un token basé sur le nom de l'utilisateur + time + salage qui sera enregistré en bdd et comparé si connexion via ce cookie
				// lorsque l'on se connecte via le cookie, le salage est ajouté. Ainsi si la bdd a été volée et qu'un token est utilisé, celui-ci sera doublement salé et il sera impossible de se connecter
				if (!is_null($pAutologinCookie)) {
					// on créé le cookie sans salage
					$autologinKey_cookie	= md5(strtoupper($pConnexion['username']).time());
					// on stock le cookie en bdd avec le salage
					$autologinKey_bdd		= md5(md5(strtoupper($pConnexion['username']).time()).self::$autologinCookieSalt);

					$this->getResponse()->setcookie('atlg', $autologinKey_cookie, 604800, '/');

					$modelLog = $this->getModel('Log');
					$modelLog->updateAutologKeyById($autologinKey_bdd, $pConnexion['id']);
				}
			}
		} else {
			$this->Tool['User']->setFlash('Echec lors de la connexion', 'errorLogin');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('user_log_login'));
		}

		// si jamais redirigé sur le formulaire de connexion depuis le firewall
		if (isset($_SESSION['referer']))
			$this->getResponse()->redirect($_SESSION['referer']);
		// si jamais atteint le formulaire de connexion depuis un lien quelconque
		elseif (isset($_POST['referer']))
			$this->getResponse()->redirect($_POST['referer']);
		else
			$this->getResponse()->redirect($this->getRequest()->root());
	}


	/**
	 * Génère la session de log liée à l'internaute
	 * Fonction en static car aussi utilisée dans le MainController pour l'autologin
	 * @param Array $pConnexion Retour de la requête SQL. Égal à false si erreur lors de la connexion
	 */
	static function createLoginSession($pConnexion)
	{
		$_SESSION['user']['id'] = $pConnexion['id'];
		$_SESSION['user']['ranks'] = is_array($pConnexion->ranks) ? $pConnexion->ranks : [];
	}
}
