<div id="userBundlelogContainer">
	<div class="title">Connexion</div>

	<?php foreach($this->Helper['User']->getFlash('errorLogin') as $errorLogin): ?>
		<span class="errorMsg"><?= $errorLogin ?></span>
	<?php endforeach ?>
	<?php foreach($this->Helper['User']->getFlash('successLogin') as $successLogin): ?>
		<span class="successMsg"><?= $successLogin ?></span>
	<?php endforeach ?>

	<form method="POST" action="<?= $this->Helper['Routing']->url('user_log_loginpost') ?>">
		<label for="username">Identifiant</label>
		<br />
		<input type="text" id="username" name="username" placeholder="login" required >
		<br />
		<label for="password">Mot de passe</label>
		<br />
		<input type="password" id="password" name="password" placeholder="mot de passe" required >
		<br />
		<label for="autologin">
			<input type="checkbox" name="autologin" value="true" id="autologin" checked>Se souvenir de moi
		</label>
		<br />
		<input type="hidden" name="referer" value="<?= $this->getRequest()->referer() ?>" >
		<input type="submit" value="Connexion">
	</form>

	<?php if(isset($_SESSION['user']['waitingAccountActivation'])): ?>
		<span class="infoMsg"><a href="<?= $this->Helper['Routing']->url('user_signup_resendAccountActivationMail') ?>">Renvoyer un mail d'activation</a></span>
	<?php endif ?>
	<div class="links">
		<a href="<?= $this->Helper['Routing']->url('user_log_generateNewPwd') ?>">J'ai oublié mon mot de passe</a>
		- <a href="<?= $this->Helper['Routing']->url('user_signup_signup') ?>">S'inscrire</a>
	</div>
</div>
