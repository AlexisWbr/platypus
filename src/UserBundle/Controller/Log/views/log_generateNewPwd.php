<div id="userBundlelogContainer">
	<div class="title">Renvoyer un nouveau mot de passe</div>

		<form method="post" action="<?= $this->Helper['Routing']->url('user_log_generateNewPwd') ?>">
			<label for="username">
				<span>Nom d'utilisateur</span>
				<input type="text" name="username" required >
			</label>
			<label for="email">
				<span>Adresse mail</span>
				<input type="email" name="email" required >
			</label>
			<input type="submit" value="Envoyer" />
		</form>
		<div class="links">
			<a href="<?= $this->Helper['Routing']->url('user_log_login') ?>">Se connecter</a>
		</div>

	</div>
</div>
