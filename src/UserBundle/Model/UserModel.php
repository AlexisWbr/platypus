<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

use src\UserBundle\Entity\UserEntity;

class UserModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM user $pFilter", 'src\UserBundle\Entity\UserEntity');
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM user $pFilter");
	}


	function countByUsername($pUsername, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM user WHERE username = :username $pFilter", ['username' => [$pUsername, 'str']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	function getOneByUsername($pUsername, $pFields ='*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user WHERE username = :username $pFilter";
		$varsToBind = ['username' => [$pUsername, 'str']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	function getOneByEmail($pEmail, $pFields ='*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user WHERE email = :email $pFilter";
		$varsToBind = ['email' => [$pEmail, 'str']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	function getOneByUserNameAndEmail($pUsername, $pEmail, $pFields ='*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user WHERE username = :username AND email = :email $pFilter";
		$varsToBind = ['username' => [$pUsername, 'str'], 'email' => [$pEmail, 'str']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	function getOneByActivationKey($pActivationKey, $pFields ='*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user WHERE activation_key = :activation_key $pFilter";
		$varsToBind = ['activation_key' => [$pActivationKey, 'str']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	/**
	 * Récupérer une liste de tant entités à partir d'un endroit donné
	 */
	function getManyWithLimit($pEntitiesPerPage, $pEntryBegin, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user $pFilter LIMIT :nbrPerPage OFFSET :entryBegin";
		$varsToBind = ['nbrPerPage' => [$pEntitiesPerPage, 'int'], 'entryBegin' => [$pEntryBegin, 'int']];
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	/**
	 * Récupérer les membres assignés à un rang selon son id (utilise la table user_rank_acl)
	 */
	function getManyByRankIdWithLimit($pRankId, $pEntitiesPerPage, $pEntryBegin, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user u LEFT JOIN user_rank_acl acl ON u.id = acl.user_id WHERE acl.rank_id = :rank_id $pFilter LIMIT :nbrPerPage OFFSET :entryBegin";
		$varsToBind = ['rank_id' => [$pRankId, 'int'], 'nbrPerPage' => [$pEntitiesPerPage, 'int'], 'entryBegin' => [$pEntryBegin, 'int']];
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\UserEntity', $varsToBind);
	}


	/**
	 * Ajouter un utilisateur
	 */
	function insertUser(UserEntity $pEntity)
	{
		$reqsql = "INSERT INTO user SET username = :username, email = :email, password = :password, last_ip = :last_ip, registration_date = NOW(), activation_key = :activation_key";
		$varsToBind = [
						'username'			=> [$pEntity['username'], 'str'],
						'password'			=> [$pEntity['password'], 'str'],
						'last_ip'			=> [$pEntity['last_ip'], 'str'],
						'email'				=> [$pEntity['email'], 'str'],
						'activation_key'	=> [$pEntity['activation_key'], 'str']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	/**
	 * Mettre à jour depuis le pannel admin
	 */
	function inAdminUpdateUser(UserEntity $pEntity)
	{
		$reqsql = "UPDATE user SET	username		= :username,
									email			= :email,
									activation_key	= :activation_key
							WHERE	id = :id";
		$varsToBind = [
			'username'			=> [$pEntity['username'], 'str'],
			'email'				=> [$pEntity['email'], 'str'],
			'activation_key'	=> [$pEntity['activation_key'], 'str'],
			'id'				=> [$pEntity['id'], 'int']
		];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updatePasswordById($pPassword, $pId, $pFilter = '')
	{
		parent::orm_action("UPDATE user SET password = :password WHERE id = :id $pFilter", ['password' => [$pPassword, 'str'], 'id' => [$pId, 'int']]);
	}


	function updateLastIpById($pIp, $pId, $pFilter = '')
	{
		parent::orm_action("UPDATE user SET last_ip = :last_ip WHERE id = :id $pFilter", ['last_ip' => [$pIp, 'str'], 'id' => [$pId, 'int']]);
	}


	function activateUserAccount($pActivationKey)
	{
		$activationKeyIsset = parent::orm_count('SELECT COUNT(*) FROM user WHERE activation_key = :activation_key', ['activation_key' => [$pActivationKey, 'str']]);
		parent::orm_action("UPDATE user SET activation_key = NULL WHERE activation_key = :activation_key $pFilter", ['activation_key' => [$pActivationKey, 'str']]);
		return $activationKeyIsset;
	}


	function delAccountById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM user WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}
}
