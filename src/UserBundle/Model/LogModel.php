<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

class LogModel extends Model
{
	/**
	 * Connexion sous formulaire classique - impossible de se connecter si compte non activé (activation_key null)
	 * @param String $pLogin le pseudo de l'internaute ou son email
	 * @param String $pPwd le mdp de l'internaute
	 * @param Boolean $pOrEmail autoriser la connexion via l'email également
	 */
	function loginByForm($pLogin, $pPwd, $pOrEmail = false)
	{
		$reqsql = "SELECT * FROM user WHERE (username = :login AND activation_key IS NULL";
		$reqsql .= $pOrEmail ? " OR email = :login" : '';
		$reqsql .= ") AND activation_key IS NULL";

		$connexion = parent::orm_getOne($reqsql, 'src\UserBundle\Entity\UserEntity', ['login' => [$pLogin, 'str']]);

		if (password_verify($pPwd, $connexion['password']))
			return $connexion;
		else
			return false;
	}


	/**
	 * Connexion via le cookie créé si demande de connexion automatique
	 * @param String $pCookieAutologin la value du cookie qui sera comparée à celle attachée au membre stockée en bdd
	 */
	function loginByCookie($pCookieAutologin)
	{
		$connexion = parent::orm_getOne("SELECT * FROM user WHERE autologin_key = :cookie_autologin", 'src\UserBundle\Entity\UserEntity', ['cookie_autologin' => [$pCookieAutologin, 'str']]);

		return $connexion;
	}


	/**
	 * Enregistrer un token pour se connecter automatiquement via un cookie
	 * @param String $pAutologin_key la value du cookie
	 * @param String $pId l'id de l'utilisateur
	 */
	function updateAutologKeyById($pAutologin_key, $pId)
	{
		parent::orm_action("UPDATE user SET autologin_key = '".$pAutologin_key."' WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
