<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

class BanlistModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_banlist $pFilter";
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\BanlistEntity');
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_banlist WHERE user_id = :user_id $pFilter";
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\BanlistEntity', ['user_id' => [$pId, 'int']]);
	}
}
