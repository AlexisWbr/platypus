<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

class BannedIpModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_banned_ip $pFilter";
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\BannedIpEntity');
	}


	function getOneByIp($pIp, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_banned_ip WHERE ip = :ip $pFilter";
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\BannedIpEntity', ['ip' => [$pIp, 'str']]);
	}
}
