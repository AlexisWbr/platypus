<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

class RankModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_rank $pFilter";
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\RankEntity');
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_rank WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\UserBundle\Entity\RankEntity', $varsToBind);
	}


	function getManyById($pIds, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_rank WHERE id IN ($pIds) $pFilter";
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\RankEntity');
	}


	/**
	 * Récupérer les rangs d'un membre selon son id (utilise la table user_rank_acl)
	 */
	function getManyByUserId($pUserId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM user_rank r LEFT JOIN user_rank_acl acl ON r.id = acl.rank_id WHERE acl.user_id = :user_id $pFilter";
		$varsToBind = ['user_id' => [$pUserId, 'int']];
		return parent::orm_getMany($reqsql, 'src\UserBundle\Entity\RankEntity', $varsToBind);
	}
}
