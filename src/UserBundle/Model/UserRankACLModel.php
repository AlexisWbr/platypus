<?php
namespace src\UserBundle\Model;
use Platypus\ORM\Model;

use src\UserBundle\Entity\UserRankACLEntity;

class UserRankACLModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM user_rank_acl $pFilter");
	}


	function getManyByUserId($pUserId, $pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM user_rank_acl WHERE user_id = :user_id $pFilter", null, ['user_id' => [$pUserId, 'int']]);
	}


	function getManyByRankId($pRankId, $pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM user_rank_acl WHERE rank_id = :rank_id $pFilter", null, ['rank_id' => [$pRankId, 'int']]);
	}


	function insertOne(UserRankACLEntity $pEntity)
	{
		var_dump($pEntity);
		$reqsql = "INSERT INTO user_rank_acl SET user_id = :user_id, rank_id = :rank_id";
		$varsToBind = [
						'user_id'			=> [$pEntity['user_id'], 'int'],
						'rank_id'			=> [$pEntity['rank_id'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	function deleteOneByUserIdAndRankId($pUserId, $pRankId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM user_rank_acl WHERE user_id = :user_id AND rank_id = :rank_id", ['user_id' => [$pUserId, 'int'], 'rank_id' => [$pRankId, 'int']]);
	}
}
