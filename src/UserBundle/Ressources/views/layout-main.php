<style type="text/css">
	#userBundlelogContainer{
		width: 300px;
		margin: 20px auto;
		background: hsla(0,0%,99%,1);
		border: 1px solid rgba(0,0,0,0.14);
		box-shadow: 0 0 5px rgba(0,0,0,0.08);
		padding: 20px;
		border-radius: 3px;
		font-family: arial;
	}
	#userBundlelogContainer .title{
		font-family: arial;
		font-size: 25px;
		color: #2b2b2b;
		border-bottom: 1px solid rgba(0, 0, 0, 0.24);
		margin-bottom: 20px;
   	}
	#userBundlelogContainer input{
		margin-bottom: 10px;
		color: black;
	}
	#userBundlelogContainer input[type="text"],
	#userBundlelogContainer input[type="email"],
	#userBundlelogContainer input[type="password"]{
		border-radius: 3px;
		border: 1px solid grey;
		width: 100%;
		height: 30px;
		font-size: 18px;
		padding-left: 5px;
	}
	#userBundlelogContainer input[type="submit"]{
		color: #333333;
		border: 1px solid #A5A5A5;
		background: #F5F5F5;
		border-radius: 3px;
		width: 80%;
		display: block;
		font-size: 14px;
		line-height: 25px;
		margin: 0 auto 10px auto;
	}
	#userBundlelogContainer label{
		color: #505050;
		font-weight: normal;
		font-size: 15px;
	}
	#userBundlelogContainer .links,
	#userBundlelogContainer .links a{
		color: #2b2b2b;
		text-decoration: none;
		font-size: 13px;
	} #userBundlelogContainer .links a:hover{
		color: #2b2b2b;
		text-decoration: underline;
	}
	#userBundlelogContainer .successMsg,
	#userBundlelogContainer .errorMsg{
		color: white;
		background: #DA3737;
		padding: 3px 5px;
		margin: 5px 0;
		display: block;
		text-align: center;
		border-radius: 3px;
	}
	#userBundlelogContainer .successMsg{
		background-color: rgba(75, 171, 58, 0.98);
	}
	#userBundlelogContainer .infoMsg{
		background-color: #3CA9DA;
		display: block;
		text-align: center;
		border-radius: 3px;
		padding: 5px 3px;
		margin: 5px;
		font-size: 11px;
	}
	#userBundlelogContainer .infoMsg a{
		color: white;
		text-decoration: none;
	} #userBundlelogContainer .infoMsg a:hover{
		text-decoration: underline;
	}
</style>

<?= $contentView ?>
