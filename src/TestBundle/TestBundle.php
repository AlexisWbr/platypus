<?php
namespace src\TestBundle;
use Platypus\Bundle\Bundle;

class TestBundle extends Bundle
{
	function run()
	{
		$controller = $this->getController();
		$controller->execute();

		$controller->getPage()->compose('layout-main', 'layout-main');
		$this->response->send($controller->getPage());
	}
}
