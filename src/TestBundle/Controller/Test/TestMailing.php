<?php
namespace src\TestBundle\Controller\Test;
// use app\component\Mailing\PHPMail;
use app\component\Mailing\MandrillMail;

class TestMailing extends MandrillMail
{
	function testsend()
	{
		$messageContent = 'salut lel';
		$senderUserName = 'Aigzy';
		$senderMail = 'localhost@mail.com';


		$this->to(['John Doe' => 'john@doe.com'])
			->cc([$senderMail])
			->fromName($senderUserName)
			->fromMail($senderMail)
			->replyTo($senderMail)
			->returnPath($senderMail)
			->subject($_SERVER['HTTP_HOST']." - Message du formulaire de contact")
			->message($messageContent);

		$this->send();
	}
}
