<?php
namespace src\RedactionBundle\Model;
use Platypus\ORM\Model;

use src\RedactionBundle\Entity\SliderEntity;
use src\RedactionBundle\Entity\SliderSlideEntity;

class SliderModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM redac_slider $pFilter", 'src\RedactionBundle\Entity\SliderEntity');
	}

	function getAllWithSliderSlides($pFilter = '')
	{
		$reqsql = "SELECT	s.id AS slider__id,
							s.name AS slider__name,
							s.position AS slider__position,
							s.type AS slider__type,
							f.id AS slider__slide__id,
							f.slider_id AS slider__slide__slider_id,
							f.content AS slider__slide__content,
							f.url_attached AS slider__slide__url_attached,
							f.position AS slider__slide__position
		FROM redac_slider s
		LEFT JOIN redac_slider_slide f ON f.slider_id = s.id $pFilter";
		return parent::orm_getManyNestedRecursive($reqsql, ['slider' => 'src\RedactionBundle\Entity\SliderEntity', 'slide' => 'src\RedactionBundle\Entity\SliderSlideEntity']);
	}


	function getMaxPosition($pFilter = '')
	{
		return parent::orm_count("SELECT MAX(position) FROM redac_slider $pFilter");
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_slider $pFilter");
	}


	function countById($pId, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_slider WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_slider WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\RedactionBundle\Entity\SliderEntity', $varsToBind);
	}


	function insertOne(SliderEntity $pSlider)
	{
		$reqsql = "INSERT INTO redac_slider SET name = :name, type = :type, position = :position";
		$varsToBind = [
						'name'		=> [$pSlider['name'], 'str'],
						'type'		=> [$pSlider['type'], 'int'],
						'position'	=> [$pSlider['position'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateOne(SliderEntity $pSlider)
	{
		$reqsql = "UPDATE redac_slider SET name = :name, type = :type WHERE	id = :id";
		$varsToBind = [
						'name'		=> [$pSlider['name'], 'str'],
						'type'		=> [$pSlider['type'], 'int'],
						'id'		=> [$pSlider['id'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	/**
	 * Changer la position des entités
	 * Les clefs des tableaux doivent être les id respectifs des entités
	 * @param Array $pSlidersPositionMap Position des sliders
	 */
	function updateManyPosition(Array $pEntitiesPositionMap)
	{
		$ids	= implode(',', array_keys($pEntitiesPositionMap));
		$reqsql	= "UPDATE redac_slider SET position = CASE id " ;
		foreach ($pEntitiesPositionMap as $id => $position) {
			$reqsql .= sprintf("WHEN %d THEN %d ", $id, $position);
		}
		$reqsql .= "END WHERE id IN ($ids)";

		parent::orm_action($reqsql);
	}


	/**
	 * Créer un trou dans les positions à l'ajout d'une entité
	 */
	function createPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_slider SET position = position+1 WHERE position >= :newEntityPosition";
		self::orm_action($reqsql, ['newEntityPosition' => [$pPosition, 'int']]);
	}


	/**
	 * Boucher un trou laissé dans les positions à la suppression d'une entité
	 */
	function fillPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_slider SET position = position-1 WHERE position > :entityPosition";
		parent::orm_action($reqsql, ['entityPosition' => [$pPosition, 'int']]);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM redac_slider WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
