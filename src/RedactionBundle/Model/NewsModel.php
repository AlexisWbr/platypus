<?php
namespace src\RedactionBundle\Model;
use Platypus\ORM\Model;

use src\RedactionBundle\Entity\NewsEntity;

class NewsModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM redac_news $pFilter", 'src\RedactionBundle\Entity\NewsEntity');
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_news $pFilter");
	}


	function countById($pId, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_news WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_news WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\RedactionBundle\Entity\NewsEntity', $varsToBind);
	}


	/**
	 * Récupérer une liste de tant entités à partir d'un endroit donné
	 */
	function getManyWithLimit($pEntitiesPerPage, $pEntryBegin, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_news $pFilter LIMIT :nbrPerPage OFFSET :entryBegin";
		$varsToBind = ['nbrPerPage' => [$pEntitiesPerPage, 'int'], 'entryBegin' => [$pEntryBegin, 'int']];
		return parent::orm_getMany($reqsql, 'src\RedactionBundle\Entity\NewsEntity', $varsToBind);
	}


	function insertOne(NewsEntity $pNews)
	{
		$reqsql = "INSERT INTO redac_news SET	name		 = :name,
												name_slug	 = :name_slug,
												content		 = :content,
												image		 = :image,
												date_add	 = NOW(),
												author_id	 = :author_id,
												display		 = :display,
												date_release = :date_release,
												pinned = :pinned";
		$varsToBind = [
			'name'			=> [$pNews['name'], 'str'],
			'name_slug'		=> [$pNews['name_slug'], 'str'],
			'content'		=> [$pNews['content'], 'str'],
			'image'			=> [$pNews['image'], 'str'],
			'author_id'		=> [$pNews['author_id'], 'int'],
			'display'		=> [$pNews['display'], 'int'],
			'date_release'	=> [$pNews['date_release'], 'str'],
			'pinned'		=> [$pNews['pinned'], 'int']
		];

		return parent::orm_action($reqsql, $varsToBind, true);
	}


	function updateOne(NewsEntity $pNews)
	{
		$reqsql = "UPDATE redac_news SET	name		 = :name,
											name_slug	 = :name_slug,
											content		 = :content,
											image		 = :image,
											date_edit	 = NOW(),
											editor_id	 = :editor_id,
											display		 = :display,
											date_release = :date_release,
											pinned = :pinned
									WHERE	id = :id";
		$varsToBind = [
			'name'			=> [$pNews['name'], 'str'],
			'name_slug'		=> [$pNews['name_slug'], 'str'],
			'content'		=> [$pNews['content'], 'str'],
			'image'			=> [$pNews['image'], 'str'],
			'editor_id'		=> [$pNews['editor_id'], 'int'],
			'display'		=> [$pNews['display'], 'int'],
			'date_release'	=> [$pNews['date_release'], 'str'],
			'pinned'		=> [$pNews['pinned'], 'int'],
			'id'			=> [$pNews['id'], 'int']
		];

		parent::orm_action($reqsql, $varsToBind);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		return parent::orm_action("DELETE FROM redac_news WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
