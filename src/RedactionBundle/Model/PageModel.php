<?php
namespace src\RedactionBundle\Model;
use Platypus\ORM\Model;

use src\RedactionBundle\Entity\PageEntity;

class PageModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM redac_page $pFilter", 'src\RedactionBundle\Entity\PageEntity');
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_page $pFilter");
	}


	function countById($pId, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_page WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_page WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\RedactionBundle\Entity\PageEntity', $varsToBind);
	}


	function getManyByCategoryId($pgetManyByCategoryId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_page WHERE category_id = :category_id $pFilter";
		$varsToBind = ['category_id' => [$pgetManyByCategoryId, 'int']];
		return parent::orm_getMany($reqsql, 'src\RedactionBundle\Entity\PageEntity', $varsToBind);
	}


	function getManyParentAndChildrenByCategoryId($pCategoryId, $pFields = '*', $pFilter = '')
	{
		$reqsql = ("SELECT $pFields FROM redac_page WHERE category_id = :category_id ORDER BY position, parent_id ");
		return parent::orm_getManyNested($reqsql, 'src\RedactionBundle\Entity\PageEntity', ['category_id' => [$pCategoryId, 'int']], 'id', 'parent_id');
	}


	function insertOne(PageEntity $pPage)
	{
		$reqsql = "INSERT INTO redac_page SET	category_id		 = :category_id,
												parent_id		 = :parent_id,
												type		 = :type,
												name			 = :name,
												name_slug		 = :name_slug,
												name_alternative = :name_alternative,
												content			 = :content,
												date_add		 = NOW(),
												author_id		 = :author_id,
												position		 = :position,
												display			 = :display,
												rights_del		 = :rights_del,
												rights_edit		 = :rights_edit";
		$varsToBind = [
			'category_id'		=> [$pPage['category_id'], 'int'],
			'parent_id'			=> [$pPage['parent_id'], 'int'],
			'type'				=> [$pPage['type'], 'int'],
			'name'				=> [$pPage['name'], 'str'],
			'name_slug'			=> [$pPage['name_slug'], 'str'],
			'name_alternative'	=> [$pPage['name_alternative'], 'str'],
			'content'			=> [$pPage['content'], 'str'],
			'author_id'			=> [$pPage['author_id'], 'int'],
			'position'			=> [$pPage['position'], 'int'],
			'display'			=> [$pPage['display'], 'int'],
			'rights_edit'		=> [$pPage['rights_edit'], 'int'],
			'rights_del'		=> [$pPage['rights_del'], 'int']
		];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateOne(PageEntity $pPage)
	{
		$reqsql = "UPDATE redac_page SET	category_id		 = :category_id,
											name			 = :name,
											name_slug		 = :name_slug,
											name_alternative = :name_alternative,
											content			 = :content,
											date_edit		 = NOW(),
											editor_id		 = :editor_id,
											display			 = :display,
											rights_edit		 = :rights_edit,
											rights_del		 = :rights_del
									WHERE	id = :id";
		$varsToBind = [
			'category_id'		=> [$pPage['category_id'], 'int'],
			'name'				=> [$pPage['name'], 'str'],
			'name_slug'			=> [$pPage['name_slug'], 'str'],
			'name_alternative'	=> [$pPage['name_alternative'], 'str'],
			'content'			=> [$pPage['content'], 'str'],
			'editor_id'			=> [$pPage['editor_id'], 'int'],
			'display'			=> [$pPage['display'], 'int'],
			'rights_edit'		=> [$pPage['rights_edit'], 'int'],
			'rights_del'		=> [$pPage['rights_del'], 'int'],
			'id'				=> [$pPage['id'], 'int']
		];

		parent::orm_action($reqsql, $varsToBind);
	}


	/**
	 * Changer la position et le parent des pages
	 * Les clefs des tableaux doivent être les id respectifs des pages
	 * @param Array $pPagesParentMap Id des parents des pages
	 * @param Array $pPagesPositionMap Position des pages
	 */
	function updateParentAndPosition(Array $pPagesParentMap, Array $pPagesPositionMap)
	{
		$ids	= implode(',', array_keys($pPagesParentMap));
		$reqsql	= "UPDATE redac_page SET parent_id = CASE id " ;
		foreach ($pPagesParentMap as $id => $parentId) {
			$reqsql .= sprintf("WHEN %d THEN %s ", $id, $parentId);
		}
		$reqsql .= "END, position = CASE id ";
		foreach ($pPagesPositionMap as $id => $position) {
			$reqsql .= sprintf("WHEN %d THEN %d ", $id, $position);
		}
		$reqsql .= "END WHERE id IN ($ids)";

		parent::orm_action($reqsql);
	}


	/**
	 * Créer un trou dans la position des pages
	 */
	function createGapInPositionByPositionAndCategoryId($pPosition, $pCategoryId)
	{
		$reqsql = "UPDATE redac_page SET position = position+1 WHERE position >= :newPagePosition AND category_id = :category_id";
		parent::orm_action($reqsql, ['newPagePosition' => [$pPosition, 'int'], 'category_id' => [$pCategoryId, 'int']]);
	}


	/**
	 * Boucher un trou dans la position des pages laissée par la suppression d'une page
	 */
	function fillGapInPositionByPositionAndCategoryId($pPosition, $pCategoryId)
	{
		$reqsql = "UPDATE redac_page SET position = position-1 WHERE position > :pagePosition AND category_id = :category_id";
		parent::orm_action($reqsql, ['pagePosition' => [$pPosition, 'int'], 'category_id' => [$pCategoryId, 'int']]);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM redac_page WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
