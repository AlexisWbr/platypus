<?php
namespace src\RedactionBundle\Model;
use Platypus\ORM\Model;

use src\RedactionBundle\Entity\SliderSlideEntity;

class SliderSlideModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM redac_slider_slide $pFilter", 'src\RedactionBundle\Entity\SliderSlideEntity');
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_slider_slide $pFilter");
	}


	function countBySliderId($pSliderId, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_slider_slide WHERE slider_id = :slider_id $pFilter", ['slider_id' => [$pSliderId, 'int']]);
	}

	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_slider_slide WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\RedactionBundle\Entity\SliderSlideEntity', $varsToBind);
	}


	function getManyBySliderId($pGetManyBySliderId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_slider_slide WHERE slider_id = :slider_id $pFilter";
		$varsToBind = ['slider_id' => [$pGetManyBySliderId, 'int']];
		return parent::orm_getMany($reqsql, 'src\RedactionBundle\Entity\SliderSlideEntity', $varsToBind);
	}


	function insertOne(SliderSlideEntity $pSliderSlide)
	{
		$reqsql = "INSERT INTO redac_slider_slide SET slider_id = :slider_id, content = :content, url_attached = :url_attached, position = :position";
		$varsToBind = [
						'slider_id'		=> [$pSliderSlide['slider_id'], 'str'],
						'content'		=> [$pSliderSlide['content'], 'int'],
						'url_attached'	=> [$pSliderSlide['url_attached'], 'int'],
						'position'		=> [$pSliderSlide['position'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateUrlById(SliderSlideEntity $pSlide)
	{
		$reqsql = "UPDATE redac_slider_slide SET url_attached = :url_attached WHERE	id = :id";
		$varsToBind = [
						'url_attached'	=> [$pSlide['url_attached'], 'str'],
						'id'			=> [$pSlide['id'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateContentAndUrlById(SliderSlideEntity $pSlide)
	{
		$reqsql = "UPDATE redac_slider_slide SET content = :content, url_attached = :url_attached WHERE	id = :id";
		$varsToBind = [
						'content'		=> [$pSlide['content'], 'str'],
						'url_attached'	=> [$pSlide['url_attached'], 'str'],
						'id'			=> [$pSlide['id'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	/**
	 * Changer la position des entités
	 * Les clefs des tableaux doivent être les id respectifs des entités
	 * @param Array $pSlidersPositionMap Position des sliders
	 */
	function updateManyPosition(Array $pEntitiesPositionMap)
	{
		$ids	= implode(',', array_keys($pEntitiesPositionMap));
		$reqsql	= "UPDATE redac_slider_slide SET position = CASE id " ;
		foreach ($pEntitiesPositionMap as $id => $position) {
			$reqsql .= sprintf("WHEN %d THEN %d ", $id, $position);
		}
		$reqsql .= "END WHERE id IN ($ids)";

		parent::orm_action($reqsql);
	}


	/**
	 * Créer un trou dans les positions à l'ajout d'une entité
	 */
	function createPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_slider SET position = position+1 WHERE position >= :newEntityPosition";
		self::orm_action($reqsql, ['newEntityPosition' => [$pPosition, 'int']]);
	}


	/**
	 * Boucher un trou laissé dans les positions à la suppression d'une entité
	 */
	function fillPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_slider SET position = position-1 WHERE position > :entityPosition";
		parent::orm_action($reqsql, ['entityPosition' => [$pPosition, 'int']]);
	}
	

	function deleteOneById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM redac_slider_slide WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
