<?php
namespace src\RedactionBundle\Model;
use Platypus\ORM\Model;

use src\RedactionBundle\Entity\PageCategoryEntity;

class PageCategoryModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM redac_page_cat $pFilter", 'src\RedactionBundle\Entity\PageCategoryEntity');
	}


	function count($pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_page_cat $pFilter");
	}


	function countById($pId, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM redac_page_cat WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM redac_page_cat WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\RedactionBundle\Entity\PageCategoryEntity', $varsToBind);
	}


	function insertOne(PageCategoryEntity $pPageCategory)
	{
		$reqsql = "INSERT INTO redac_page_cat SET name = :name, rights_add = :rights_add, position = :position";
		$varsToBind = [
						'name'			=> [$pPageCategory['name'], 'str'],
						'rights_add'	=> [$pPageCategory['rights_add'], 'str'],
						'position'		=> [$pPageCategory['position'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateOne(PageCategoryEntity $pPageCategory)
	{
		$reqsql = "UPDATE redac_page_cat SET name = :name, rights_add = :rights_add, position = :position WHERE id =:id";
		$varsToBind = [
						'name'			=> [$pPageCategory['name'], 'str'],
						'rights_add'	=> [$pPageCategory['rights_add'], 'str'],
						'id'			=> [$pPageCategory['id'], 'int'],
						'position'		=> [$pPageCategory['position'], 'int']
					];

		parent::orm_action($reqsql, $varsToBind);
	}


	/**
	 * Créer un trou dans les positions à l'ajout d'une entité
	 */
	function createPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_page_cat SET position = position+1 WHERE position >= :newEntityPosition";
		self::orm_action($reqsql, ['newEntityPosition' => [$pPosition, 'int']]);
	}


	/**
	 * Boucher un trou laissé dans les positions à la suppression d'une entité
	 */
	function fillPositionGap($pPosition)
	{
		$reqsql = "UPDATE redac_page_cat SET position = position-1 WHERE position > :entityPosition";
		parent::orm_action($reqsql, ['entityPosition' => [$pPosition, 'int']]);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM redac_page_cat WHERE id = :id", ['id' => [$pId, 'int']]);
	}
}
