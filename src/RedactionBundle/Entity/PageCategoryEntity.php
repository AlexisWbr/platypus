<?php
namespace src\RedactionBundle\Entity;
use Platypus\ORM\Entity;

class PageCategoryEntity extends Entity
{
	protected	$name;
	protected	$rights_add = 0;
	protected	$position;

	const		NAME_EMPTY 	= "Le nom de la catégorie ne peut pas être vide";

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		if (empty($pName) || ctype_space($pName)) $this->errors[] = self::NAME_EMPTY;

		$this->name = $pName;
	}
	
	function setRights_add($pRights_add){
		$this->rights_add = $pRights_add;
	}

	function setPosition($pPosition){
		$this->position = $pPosition;
	}

	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getRights_add(){
		return $this->rights_add;
	}

	function getPosition(){
		return $this->position;
	}
}
