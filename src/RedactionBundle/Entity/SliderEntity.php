<?php
namespace src\RedactionBundle\Entity;
use Platypus\ORM\Entity;

class SliderEntity extends Entity
{
	protected	$name;
	protected	$position;
	protected	$type;

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		$this->name = $pName;
	}
	
	function setPosition($pPosition){
		$this->position = (int)$pPosition;
	}
	
	function setType($pType){
		$this->type = (int)$pType;
	}

	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getPosition(){
		return $this->position;
	}
	
	function getType(){
		return $this->type;
	}
}
