<?php
namespace src\RedactionBundle\Entity;
use Platypus\ORM\Entity;

class SliderSlideEntity extends Entity
{
	protected	$slider_id;
	protected	$content;
	protected	$url_attached = null;
	protected	$position;

	// =================================================================
	// =============================SETTERS=============================
	
	function setSlider_id($pSlider_id){
		$this->slider_id = (int)$pSlider_id;
	}

	function setContent($pContent){
		$this->content = $pContent;
	}

	function setUrl_attached($pUrl_attached){
		$this->url_attached = empty($pUrl_attached) || ctype_space($pUrl_attached) ? null : $pUrl_attached;
	}
	
	function setPosition($pPosition){
		$this->position = (int)$pPosition;
	}
	
	// =================================================================
	// =============================GETTERS=============================
	
	function getSlider_id(){
		return $this->slider_id;
	}
	
	function getContent(){
		return $this->content;
	}

	function getUrl_attached(){
		return $this->url_attached;
	}
	
	function getPosition(){
		return $this->position;
	}
}
