<?php
namespace src\RedactionBundle\Entity;
use Platypus\ORM\Entity;

class PageEntity extends Entity
{
	protected	$category_id;
	protected	$parent_id		= null;
	protected	$type			= 1;
	protected	$name;
	protected	$name_slug;
	protected	$name_alternative;
	protected	$content;
	protected	$date_add;
	protected	$date_edit;
	protected	$author_id;
	protected	$editor_id;
	protected	$position;
	protected	$display		= 0;
	// protected	$rights_rights;
	protected	$rights_edit	= 0;
	protected	$rights_del		= 0;

	const		NAME_EMPTY 	= "Le nom de la page ne peut pas être vide";

	// =================================================================
	// =============================SETTERS=============================
	
	function setCategory_id($pCategory_id){
		$this->category_id = $pCategory_id;
	}
	
	function setParent_id($pParent_id){		
		$this->parent_id = empty($pParent_id) || ctype_space($pParent_id) ? null : $pParent_id;
	}
	
	function setType($pType){
		$pType = $pType > 0 ? $pType : 1;

		$this->type = (int)$pType;
	}
	
	function setName($pName){
		if (empty($pName) || ctype_space($pName)) $this->errors[] = self::NAME_EMPTY;
		
		$this->name = $pName;
	}
	
	function setName_slug($pName_slug){
		$this->name_slug = $pName_slug;
	}
	
	function setName_alternative($pName_alternative){
		$this->name_alternative = $pName_alternative;
	}
	
	function setContent($pContent){
		$this->content = $pContent;
	}
	
	function setDate_add($pDate_add){
		$this->date_add = $pDate_add;
	}
	
	function setDate_edit($pDate_edit){
		$this->date_edit = $pDate_edit;
	}
	
	function setAuthor_id($pAuthor_id){
		$this->author_id = $pAuthor_id;
	}
	
	function setEditor_id($pEditor_id){
		$this->editor_id = $pEditor_id;
	}
	
	function setPosition($pPosition){
		$this->position = $pPosition;
	}
	
	function setDisplay($pDisplay){
		$this->display = $pDisplay;
	}
	
	function setRights_edit($pRights_edit){
		$this->rights_edit = $pRights_edit;
	}

	function setRights_del($pRights_del){
		$this->rights_del = $pRights_del;
	}
	
	// =================================================================
	// =============================GETTERS=============================
	
	function getCategory_id(){
		return $this->category_id;
	}
	
	function getParent_id(){
		return $this->parent_id;
	}
	
	function getType(){
		return $this->type;
	}
	
	function getName(){
		return $this->name;
	}
	
	function getName_slug(){
		return $this->name_slug;
	}
	
	function getName_alternative(){
		return $this->name_alternative;
	}
	
	function getContent(){
		return $this->content;
	}
	
	function getDate_add(){
		return $this->date_add;
	}
	
	function getDate_edit(){
		return $this->date_edit;
	}
	
	function getAuthor_id(){
		return $this->author_id;
	}
	
	function getEditor_id(){
		return $this->editor_id;
	}
	
	function getPosition(){
		return $this->position;
	}
	
	function getDisplay(){
		return $this->display;
	}
	
	function getRights_edit(){
		return $this->rights_edit;
	}
	
	function getRights_del(){
		return $this->rights_del;
	}
}
