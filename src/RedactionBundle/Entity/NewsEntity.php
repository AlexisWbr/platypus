<?php
namespace src\RedactionBundle\Entity;
use Platypus\ORM\Entity;

class NewsEntity extends Entity
{
	protected	$name;
	protected	$name_slug;
	protected	$content;
	protected	$image		= null;
	protected	$date_add;
	protected	$date_edit	= null;
	protected	$author_id;
	protected	$editor_id	= null;
	protected	$display	= 0;
	protected	$date_release;
	protected	$pinned		= 0;

	const		NAME_EMPTY 	= "Le nom de l'actualité ne peut pas être vide";

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		if (empty($pName) || ctype_space($pName)) $this->errors[] = self::NAME_EMPTY;

		$this->name = $pName;
	}
	
	function setName_slug($pName_slug){
		$this->name_slug = $pName_slug;
	}
	
	function setContent($pContent){
		$this->content = $pContent;
	}
	
	function setImage($pImage){
		$this->image = $pImage;
	}
	
	function setDate_add($pDate_add){
		$this->date_add = $pDate_add;
	}
	
	function setDate_edit($pDate_edit){
		$this->date_edit = $pDate_edit;
	}
	
	function setAuthor_id($pAuthor_id){
		$this->author_id = $pAuthor_id;
	}
	
	function setEditor_id($pEditor_id){
		$this->editor_id = $pEditor_id;
	}

	function setDisplay($pDisplay){
		$this->display = $pDisplay;
	}

	function setDate_release($pDate_release){
		$pDate_release = empty($pDate_release) || ctype_space($pDate_release) ? null : $pDate_release;

		$this->date_release = $pDate_release;
	}
	
	function setPinned($pPinned){
		$this->pinned = $pPinned;
	}
	
	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getName_slug(){
		return $this->name_slug;
	}
	
	function getContent(){
		return $this->content;
	}
	
	function getImage(){
		return $this->image;
	}
	
	function getDate_add(){
		return $this->date_add;
	}
	
	function getDate_edit(){
		return $this->date_edit;
	}
	
	function getAuthor_id(){
		return $this->author_id;
	}
	
	function getEditor_id(){
		return $this->editor_id;
	}
	
	function getDisplay(){
		return $this->display;
	}
	
	function getDate_release(){
		return $this->date_release;
	}

	function getPinned(){
		return $this->pinned;
	}
}
