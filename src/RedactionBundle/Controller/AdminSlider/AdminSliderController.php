<?php
namespace src\RedactionBundle\Controller\AdminSlider;
use Platypus\Controller\Controller;

use src\RedactionBundle\Entity\SliderEntity;
use src\RedactionBundle\Entity\SliderSlideEntity;

class AdminSliderController extends Controller
{
	private $pathofUpload = __DIR__.'/../../../../web/uploads/slider/';

	function indexAction()
	{
		$this->Tool['Secure']->generateCSRFToken('delSlideToken');

		$sliderModel		= $this->getModel('Slider');
		$sliderSlideModel	= $this->getModel('SliderSlide');

		$sliderList			= $sliderModel->getAll('*', 'ORDER BY position');

		if (!empty($sliderList))
		{
			$currentSlider = isset($_GET['slider']) ? $_GET['slider'] : $sliderList[0]['id'] ;
			// si aucun slider de trouvé
			if (!$slider = $sliderModel->getOneById($currentSlider))
				$this->getResponse()->sendError(404);
			$sliderSlideList	= $sliderSlideModel->getManyBySliderId($currentSlider, '*', 'ORDER BY position');
		}
		else {
			$slider			 = null;
			$sliderSlideList = [];
			$currentSlider	 = null;
		}

		$this->addToPage('sliderList', $sliderList);
		$this->addToPage('slider', $slider);
		$this->addToPage('sliderSlideList', $sliderSlideList);
		$this->addToPage('currentSlider', $currentSlider);
	}


	function writeSliderAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$sliderModel	= $this->getModel('Slider');

		$oldSlider		= $sliderModel->getOneById($this->getRequest()->getData('id'));

		$this->addToPage('oldSlider', $oldSlider);
	}


	function writeSliderPOSTAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$sliderModel	= $this->getModel('slider');
		$slider			= new SliderEntity($_POST, true);

		if ($slider->isValid())
		{
			$slider->setPosition($sliderModel->getMaxPosition()+1);

			if ($slider->isNew()) {
				$sliderModel->insertOne($slider);
				$this->Tool['User']->setFlash("Slider ajouté", 'success');
			}
			else {
				$sliderModel->updateOne($slider);
				$this->Tool['User']->setFlash("Slider modifié", 'success');
			}
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index'));
		}
		else
		{
			foreach ($slider->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
		}
	}


	function changeSlidersOrderAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$sliderModel	= $this->getModel('Slider');

		$sliderList		= $sliderModel->getAll('*', 'ORDER BY position');

		$this->addToPage('sliderList', $sliderList);
	}


	function addSlideAction()
	{
		$sliderModel		= $this->getModel('Slider');
		$sliderSlideModel	= $this->getModel('SliderSlide');
		$slide				= new SliderSlideEntity($_POST, true);
		$slider				= $sliderModel->getOneById($slide['slider_id'], 'type');
		$file				= $this->getRequest()->fileData('image');

		if ($slide->isValid())
		{
			$slide->setPosition($sliderSlideModel->countBySliderId($slide['slider_id'])+1);

			// === si slide de type image ===
			// ==============================
			if($slider['type'] == 1)
			{
				// === si upload valide et correspond aux attentes ===
				// ===================================================
				if ($this->Tool['Files']->verifAndUploadFile($file, ['allow' => array('jpg', 'jpeg', 'png', 'gif')], null, false))
				{
					// créé un nom de fichier random
					$file['name'] = (md5(uniqid(rand(), true).$file['name']).'.'.pathinfo($file['name'])['extension']);
					$slide->setContent($file['name']);

					$pathofUpload = $this->pathofUpload.'/'.$slide['slider_id'].'/';

					// upload
					$this->Tool['Files']->uploadFile($file, $pathofUpload, $file['name']);
					if (!$this->Tool['Files']->resizeAndUploadImage($pathofUpload.'/'.$file['name'], 250, 250, 'thumb_'.$file['name'], $pathofUpload))
						$this->Tool['User']->setFlash('Aucune miniature n\'a pu être créée, vérifiez que l\'image soit d\'extension png jpg ou gif', 'error');

					$sliderSlideModel->insertOne($slide);
					$this->Tool['User']->setFlash("Slide ajoutée", 'success');
				}
				else
				{
					$this->Tool['User']->setFlash("La slide n'a pas pu être envoyée. Les extensions autorisées sont png, jpg, jpeg ou gif", 'error');
				}
			}
			// === si slide de type textuelle ===
			// ==================================
			elseif($slider['type'] == 2)
			{
				$sliderSlideModel->insertOne($slide);
			}
		}
		else
		{
			foreach ($slide->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
		}

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index', [], ['slider' => $slide['slider_id']]));
	}


	function editSlidePOSTAction()
	{
		$sliderModel = $this->getModel('Slider');
		$sliderSlideModel = $this->getModel('SliderSlide');

		$slider	= $sliderModel->getOneById($this->getRequest()->postData('slider_id'), 'type');
		$slide	= new SliderSlideEntity($_POST, true);

		// si slider type image
		if($slider['type'] == 1) {
			$sliderSlideModel->updateUrlById($slide);
		}
		// si slider type textuel
		elseif($slider['type'] == 2) {
			$sliderSlideModel->updateContentAndUrlById($slide);
		}

		$this->Tool['User']->setFlash('Slide mise à jour', 'success');
		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index', [], ['slider' => $slide['slider_id']]));
	}
}
