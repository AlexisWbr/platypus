<h1>Gestion des sliders</h1>

<div class="containerMain">
	<?php if($oldSlider['id']) : ?>
		<h2>Modifier un slider</h2>
	<?php else : ?>
		<h2>Ajouter un slider</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('redac_adminSlider_index') ?>" class="backLink">Retour</a>

	<div class="containerSimple">
		<form class="u-form formClassic" method="post" action="">
			<div class="u-container-fluid">
				<div class="u-row">
					<div class="col-lg-24">
						<div class="u-onGrid grid-w20">
							<label class="u-w100" for="name">
								<span>Nom du slider *</span>
								<input type="text" id="name" name="name" value="<?= $oldSlider['name'] ?>" maxlength="255" placeholder="Titre" required />
							</label>
							<label for="type1">
								<span>Type de slider</span>
								<input type="radio" name="type" id="type1" value="1" <?= !$oldSlider || (isset($oldSlider) && $oldSlider['type'] == 1) ? 'checked' : '' ?> />
								<i class="fa fa-picture-o"></i> Images
							</label>
							<label for="type2">
								<input type="radio" name="type" id="type2" value="2" <?= isset($oldSlider) && $oldSlider['type'] == 2 ? 'checked' : '' ?> />
								<i class="fa fa-font"></i> Textuel
							</label>
							<br />
							<br />
							<input type="hidden" name="id" value="<?= $oldSlider['id'] ?>" />
							<input type="submit" class="btnColor1 btnSmall" value="Enregistrer">
						</div>
					</div>
				</div>
			</div>
		</form>

		<?php if($oldSlider) : ?>
			<a	href="<?= $this->Helper['Routing']->url('redac_adminSlider_delSlider', ['id' => $oldSlider['id']]) ?>" class="btnColor3 btnSmall btnFade"
				onClick="return confirm('Supprimer le slider ? Cette action est irréversible et n\'est possible que si le slider ne contient aucune page.')" >
				<i class="fa fa-times"></i>
				Supprimer le slider
			</a>
		<?php endif ?>
	</div>
</div>
