<div class="containerMain">
	<h2>Ordre des sliders</h2>
	<a href="<?= $this->Helper['Routing']->url('redac_adminSlider_index') ?>" class="backLink">Retour</a>
	<br />
	<br />

	<div class="sortableTree">
		<ul class="sortable">
			<?php foreach($sliderList as $slider) : ?>
				<li id="alist_<?= $slider['id'] ?>">
					<div class="element">
						<i class="fa fa-arrows-alt"></i>
						<?= $slider['name'] ?>
					</div>
					<?php if (isset($slider->children)) : ?>
						<ul class="subTree">
							<?php $recursiveOutput($slider->children,$i,$j,$html) ?>
						</ul>
					<?php endif ?>
				</li>
			<?php endforeach?>
		</ul>
	</div>

	<form method="POST" action="<?= $this->Helper['Routing']->url('redac_adminSlider_changeSlidersOrderPOST') ?>" id="ajaxSendSliderPosition">
		<input type="hidden" value="" name="listSlidersOrder" id="listSlidersOrder">
		<input type="submit" value="Enregistrer l'ordre" class="btnColor1 btnSmall">
	</form>
</div>
