<h1>Gestion des sliders</h1>

<?php if(empty($sliderList)) : ?>
	<div class="blockDashed">Passez en mode SuperAdmin puis créez un slider pour y ajouter des slides</div>
	<?php if ($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
		<div class="text-center">
			<a class="btnColor1 btnSmall" href="<?= $this->Helper['Routing']->url('redac_adminSlider_writeSlider') ?>"><i class="fa fa-plus-circle"></i> Ajouter un slider</a>
		</div>
	<?php endif ?>
<?php else : ?>
	<nav class="navSub">
		<ul>
			<li class="title">Sliders</li>
			<?php foreach ($sliderList as $vslider) : ?>
				<li class="<?= ($vslider === reset($sliderList) && is_null($currentSlider)) || $currentSlider == $vslider['id'] ?'current':'' ?>">
					<a href="?slider=<?= $vslider['id'] ?>">
						<?= $vslider['type'] == 1 ? '<i class="fa fa-picture-o"></i> '  : '<i class="fa fa-font"></i> ' ?>
						<?= $vslider['name'] ?>
					</a>
					<?php if ($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
						<a href="<?= $this->Helper['Routing']->url('redac_adminSlider_writeSlider', ['id' => '-'.$vslider['id']]) ?>" class="edit" title="Editer le slider"><i class="fa fa-pencil-square-o"></i></a>
					<?php endif ?>
				</li>
			<?php endforeach ?>
			<?php if($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
				<li class="btnColor1 text-center">
					<a style="padding: 7px; display: inline-block" href="<?= $this->Helper['Routing']->url('redac_adminSlider_writeSlider') ?>"><i class="fa fa-plus-circle"></i></a>
				</li>
			<?php endif ?>
		</ul>
	</nav>

	<div class="containerMain">
		<?php if($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
			<a class="btnColor1 btnSmall" href="<?= $this->Helper['Routing']->url('redac_adminSlider_changeSlidersOrder') ?>">Modifier l'ordre des sliders</a>
		<?php endif ?>

		<h2>Liste des sliders</h2>

		<?php if($slider['type'] == 1): ?>
			<form class="u-form containerSimple dodgerblue containerUploadFiles popWaveContainer" method="POST" enctype="multipart/form-data" action="<?= $this->Helper['Routing']->url('redac_adminSlider_addSlide') ?>">
				<label class="u-w100" for="image" style="display: inline-block">
					<input type="file" name="image"  accept="image/*" required>
				</label><br />
				<input type="hidden" name="slider_id" value="<?= $currentSlider ?>" />
				<button type="submit" class="btnColor6 btnMedium u-none JSloaderOnClick popWaveTrigger">Ajouter l'image</button>
				<p>png - jpg - jpeg - gif</p>
				<div class="popWave"></div>
			</form>
		<?php elseif($slider['type'] == 2): ?>
			<form class="u-form containerSimple" method="POST" action="<?= $this->Helper['Routing']->url('redac_adminSlider_addSlide') ?>">
				<label for="content" class="u-w100">
					<span>Texte</span>
					<textarea name="content" id="content" rows="5" required /></textarea>
				</label>
				<label for="url_attached" class="u-w100">
					<span>Url attachée</span>
					<input type="text" name="url_attached" id="url_attached" />
				</label>
				<input type="hidden" name="slider_id" value="<?= $currentSlider ?>" />
				<button type="submit" class="btnColor1 btnMedium">Ajouter la slide</button>
			</form>
		<?php endif ?>

		<ul class="ulInline sortable">
			<?php foreach($sliderSlideList as $slide): ?>
				<li id="alist_<?= $slide['id'] ?>">
					<div class="element fileContainer graspable">
						<i class="fa fa-arrows-alt"></i>
						<?php if($slider['type'] == 1): ?>
							<div class="fileContain" style="background: url('<?= $this->getRequest()->root() ?>/web/uploads/slider/<?= $slide['slider_id'] ?>/<?= $slide['content'] ?>') 50% 50% no-repeat; background-size: cover;"></div>
						<?php elseif($slider['type'] == 2): ?>
							<div class="fileContain isText"><span><?= $this->Helper['Text']->truncate($slide['content'], 53) ?>...</span></div>
						<?php endif ?>
						<span class="modalTrigger link fa-flip-horizontal" data-modal="#editSlide<?= $slide['id'] ?>Modal" title="Modifier">
							<i class="fa fa-wrench"></i>
						</span>
						<span class="link l2" title="Supprimer">
							<a	href="<?= $this->Helper['Routing']->url('redac_adminSlider_delSlide', ['id' => $slide['id'], 'delSlideToken' => $_SESSION['CRSF']['delSlideToken']['token']]) ?>"
								onClick="return confirm('Supprimer la slide ? Cette action est irréversible.')">
								<i class="fa fa-trash"></i>
							</a>
						</span>
					</div>
				</li>
			<?php endforeach ?>
		</ul>
		<?php if(!empty($sliderSlideList)): ?>
			<form method="POST" action="<?= $this->Helper['Routing']->url('redac_adminSlider_changeSlideOrder') ?>" id="ajaxSendSlidePosition">
				<input type="hidden" value="" name="listSlideOrder" id="listSlideOrder">
				<input type="submit" value="Enregistrer l'ordre" class="btnColor1 btnSmall">
			</form>
		<?php else : ?>
			<div class="blockDashed">Aucune slide dans le slider</div>
		<?php endif ?>


		<?php foreach($sliderSlideList as $slide): ?>
			<!-- MODALE MODIFIER LES DETAILS D'UNE SLIDE -->
			<div id="editSlide<?= $slide['id'] ?>Modal" class="u-modalContainer">
				<div class="u-modalContain containerSimple">
					<h2>Modifier une slide</h2>
					<form method="POST" class="u-form" action="<?= $this->Helper['Routing']->url('redac_adminSlider_editSlide', ['id' => $slide['id']]) ?>">
						<?php if($slider['type'] == 2): ?>
							<label for="content" class="u-w100">
								<span>Texte</span>
								<textarea name="content" id="content" rows="5" required /><?= $slide['content'] ?></textarea>
							</label>
						<?php endif ?>
						<label for="url_attached" class="u-w100">
							<span>Url attachée</span>
							<input type="text" name="url_attached" id="url_attached" value="<?= $slide['url_attached'] ?>" />
						</label>

						<input type="hidden" name="slider_id" value="<?= $slide['slider_id'] ?>" />
						<input type="hidden" name="id" value="<?= $slide['id'] ?>" />
						<input type="submit" value="Enregistrer" class="btnSmall btnColor1" />
					</form>
				</div>
			</div>
			<!-- /MODALE MODIFIER LES DETAILS D'UNE SLIDE -->
		<?php endforeach ?>
	</div>
<?php endif ?>
