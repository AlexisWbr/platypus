<?php
namespace src\RedactionBundle\Controller\AdminSlider;
use Platypus\Controller\Controller;

use src\RedactionBundle\Entity\SliderEntity;
use src\RedactionBundle\Entity\SliderSlideEntity;

class AjaxController extends Controller
{
	function changeSlidersOrderAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$sliderModel		= $this->getModel('Slider');

		$slidersPositionMap = $_POST['alist'];
		// on répertorie la position de chaque entité
		$slidersPositionMap	= [];
		$i = 1;
		foreach ($_POST['alist'] as $entityId => $none) {
			$slidersPositionMap[$entityId] = $i++;
		}

		$sliderModel->updateManyPosition($slidersPositionMap);

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index'));
	}


	function delSliderAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$sliderModel = $this->getModel('Slider');

		// si existe bien on la supprime
		if ($slider = $sliderModel->countById($this->getRequest()->getData('id'))) {
			try {
				$sliderModel->deleteOneById($_GET['id']);
				$sliderModel->fillPositionGap($slider['position']);
				$this->Tool['User']->setFlash("Slider supprimé", 'success');
			} catch(\Exception $e) {
				$this->Tool['User']->setFlash("Impossible de supprimer le slider, il contient encore des slides", 'error');
			}
		}
		else {
			$this->Tool['User']->setFlash("Slider inexistant", 'error');
		}
		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index'));
	}


	function delSlideAction()
	{
		if (!$this->Tool['Secure']->checkCSRFToken('delSlideToken', 'GET', true))
		{
			$this->Tool['User']->setFlash("Impossible d'effectuer l'opération demandée, jeton expiré", 'error');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index'));
		}

		$sliderModel		= $this->getModel('Slider');
		$sliderSlideModel	= $this->getModel('SliderSlide');
		$slide				= $sliderSlideModel->getOneById($_GET['id']);

		if($slide) {
			$slider			= $sliderModel->getOneById($slide['slider_id']);

			if($slider['type'] == 1) {
				$pathofFile			= __DIR__.'/../../../../web/uploads/slider/'.$slide['slider_id'].'/'.$slide['content'];
				$pathofFileThumb	= __DIR__.'/../../../../web/uploads/slider/'.$slide['slider_id'].'/thumb_'.$slide['content'];

				if (file_exists($pathofFile))
					unlink($pathofFile);
				if (file_exists($pathofFileThumb))
					unlink($pathofFileThumb);
			}

			$sliderSlideModel->fillPositionGap($slide['position'], $slide['slider_id']);
			$sliderSlideModel->deleteOneById($slide['id']);

			$this->Tool['User']->setFlash("Slide supprimée", 'success');
		}
		else {
			$this->Tool['User']->setFlash("Slide innexistante", 'error');
		}

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index', [], ['slider' => $slide['slider_id']]));
	}


	function changeSlideOrderPOSTAction()
	{
		$sliderSlideModel	= $this->getModel('SliderSlide');

		$slidesPositionMap = $_POST['alist'];
		// on répertorie la position de chaque entité
		$slidesPositionMap	= [];
		$i = 1;
		foreach ($_POST['alist'] as $entityId => $none) {
			$slidesPositionMap[$entityId] = $i++;
		}

		$sliderSlideModel->updateManyPosition($slidesPositionMap);

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminSlider_index'));
	}
}
