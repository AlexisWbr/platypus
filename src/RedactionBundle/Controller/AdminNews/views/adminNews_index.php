<h1>Gestion des actualités</h1>

<div class="containerMain">
	<h2>Liste des actualités</h2>
	<?php if(empty($newsList)) : ?>
		<div class="blockDashed">Aucune actualité d'existante</div>
	<?php else : ?>
			<div>
				<table class="table-autoscroll tableClassic">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Création</th>
							<th>Édition</th>
							<th>Parution</th>
							<th>Lecture</th>
							<th>Supprimer</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($newsList as $news) : ?>
							<tr>
								<td style="width: 50%">
									<?= $news['pinned'] ? '<i class="fa fa-thumb-tack"></i>' : ' ' ?>
									<a href="<?= $this->Helper['Routing']->url('redac_adminNews_writeNews',['id'=>'-'.$news['id']]) ?>" title="Éditer l'actualité"><?= $news['name'] ?></a>
								</td>
								<td class="text-center" style="width: 20%">
									<?= date_format((date_create($news['date_add'])), "d/m/Y H\hi") ?>
								</td>
								<td class="text-center" style="width: 20%">
									<?php if(!is_null($news['date_edit'])) : ?>
										<?= date_format((date_create($news['date_edit'])), "d/m/Y H\hi") ?>
									<?php else : ?>
										-
									<?php endif ?>
								</td>
								<td class="text-center" style="width: 20%">
									<?php if(!is_null($news['date_release'])) : ?>
										<span class="<?= time() >= strtotime($news['date_release']) ? 'greenMark' : 'royalBlueMark'  ?>">
											<?= date_format((date_create($news['date_release'])), "d/m/Y H\hi") ?>
										</span>
									<?php else : ?>
										<span class="greenMark">
											<?= date_format((date_create($news['date_add'])), "d/m/Y H\hi") ?>
										</span>
									<?php endif ?>
								</td>
								<td class="text-center" style="width: 5%">
									<?php if($news['display']) : ?>
										<i class="fa fa-eye" title="Lecture autorisée"></i>
									<?php else : ?>
										<i class="fa fa-eye-slash" title="Lecture interdite"></i>
									<?php endif ?>
								</td>
								<td class="text-center" style="width: 5%">
									<a	href="<?= $this->Helper['Routing']->url('redac_adminNews_delNews',['id'=>$news['id'],'token'=>$_SESSION['CRSF']['delNewsToken']['token']]) ?>" title="Supprimer"
										onClick="return confirm('Supprimer l\'actualité ? Cette action est irréversible.')">
										<i class="fa fa-times"></i>
									</a>
								</td>
							</tr>
						<?php endforeach?>
					</tbody>
				</table>
			</div>
	<?php endif ?>

	<a href="<?= $this->Helper['Routing']->url('redac_adminNews_writeNews') ?>" class="btnColor1 btnSmall">
		<i class="fa fa-plus-circle"></i> Ajouter une actualité
	</a>

	<div class="pagination">
		<?= $this->cell('Pagination', array($currentPage, $totalPage, $this->Helper['Routing']->url('redac_adminNews_index'), array('<<', '<', '>', '>>'))) ?>
	</div>
</div>
