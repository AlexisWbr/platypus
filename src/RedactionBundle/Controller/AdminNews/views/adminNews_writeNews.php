<h1>Gestion des actualités</h1>
<div class="containerMain">
	<?php if($oldNews['id']) : ?>
		<h2>Modifier une actualité</h2>
	<?php else : ?>
		<h2>Ajouter une actualité</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('redac_adminNews_index') ?>" class="backLink">Retour</a>

	<form class="u-form formClassic" method="post" enctype="multipart/form-data" action="">
		<div class="u-container-fluid">
			<div class="u-row">
				<div class="col-lg-17">
					<div class="containerSimple">
						<label class="u-w100" for="name">
							<span>Titre de l'actualité *</span>
							<input style="height: 40px; font-size: 20px;" type="text" id="name" name="name" value="<?= $oldNews['name'] ?>" maxlength="255" placeholder="Titre" required />
						</label>
						<br />
						<label class="u-w100" for="content">
							<span>Contenu</span>
							<textarea type="text" id="content" class="tinymceTextarea" name="content"><?= $oldNews['content'] ?></textarea>
						</label>
						<br />
						<div class="text-center formClassic containerSimple dodgerblue containerUploadFiles">
							<label class="u-w100 u-inline" for="image">
								<span>Vignette de l'actualité <?= !$oldNews['id']?'*':'' ?></span>
								<br /><br />
								<input type="file" accept="image/*" id="image" name="image" <?= !$oldNews['id']?'required':'' ?> />
							</label>
							<p style="color: white">png - jpg - jpeg - gif</p>
						</div>

						<input type="hidden" name="id" value="<?= $oldNews['id'] ?>" >
						<input type="submit" class="btnColor1 btnMedium" value="Enregistrer l'actualité">
					</div>
				</div>

				<div class="col-lg-6 col-offset-1">
					<div class="containerSimple">
						<label for="date_release">
							<span>Parution le :</span>
							<input id="date_release" class="datetimepicker" type="text" name="date_release" value="<?= $oldNews['date_release'] ?>" placeholder="immédiatement">
						</label>
					</div>
					<div class="containerSimple">
						<label for="display">
							<span class="u-help" title="Afficher / Cacher l'actualité aux internautes">Autoriser la lecture</span>
							<label for="display">
								<input type="checkbox" name="display" value="1" id="display" <?= $oldNews['display'] == 1 || !$oldNews ? 'checked' : '' ?> >
							</label>
						</label>
						<br />
						<label for="pinned">
							<span>Épingler l'actualité</span>
							<label for="pinned">
								<input type="checkbox" name="pinned" value="1" id="pinned" <?= $oldNews['pinned'] == 1 ? 'checked' : '' ?> >
							</label>
						</label>
					</div>
					<?php if(!is_null($oldNews['image'])) : ?>
						<div class="containerSimple dodgerblue">
							<img style="max-width: 100%; border: 1px solid rgba(255, 255, 255, 0.45); display: block; margin: 0 auto;" src="<?= $this->getRequest()->root().'/web/uploads/news/images/thumb_'.$oldNews['image'] ?>">
							<div class="blockDashed white">Vignette actuelle</div>
						</div>
					<?php endif ?>
				</div>
			</div>
		</div>
	</form>
</div>
