<?php
namespace src\RedactionBundle\Controller\AdminNews;
use Platypus\Controller\Controller;

use src\RedactionBundle\Entity\NewsEntity;

class AdminNewsController extends Controller
{
	private $pathofUpload = __DIR__.'/../../../../web/uploads/news/images/';

	function indexAction()
	{
		$this->Tool['Secure']->generateCSRFToken('delNewsToken');

		$newsModel	= $this->getModel('News');

		$pgto		= $this->Tool['Paginate']->paginate(15, $newsModel->count('*'), $this->getRequest()->getData('p'));
		$newsList	= $newsModel->getManyWithLimit($pgto['entitiesPerPage'], $pgto['entryBegin'], '*', 'ORDER BY date_add DESC');

		$this->addToPage('newsList', $newsList);
	}


	function writeNewsAction()
	{
		$newsModel	= $this->getModel('News');

		// récupère une potentielle news en édition
		$oldNews = $newsModel->getOneById($this->getRequest()->getData('id'));

		$this->addToPage('oldNews', $oldNews);
	}


	function writeNewsPOSTAction()
	{
		$newsModel	= $this->getModel('News');
		$news		= new NewsEntity($_POST, true);
		$file		= $this->getRequest()->fileData('image');

		if ($news->isValid())
		{
			// === si upload valide et correspond aux attentes ou que pas existant ===
			// =======================================================================
			if ($this->Tool['Files']->verifAndUploadFile($file, ['allow' => array('jpg', 'jpeg', 'png', 'gif')], null, false) || $file['error'] == 4)
			{
				// slugify le nom
				$news->setName_slug($this->Tool['Text']->slugify($news['name']));
				// créé un nom de fichier random si fichier envoyé
				if ($file['error'] != 4) {
					$file['name'] = (md5(uniqid(rand(), true).$file['name']).'.'.pathinfo($file['name'])['extension']);
					$news->setImage($file['name']);
				}

				// upload l'image si envoyée
				if ($file['error'] != 4) {
					$this->Tool['Files']->uploadFile($file, $this->pathofUpload, $file['name']);
					if (!$this->Tool['Files']->resizeAndUploadImage($this->pathofUpload.'/'.$file['name'], 250, 250, 'thumb_'.$file['name'], $this->pathofUpload))
						$this->Tool['User']->setFlash('Aucune miniature n\'a pu être créée, vérifiez que l\'image soit d\'extension png jpg ou gif', 'error');
				}

				if ($news->isNew())
				{
					$news->setAuthor_id($_SESSION['user']['id']);
					$newsModel->insertOne($news);
					$this->Tool['User']->setFlash("Actualité ajoutée", 'success');
				}
				else
				{
					// si nouvelle image hébergée, on supprime l'ancienne
					$oldNews = $newsModel->getOneById($news['id']);
					if(!is_null($news['image'])) {
						unlink($this->pathofUpload.$oldNews['image']);
						unlink($this->pathofUpload.'thumb_'.$oldNews['image']);
					}
					// sinon on garde l'ancien nom de l'image
					else {
						$news->setImage($oldNews['image']);
					}

					$news->setEditor_id($_SESSION['user']['id']);
					$newsModel->updateOne($news);
					$this->Tool['User']->setFlash("Actualité modifiée", 'success');
				}
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminNews_index'));
			}
			else
			{
				$this->Tool['User']->setFlash("La vignette n'a pas pu être envoyée. Les extensions autorisées sont png, jpg, jpeg ou gif", 'error');
			}
		}
		else
		{
			foreach ($news->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
		}

		$this->setView('writeNews');
		$this->addToPage('oldNews', $news);
	}


	function delNewsAction()
	{
		if (!$this->Tool['Secure']->checkCSRFToken('delNewsToken')) {
			$this->Tool['User']->setFlash("Impossible d'effectuer l'opération demandée, jeton expiré", 'error');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminNews_index'));
		}

		$newsModel	= $this->getModel('News');

		if($oldNews = $newsModel->getOneById($_GET['id'])) {
			if($newsModel->deleteOneById($_GET['id'])) {
				$this->Tool['User']->setFlash("Actualité supprimée", 'success');
				if(!is_null($this->pathofUpload.$oldNews['image']) && is_file($this->pathofUpload.$oldNews['image'])) {
					unlink($this->pathofUpload.$oldNews['image']);
					unlink($this->pathofUpload.'thumb_'.$oldNews['image']);
				}
			}
		}
		else {
			$this->Tool['User']->setFlash("Actualité innexistante", 'error');
		}

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminNews_index'));
	}
}
