<?php
namespace src\RedactionBundle\Controller\AdminPage;
use Platypus\Controller\Controller;

use src\RedactionBundle\Entity\PageEntity;

class AjaxController extends Controller
{
	/**
	 * POST - Enregistrement du nouvel ordre des pages
	 */
	function changePagesOrderAction()
	{
		$pageModel		= $this->getModel('Page');

		$pagesParentMap	= $_POST['alist'];

		// on répertorie la position de chaque page
		// l'ancienne version commentée utilisait un système de position relatif à la profondeur des pages - maintenant la position est globale
		$pagesPositionMap	= [];
		// $array				= [];
		$i = 1;
		foreach ($pagesParentMap as $pageId => $parentId) {
			// $array[] = $parentId;
			// $pagesPositionMap[$pageId] = array_count_values($array)[$parentId];
			$pagesPositionMap[$pageId] = $i++;
		}

		$pageModel->updateParentAndPosition($pagesParentMap, $pagesPositionMap);

		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
	}


	/**
	 * Supprimer une page (et toutes ses sous-pages)
	 */
	function delPageAction()
	{
		if (!$this->Tool['Secure']->checkCSRFToken('delPageToken')) {
			$this->Tool['User']->setFlash("Impossible d'effectuer l'opération demandée, jeton expiré", 'error');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
		}

		$pageModel	= $this->getModel('Page');
		$page		= $pageModel->getOneById($_GET['id']);

		// si tentative de suppression alors que pas les droits
		if (!$page['rights_del'] && !$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole')))  {
			$this->getResponse()->sendError(403);
		}
		else {
			$pageModel->fillGapInPositionByPositionAndCategoryId($page['position'], $page['category_id']);
			$pageModel->deleteOneById($_GET['id']);
			$this->Tool['User']->setFlash("Page supprimée", 'success');

			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index', [], ['cat' => $page['category_id']]));
		}
	}
}
