<?php
namespace src\RedactionBundle\Controller\AdminPage;
use Platypus\Controller\Controller;

use src\RedactionBundle\Entity\PageEntity;
use src\RedactionBundle\Entity\PageCategoryEntity;
use src\UserBundle\Controller\UserRanksUtility;

class AdminPageController extends Controller
{
	// ajouter l'ajax de display sur liste des pages

	/**
	 * Page d'index de l'administration des pages
	 */
	function indexAction()
	{
		$this->Tool['Secure']->generateCSRFToken('delPageToken');

		$pageCatModel	= $this->getModel('PageCategory');
		$pageModel		= $this->getModel('Page');

		$pageCatList	= $pageCatModel->getAll('*', 'ORDER BY position');

		if (!empty($pageCatList))
		{
			$currentCat = isset($_GET['cat']) ? $_GET['cat'] : $pageCatList[0]['id'] ;
			// si aucune catégorie de trouvée
			if (!$pageCat = $pageCatModel->getOneById($currentCat))
				$this->getResponse()->sendError(404);

			$pageList	= $pageModel->getManyParentAndChildrenByCategoryId($currentCat, 'id, parent_id, type, rights_edit, name, date_add, date_edit, display, rights_edit, rights_del');
		}
		else {
			$pageCat	= null;
			$pageList	= [];
		}

		$catInGet		= $this->getRequest()->getData('cat'); // utilisé pour placer une classe 'current' sur la navbar des catégories

		$this->addToPage('pageCatList', $pageCatList);
		$this->addToPage('pageCat', $pageCat);
		$this->addToPage('pageList', $pageList);
		$this->addToPage('catInGet', $catInGet);
	}


	/**
	 * GET - Formulaire de rédaction d'une catégorie page
	 */
	function writeCatAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$pageCategoryModel	= $this->getModel('PageCategory');

		$oldPageCat			= $pageCategoryModel->getOneById($this->getRequest()->getData('id'));
		$pageCatlist		= $pageCategoryModel->getAll('*', 'ORDER BY position');

		$this->addToPage('oldPageCat', $oldPageCat);
		$this->addToPage('pageCatlist', $pageCatlist);
	}


	/**
	 * POST - Enregistrement d'une catégorie page
	 */
	function writeCatpostAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$pageCategoryModel	= $this->getModel('PageCategory');
		$pageCategory		= new PageCategoryEntity($_POST, true);

		if ($pageCategory->isValid())
		{
			$pageCategory->setPosition($pageCategory['position']);

			if ($pageCategory->isNew()) {
				// réarrange les positions
				$pageCategoryModel->createPositionGap($pageCategory['position']);

				$pageCategoryModel->insertOne($pageCategory);
				$this->Tool['User']->setFlash("Catégorie ajoutée", 'success');
			}
			else {
				// réarrange les positions
				$oldPageCat = $pageCategoryModel->getOneById($pageCategory['id'], 'position');
				$pageCategoryModel->fillPositionGap($oldPageCat['position']);
				$pageCategoryModel->createPositionGap($pageCategory['position']);

				$pageCategoryModel->updateOne($pageCategory);
				$this->Tool['User']->setFlash("Catégorie modifiée", 'success');
			}
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
		}
		else
		{
			foreach ($pageCategory->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
		}
	}


	/**
	 * Suppression d'une catégorie de page
	 */
	function delCatAction()
	{
		$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));

		$pageCategoryModel	= $this->getModel('PageCategory');

		// si existe bien on la supprime
		if ($pageCategory = $pageCategoryModel->countById($this->getRequest()->getData('id'))) {
			$pageCategoryModel->deleteOneById($_GET['id']);
			$pageCategoryModel->fillPositionGap($pageCategory['position']);
			$this->Tool['User']->setFlash("Catégorie supprimée", 'success');
		}
		else {
			$this->Tool['User']->setFlash("Catégorie inexistante", 'error');
		}
		$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index'));
	}


	/**
	 * GET - Formulaire de rédaction d'une page
	 */
	function writePageAction()
	{
		$pageCatModel	= $this->getModel('PageCategory');
		$pageModel		= $this->getModel('Page');

		// récupère une potentielle page en édition
		$oldPage = $pageModel->getOneById($this->getRequest()->getData('id'));
		$pageCat = $oldPage ? $pageCatModel->getOneById($oldPage['category_id']) : $pageCatModel->getOneById($this->getRequest()->getData('catid'));
		// si création de page interdite
		if (!$oldPage && !$pageCat['rights_add'])
			$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));
		// si édition de page interdite
		if ($oldPage && !$oldPage['rights_edit'])
			$this->firewall->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'));
		// récupère toutes les pages
		$currentCat			= !$oldPage ? $_GET['catid'] : $oldPage['category_id'];
		$listWrittenPages	= $pageModel->getManyParentAndChildrenByCategoryId($currentCat, 'id, parent_id, type, name');

		$this->addToPage('oldPage', $oldPage);
		$this->addToPage('listWrittenPages', $listWrittenPages);

		if($this->getRequest()->getData('t') == 'islink' || $oldPage['type'] == 2)
			$this->setView('writeLink');
	}


	/**
	 * POST - Enregistrement d'une page
	 */
	function writePagepostAction()
	{
		$pageCategoryModel	= $this->getModel('PageCategory');
		$pageModel			= $this->getModel('Page');
		$page				= new PageEntity($_POST, true);

		// si sous page va chercher la position de la page parente pour créer un trou dans la position des pages suivantes
		$pageParentPosition	= $page['parent_id'] ? $pageModel->getOneById($page['parent_id'], 'position')['position'] : NULL;
		$page->setName_slug($this->Tool['Text']->slugify($page['name']));

		if ($page->isValid())
		{
			if ($page->isNew())
			{
				$page->setCategory_id($_GET['catid']);
				$page->setAuthor_id($_SESSION['user']['id']);

				// si sous page
				if ($pageParentPosition)
					$pagePosition = $pageModel->count('id', "WHERE position >= ".$pageParentPosition." AND category_id =".$page['category_id'])+1;
				else
					$pagePosition = $pageModel->count('id', "WHERE category_id =".$page['category_id'])+1;
				$page->setPosition($pagePosition);

				// si sous page créé un trou
				if ($pageParentPosition)
					$pageModel->createGapInPositionByPositionAndCategoryId($page['position'], $page['category_id']);
				$pageModel->insertOne($page);
				$this->Tool['User']->setFlash("Page ajoutée", 'success');
			}
			else
			{
				$page->setEditor_id($_SESSION['user']['id']);
				$pageModel->updateOne($page);
				$this->Tool['User']->setFlash("Page modifiée", 'success');
			}
			$catid = isset($page->isNew) ? $_GET['catid'] : $page['category_id'];
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_adminPage_index', [], ['cat' => $catid]));

		}
		else
		{
			foreach ($page->getErrors() as $key => $value) {
				$this->Tool['User']->setFlash($value, 'error');
			}
			$this->addToPage('oldPage', $page);
		}
	}


	/**
	 * GET - Page pour changer l'ordre des pages
	 */
	function changePagesOrderAction()
	{
		$pageCatModel	= $this->getModel('PageCategory');
		$pageModel		= $this->getModel('Page');

		$pageCatList	= $pageCatModel->getAll();
		// si aucune catégorie de trouvée depuis _GET['cat'] c'est que inexistante
		if (isset($_GET['cat']) && !$pageCatModel->getOneById($_GET['cat']))
			$this->getResponse()->sendError(404);
		// si pas de _GET['id'] cherche la premiere catégorie
		$currentCat		= isset($_GET['cat']) ? $_GET['cat'] : $pageCatList[0]['id'] ;
		// si pas de catégorie existante c'est que forcément pas de page possible
		if (!empty($pageCatList)) {
			$pageCat	= $pageCatModel->getOneById($currentCat);
			$pageList	= $pageModel->getManyParentAndChildrenByCategoryId($currentCat, 'id, parent_id, name');
		}
		$pageList		= !is_null($pageList) ? $pageList : [];

		$this->addToPage('pageList', $pageList);
		$this->addToPage('pageCat', $pageCat);
	}
}
