<h1>Gestion des pages</h1>

<div class="containerMain">
	<?php if($oldPage['id']) : ?>
		<h2>Modifier une page</h2>
	<?php else : ?>
		<h2>Ajouter une page</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('redac_adminPage_index') ?>" class="backLink">Retour</a>

	<form class="u-form formClassic" method="post" action="">
		<div class="u-container-fluid">
			<div class="u-row">
				<div class="col-lg-17">
					<div class="containerSimple">
						<label class="u-w100" for="name">
							<span>Titre de la page *</span>
							<input style="height: 40px; font-size: 20px;" type="text" id="name" name="name" value="<?= $oldPage['name'] ?>" maxlength="255" placeholder="Titre" required />
						</label>
						<label class="u-w100" for="name_alternative">
							<span>Nom alternatif</span>
							<input type="text" id="name_alternative" name="name_alternative" value="<?= $oldPage['name_alternative'] ?>" maxlength="255" placeholder="Laisser vide pour utiliser le nom de la page" />
						</label>
						<br />
						<label class="u-w100" for="content">
							<span>Contenu</span>
							<textarea type="text" id="content" class="tinymceTextarea" name="content"><?= $oldPage['content'] ?></textarea>
						</label>
						<br />
						<input type="submit" class="btnColor1 btnMedium" value="Enregistrer la page">
					</div>
				</div>

				<div class="col-lg-6 col-offset-1">
					<?php include_once(__DIR__.'/includes/adminPage_writePageSidebar.php') ?>
				</div>
			</div>
		</div>
	</form>
</div>
