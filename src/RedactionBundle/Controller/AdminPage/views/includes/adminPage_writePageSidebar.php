<?php if(!$oldPage || ($oldPage->isNew())) : ?>
	<?php if($this->getRequest()->getData('t') != 'islink') : ?>
		<div class="containerSimple">
			<a class="btnColor1 btnSmall" href="<?= isset($_GET['sub']) ? '?sub='.$_GET['sub'].'&' :'?' ?>t=islink"><i class="fa fa-link"></i> Faire de cette page un lien</a>
		</div>
	<?php endif ?>
	<div class="containerSimple">
		<label class="u-align" for="parent_id">
			<span>Rattacher à</span>
			<select name="parent_id" id="parent_id">
			<option value="0">Aucune page</option>
				<?php function recursiveOutput($array, $oldPage, $i=0) { ?>
						<?php ++$i; ?>
						<?php foreach ($array as $item) : ?>
							<option value="<?= $item['id'] ?>" <?php if ( ($oldPage['parent_id'] == $item['id']) || (isset($_GET['sub']) && $_GET['sub'] == $item['id']) ) echo ('selected') ?> <?php if ($oldPage['id'] == $item['id']) echo 'disabled' ?>>
								<?php for($j=1;$j<$i;$j++) echo "&nbsp;" ?>
								<?php if ($i!=1) echo "└" ?>
								<?= $item['name'] ?>
							</option>
							<?php if (isset($item->children)) : ?>
								<div>
									<?php recursiveOutput($item->children, $oldPage, $i) ?>
								</div>
							<?php endif ?>
						<?php endforeach ?>
						<?php $i=0; ?>
				<?php } ?>
				<?php recursiveOutput($listWrittenPages, $oldPage) ?>

			</select>
		</label>
	</div>
<?php endif ?>

<div class="containerSimple">
	<label for="display">
		<span class="u-help" title="Afficher / Cacher la page aux internautes">Autoriser la lecture</span>
			<label for="display">
				<input type="checkbox" name="display" value="1" id="display" <?= $oldPage['display'] == 1 || !$oldPage ? 'checked' : '' ?> >
			</label>
	</label>
	<?php if ($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
		<br />
		<label for="rights_edit">
			<span>Autoriser l'édition</span>
			<label for="rights_edit">
				<input type="checkbox" name="rights_edit" value="1" id="rights_edit" <?= $oldPage['rights_edit'] == 1 || !$oldPage ? 'checked' : '' ?> >
			</label>
		</label>
		<br />
		<label for="rights_del">
			<span>Autoriser la suppression</span>
			<label for="rights_del">
				<input type="checkbox" name="rights_del" value="1" id="rights_del" <?= $oldPage['rights_del'] == 1 || !$oldPage ? 'checked' : '' ?> >
			</label>
		</label>
	<?php else : ?>
		<input type="hidden" name="rights_edit" value="<?= $oldPage['rights_edit'] == 1 || !$oldPage ? '1' : '0' ?>">
		<input type="hidden" name="rights_del" value="<?= $oldPage['rights_del'] == 1 || !$oldPage ? '1' : '0' ?>">
	<?php endif ?>
	<input type="hidden" name="id" value="<?= $oldPage['id'] ?>" >
	<input type="hidden" name="category_id" value="<?= $oldPage['category_id'] ?>" >
</div>
