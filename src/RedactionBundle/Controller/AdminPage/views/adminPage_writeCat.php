<h1>Gestion des pages</h1>

<div class="containerMain">
	<?php if($oldPageCat['id']) : ?>
		<h2>Modifier une catégorie</h2>
	<?php else : ?>
		<h2>Ajouter une catégorie</h2>
	<?php endif ?>

	<a href="<?= $this->Helper['Routing']->url('redac_adminPage_index') ?>" class="backLink">Retour</a>

	<div class="containerSimple">

		<form class="u-form formClassic" method="post" action="">
			<div class="u-onGrid grid-w30">
				<label class="u-w100" for="name">
					<span>Nom de la catégorie *</span>
					<input type="text" id="name" name="name" value="<?= $oldPageCat['name'] ?>" maxlength="128" required />
				</label>
				<label for="position">
					<span>Placer après</span>
					<select name="position" id="position">
						<option value="1">En première position</option>
						<?php foreach($pageCatlist as $pageCat) : ?>
							<option value="<?= $pageCat['position'] ?>" <?php if ($oldPageCat['position'] == $pageCat['position']+1) echo ('selected') ?> <?php if ($oldPageCat['id'] == $pageCat['id']) echo 'disabled' ?>>
								<?= $pageCat['name'] ?>
							</option>
						<?php endforeach ?>
					</select>
				</label>
				<br />
				<label for="rights_add">
					<span>Autoriser l'ajout de page</span>
					<label for="rights_add">
						<input type="checkbox" name="rights_add" value="1" id="rights_add" <?= $oldPageCat['rights_add'] == 1 || !$oldPageCat ? 'checked' : '' ?> >
					</label>
				</label>
			</div>
			<input type="hidden" name="id" value="<?= $oldPageCat['id'] ?>" >
			<input type="submit" class="btnColor1 btnSmall" value="Enregistrer">
		</form>
		<?php if($oldPageCat) : ?>
			<a	href="<?= $this->Helper['Routing']->url('redac_adminPage_delCatpost', ['id' => $oldPageCat['id']]) ?>" class="btnColor3 btnSmall btnFade"
				onClick="return confirm('Supprimer la catégorie ? Cette action est irréversible et n\'est possible que si la catégorie ne contient aucune page.')" >
				<i class="fa fa-times"></i>
				Supprimer la catégorie
			</a>
		<?php endif ?>
	</div>
</div>
