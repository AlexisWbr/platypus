<h1>Gestion des pages</h1>

<?php if(empty($pageCatList)) : ?>
	<div class="blockDashed">Passez en mode SuperAdmin puis créez une catégorie pour ajouter des pages</div>
	<?php if ($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
		<div class="text-center">
			<a class="btnColor1 btnSmall" href="<?= $this->Helper['Routing']->url('redac_adminPage_writeCat') ?>"><i class="fa fa-plus-circle"></i> Ajouter une catégorie</a>
		</div>
	<?php endif ?>
<?php else : ?>
	<nav class="navSub">
		<ul>
			<li class="title">Catégories</li>
			<?php foreach ($pageCatList as $cat) : ?>
				<li class="<?= ($cat === reset($pageCatList) && is_null($catInGet)) || $catInGet == $cat['id'] ?'current':'' ?>">
					<a href="?cat=<?= $cat['id'] ?>">
						<?= $cat['name'] ?>
					</a>
					<?php if ($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
						<a href="<?= $this->Helper['Routing']->url('redac_adminPage_writeCat', ['id' => '-'.$cat['id']]) ?>" class="edit" title="Editer la catégorie"><i class="fa fa-pencil-square-o"></i></a>
					<?php endif ?>
				</li>
			<?php endforeach ?>
			<?php if($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
				<li class="btnColor1 text-center">
					<a style="padding: 7px; display: inline-block" href="<?= $this->Helper['Routing']->url('redac_adminPage_writeCat') ?>"><i class="fa fa-plus-circle"></i></a>
				</li>
			<?php endif ?>
		</ul>
	</nav>

	<div class="containerMain">
		<h2>Liste des pages</h2>

		<?php if(empty($pageList)) : ?>
			<div class="blockDashed">Aucune page d'existante dans cette catégorie</div>
		<?php else : ?>
			<div>
				<table class="table-autoscroll tableClassic">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Création</th>
							<th>Édition</th>
							<th>Lecture</th>
							<th>Supprimer</th>
						</tr>
					</thead>
					<tbody>
						<?php $recursiveOutput = function($pageList,$i=0,$html)use(&$recursiveOutput,$pageCat) { ?>
							<?php ++$i; ?>
							<?php foreach($pageList as $page) : ?>
								<tr>
									<td class="<?= !$page['rights_edit'] ? 'forbidden' : '' ?>" style="width: 50%">
										<?php for($j=1;$j<$i;$j++) echo '<span class="childIndent"></span>' ?>
										<?php if ($page['rights_edit'] || $this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
											<span class="childIndentAnchor">
												<?php if($page['type'] == 2) : ?><i class="fa fa-link"></i><?php endif ?>
												<a href="<?= $html->Helper['Routing']->url('redac_adminPage_writePage',['id'=>'-'.$page['id']]) ?>" title="Éditer la page"><?= $page['name'] ?></a>
											</span>
										<?php else : ?>
											<span class="childIndentAnchor">
												<?php if($page['type'] == 2) : ?><i class="fa fa-link"></i><?php endif ?>
												<?= $page['name'] ?>
											</span>
										<?php endif ?>
										<?php if ($pageCat['rights_add'] || $this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
											<a href="<?= $html->Helper['Routing']->url('redac_adminPage_writePage',['catid'=>'-into'.$pageCat['id']],['sub'=>$page['id']]) ?>" title="Ajouter une sous-page"><i class="fa fa-plus"></i></a>
										<?php endif ?>
									</td>
									<td class="text-center" style="width: 20%">
										<?= date_format((date_create($page['date_add'])), "d/m/Y H\hi") ?>
									</td>
									<td class="text-center" style="width: 20%">
										<?php if(!is_null($page['date_edit'])) : ?>
											<?= date_format((date_create($page['date_edit'])), "d/m/Y H\hi") ?>
										<?php else : ?>
											-
										<?php endif ?>
									</td>
									<td class="text-center" style="width: 5%">
										<?php if($page['display']) : ?>
											<!-- <a href="" title="Interdire la lecture"><i class="fa fa-eye"></i></a> -->
											<i class="fa fa-eye" title="Lecture autorisée"></i>
										<?php else : ?>
											<!-- <a href="" title="Autoriser la lecture"><i class="fa fa-eye-slash"></i></a> -->
											<i class="fa fa-eye-slash" title="Lecture interdite"></i>
										<?php endif ?>

									</td>
									<td class="text-center <?= !$page['rights_del'] ? 'forbidden' : '' ?>" style="width: 5%">
										<?php if($page['rights_del'] || $this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
											<a	href="<?= $this->Helper['Routing']->url('redac_adminPage_delPage',['id'=>$page['id'],'token'=>$_SESSION['CRSF']['delPageToken']['token']]) ?>" title="Supprimer"
												onClick="return confirm('Supprimer la page ? Cette action est irréversible et supprimera toutes les sous pages.')">
												<i class="fa fa-times"></i>
											</a>
										<?php endif ?>
									</td>
								</tr>
								<?php if (isset($page->children)) : ?>
									<div class="subTree">
										<?php $recursiveOutput($page->children,$i,$html) ?>
									</div>
								<?php endif ?>
							<?php endforeach?>
							<?php $i=0; ?>
						<?php } ?>
						<?php $recursiveOutput($pageList,0,$this) ?>
					</tbody>
				</table>
			</div>
		<?php endif?>

		<?php if($pageCat['rights_add'] || $this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
			<a <?php if(!empty($pageCatList)) : ?> href="<?= $this->Helper['Routing']->url('redac_adminPage_writePage', ['catid'=>'-into'.$pageCat['id']]) ?>" <?php endif ?> class="btnColor1 btnSmall <?= empty($pageCatList)?'btnDisabled':'' ?>" <?= empty($pageCatList)?'title="Ajoutez une catégorie"':'' ?>>
				<i class="fa fa-plus-circle"></i> Ajouter une page
			</a>
			<?php if(!empty($pageList)) : ?>
				<a href="<?= $this->Helper['Routing']->url('redac_adminPage_changePagesOrder', [], ['cat'=>$pageCat['id']]) ?>" class="btnColor1 btnSmall" title="Modifier l'ordre des pages">
					 Modifier l'ordre des pages
				</a>
			<?php endif ?>
		<?php endif ?>
	</div>

<?php endif ?>
