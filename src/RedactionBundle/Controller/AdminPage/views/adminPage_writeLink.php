<h1>Gestion des pages</h1>

<div class="containerMain">
	<?php if($oldPage['id']) : ?>
		<h2>Modifier un lien</h2>
	<?php else : ?>
		<h2>Ajouter un lien</h2>
	<?php endif ?>
	<a href="<?= $this->Helper['Routing']->url('redac_adminPage_index') ?>" class="backLink">Retour</a>

	<form class="u-form formClassic" method="post" action="">
		<div class="u-container-fluid">
			<div class="u-row">
				<div class="col-lg-17">
					<div class="containerSimple">
						<label class="u-w100" for="name">
							<span>Nom du lien *</span>
							<input type="text" id="name" name="name" value="<?= $oldPage['name'] ?>" maxlength="255" required />
						</label>
						<label class="u-w100" for="content">
							<span>Lien hypertexte *</span>
							<input type="text" id="content" name="content" value="<?= $oldPage['content'] ?>" required />
						</label>
						<br />
						<input type="hidden" name="type" value="2" />
						<input type="submit" class="btnColor1 btnMedium" value="Enregistrer le lien">
					</div>
				</div>

				<div class="col-lg-6 col-offset-1">
					<?php include_once(__DIR__.'/includes/adminPage_writePageSidebar.php') ?>
				</div>
			</div>
		</div>
	</form>
</div>
