<div class="containerMain">
	<h2>Ordre des pages - <?= $pageCat['name'] ?></h2>
	<a href="<?= $this->Helper['Routing']->url('redac_adminPage_index', [], ['cat'=>$pageCat['id']]) ?>" class="backLink">Retour</a>
	<br />
	<br />

	<div class="sortableTree">
		<ul class="sortable">
			<?php $j = 0 ?>
			<?php $recursiveOutput = function($pageList,$i=0,&$j,$html)use(&$recursiveOutput,$pageCat) { ?>
				<?php ++$i; ?>
				<?php foreach($pageList as $page) : ?>
					<?php ++$j; ?>
					<li id="alist_<?= $page['id'] ?>">
						<div class="element">
							<i class="fa fa-arrows-alt"></i>
							<?= $page['name'] ?>
						</div>
						<?php if (isset($page->children)) : ?>
							<ul class="subTree">
								<?php $recursiveOutput($page->children,$i,$j,$html) ?>
							</ul>
						<?php endif ?>
					</li>
				<?php endforeach?>
				<?php $i=0; ?>
			<?php } ?>
			<?php $recursiveOutput($pageList,0,$j,$this) ?>
		</ul>
	</div>

	<form method="POST" action="<?= $this->Helper['Routing']->url('redac_adminPage_changePagesOrderpost') ?>" id="ajaxSendNestedTree">
		<input type="hidden" value="" name="listPagesOrder" id="listPagesOrder">
		<input type="submit" value="Enregistrer l'ordre" class="btnColor1 btnSmall">
	</form>
</div>
