<?php
namespace src\RedactionBundle\Controller\News;
use Platypus\Controller\Controller;

class NewsController extends Controller
{
	/**
	 * Liste des news
	 */
	function indexAction()
	{
		$newsModel		= $this->getModel('News');

		$pgto			= $this->Tool['Paginate']->paginate(15, $newsModel->count('*'), $this->getRequest()->getData('p'));
		$newsList		= $newsModel->getManyWithLimit($pgto['entitiesPerPage'], $pgto['entryBegin'], '*', 'WHERE pinned = 0 && display = 1 && (date_release <= CURDATE() || date_release IS NULL) ORDER BY date_add DESC');
		$newsPinnedList	= $newsModel->getAll('*', 'WHERE pinned = 1 && display = 1 && (date_release <= CURDATE() || date_release IS NULL) ORDER BY date_add DESC');

		$this->addToPage('newsList', $newsList);
		$this->addToPage('newsPinnedList', $newsPinnedList);
	}


	/**
	 * Détail d'une news
	 */
	function showAction()
	{
		$newsModel = $this->getModel('News');

		if (!$news = $newsModel->getOneById($_GET['id'], '*', 'AND display = 1 && (date_release <= CURDATE() || date_release IS NULL)'))
			$this->getResponse()->sendError(404);

		if ($_GET['slug'] != $news['name_slug'])
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_news_show', ['id' => $news['id'], 'slug' => $news['name_slug']]));

		$this->addToPage('news', $news);
	}
}
