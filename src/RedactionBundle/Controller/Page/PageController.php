<?php
namespace src\RedactionBundle\Controller\Page;
use Platypus\Controller\Controller;

class PageController extends Controller
{
	/**
	 * Afficher une page
	 */
	function showAction()
	{
		$pageModel = $this->getModel('Page');

		if (!$page = $pageModel->getOneById($_GET['id'], '*', 'AND display = 1'))
			$this->getResponse()->sendError(404);

		if ($_GET['slug'] != $page['name_slug'])
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('redac_page_show', ['id' => $page['id'], 'slug' => $page['name_slug']]));
		
		$this->addToPage('pageTitle', $page['name']);
		$this->addToPage('page', $page);
	}
}
