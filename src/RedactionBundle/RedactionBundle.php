<?php
namespace src\RedactionBundle;
use Platypus\Bundle\Bundle;

class RedactionBundle extends Bundle
{
	function run()
	{
		$controller = $this->getController();
		$controller->execute();

		if ($this->getRoute()->hasPrefixe("/adminpanel")) {
			$appLayout = 'layout-admin';
			$controller->getFirewall()->hasRank(['ADMIN']);
		} else {
			$appLayout = 'layout-main';
		}

		$controller->getPage()->compose($appLayout, 'layout-main');
		$this->response->send($controller->getPage());
	}
}
