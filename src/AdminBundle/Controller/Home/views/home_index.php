<h1>Espace Administration</h1>

<div class="containerMain">
	<div class="coloredBlockContainer">
		<div id="numberPages" class="coloredBlock blue">
			<span class="statNumber"><i class="fa fa-fw fa-file-text"></i>22</span>
		</div>
		<span class="title">Pages rédigées</span>
	</div>

	<div class="coloredBlockContainer">
		<div id="numberPages" class="coloredBlock red">
			<span class="statNumber">124</span>
		</div>
		<span class="title">Lorem ipsum</span>
	</div>
</div>
