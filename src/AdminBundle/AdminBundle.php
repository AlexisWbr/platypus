<?php
namespace src\AdminBundle;
use Platypus\Bundle\Bundle;

class AdminBundle extends Bundle
{
	function run()
	{
		$controller = $this->getController();
		$controller->getFirewall()->hasRank(['ADMIN', 'EDITOR']);
		$controller->execute();

		$controller->getPage()->compose('layout-admin', 'layout-main');
		$this->response->send($controller->getPage());
	}
}
