<?php
namespace src\LibraryBundle\Entity;
use Platypus\ORM\Entity;

class FolderEntity extends Entity
{
	protected	$name;
	protected	$parent_id = null;
	protected	$color = 1;

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		$this->name = $pName;
	}
	
	function setParent_id($pParent_id){
		$pParent_id = empty($pParent_id) ? null : $pParent_id;
		$this->parent_id = $pParent_id;
	}

	function setColor($pColor){
		$this->color = (int) $pColor;
	}

	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getParent_id(){
		return $this->parent_id;
	}

	function getColor(){
		return $this->color;
	}
}
