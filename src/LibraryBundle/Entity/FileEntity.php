<?php
namespace src\LibraryBundle\Entity;
use Platypus\ORM\Entity;

class FileEntity extends Entity
{
	protected	$name;
	protected	$type = null;
	protected	$folder_id;

	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		$this->name = $pName;
	}
	
	function setType($pType){
		$this->type = $pType;
	}

	function setFolder_id($pFolder_id){
		$pFolder_id = empty($pFolder_id) ? null : $pFolder_id;
		$this->folder_id = $pFolder_id;
	}
	
	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getType(){
		return $this->type;
	}

	function getFolder_id(){
		return $this->folder_id;
	}
}
