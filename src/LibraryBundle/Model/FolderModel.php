<?php
namespace src\LibraryBundle\Model;
use Platypus\ORM\Model;

use src\LibraryBundle\Entity\FolderEntity;

class FolderModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM lib_folder $pFilter", 'src\LibraryBundle\Entity\FolderEntity');
	}


	function getAllNested($pFields = '*', $pFilter = '')
	{
		$reqsql = ("SELECT $pFields FROM lib_folder $pFilter ORDER BY id");
		return parent::orm_getManyNested($reqsql, 'src\LibraryBundle\Entity\FolderEntity', [], 'id', 'parent_id');
	}


	function getAllNestedInArray($pFields = '*', $pFilter = '')
	{
		$reqsql = ("SELECT $pFields FROM lib_folder $pFilter ORDER BY id");
		return parent::orm_getManyNested($reqsql, null, [], 'id', 'parent_id');
	}


	function countByName($pName, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM lib_folder WHERE name = :name $pFilter", ['name' => [$pName, 'str']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM lib_folder WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\LibraryBundle\Entity\FolderEntity', $varsToBind);
	}


	function getManyByParentId($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM lib_folder WHERE parent_id ";
		$reqsql .= !is_null($pId) ? '= :id ' : 'IS NULL ';
		$reqsql .= $pFilter;

		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getMany($reqsql, 'src\LibraryBundle\Entity\FolderEntity', $varsToBind);
	}


	function insertOne(FolderEntity $pFolder)
	{
		$reqsql = "INSERT INTO lib_folder SET	name = :name,
												parent_id = :parent_id,
												color = :color";
		$varsToBind = [
			'name'			=> [$pFolder['name'], 'str'],
			'parent_id'		=> [$pFolder['parent_id'], 'int'],
			'color'			=> [$pFolder['color'], 'str']
		];

		return parent::orm_action($reqsql, $varsToBind);
	}


	function updateOne(FolderEntity $pFolder)
	{
		$reqsql = "UPDATE lib_folder SET	name	 = :name,
											color	 = :color
									WHERE	id = :id";
		$varsToBind = [
			'name'	=> [$pFolder['name'], 'int'],
			'color'	=> [$pFolder['color'], 'str'],
			'id'	=> [$pFolder['id'], 'int']
		];

		return parent::orm_action($reqsql, $varsToBind);
	}


	function updateParentIdByIds($pParentId, String $pIds)
	{
		$reqsql = "UPDATE lib_folder SET parent_id = :parent_id WHERE id IN ($pIds)";
		$varsToBind = [
			'parent_id'		=> [$pParentId, 'int']
		];
		return parent::orm_action($reqsql, $varsToBind);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		return parent::orm_action("DELETE FROM lib_folder WHERE id = :id $pFilter", ['id' => [$pId, 'int']]);
	}


	function deleteAll($pFilter = '')
	{
		return parent::orm_action("DELETE FROM lib_folder $pFilter");
	}
}
