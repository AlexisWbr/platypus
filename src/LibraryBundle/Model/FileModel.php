<?php
namespace src\LibraryBundle\Model;
use Platypus\ORM\Model;

use src\LibraryBundle\Entity\FileEntity;

class FileModel extends Model
{
	function getAll($pFields = '*', $pFilter = '')
	{
		return parent::orm_getMany("SELECT $pFields FROM lib_file $pFilter", 'src\LibraryBundle\Entity\FileEntity');
	}


	function countByName($pName, $pFields = '*',  $pFilter = '')
	{
		return parent::orm_count("SELECT COUNT($pFields) FROM lib_file WHERE name = :name $pFilter", ['name' => [$pName, 'str']]);
	}


	function getOneById($pId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM lib_file WHERE id = :id $pFilter";
		$varsToBind = ['id' => [$pId, 'int']];
		return parent::orm_getOne($reqsql, 'src\LibraryBundle\Entity\FileEntity', $varsToBind);
	}


	function getManyByFolderId($pFolderId, $pFields = '*', $pFilter = '')
	{
		$reqsql = "SELECT $pFields FROM lib_file WHERE folder_id ";
		$reqsql .= !is_null($pFolderId) ? '= :folder_id ' : 'IS NULL ';
		$reqsql .= $pFilter;

		$varsToBind = ['folder_id' => [$pFolderId, 'int']];
		return parent::orm_getMany($reqsql, 'src\LibraryBundle\Entity\FileEntity', $varsToBind);
	}


	function insertOne(FileEntity $pFile)
	{
		$reqsql = "INSERT INTO lib_file SET	name = :name, folder_id = :folder_id,
										type = :type";
		$varsToBind = [
			'name'		=> [$pFile['name'], 'str'],
			'type'		=> [$pFile['type'], 'str'],
			'folder_id'	=> [$pFile['folder_id'], 'int']
		];

		parent::orm_action($reqsql, $varsToBind);
	}


	function updateFolderIdByIds($pFolderId, String $pIds)
	{
		$reqsql = "UPDATE lib_file SET folder_id = :folder_id WHERE id IN ($pIds)";
		$varsToBind = [
			'folder_id'		=> [$pFolderId, 'int']
		];
		parent::orm_action($reqsql, $varsToBind);
	}


	function deleteOneById($pId, $pFilter = '')
	{
		parent::orm_action("DELETE FROM lib_file WHERE id = :id", ['id' => [$pId, 'int']]);
	}


	function deleteAll($pFilter = '')
	{
		parent::orm_action("DELETE FROM lib_file $pFilter");
	}
}
