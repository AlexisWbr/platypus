<?php
namespace src\LibraryBundle\Controller;

trait FolderUtility
{
	/**
	 * Parcours tous les dossiers et sauvegarde les tableaux des enfants de ceux sélectionnés par l'utilisateur
	 * @param Array $pFolderList La liste complète de tous les dossiers
	 * @param Array $pFolderChildren Les dossiers sélectionnés
	 * @param Boolean $pKeepParent Garder les parents dans le tableau de retours ou ne renvoyer que les enfants
	 * @return Array $pReturn
	 */
	static function getChildren(Array $pFolderList, Array $pSelectedFolders, bool $pKeepParent = false, Array $pReturn = [])
	{
		foreach ($pFolderList as $folder) {
			$dontGoDeeper = false;
			if(is_object($folder) && property_exists($folder, 'children'))
				$children = $folder->children;
			elseif(is_array($folder) && array_key_exists('children', $folder))
				$children = $folder['children'];
			else
				$children = [];

			if (in_array($folder['id'], $pSelectedFolders)) {
				if(!$pKeepParent)
					$pReturn[] = $children;
				else
					$pReturn[] = $folder;
				$dontGoDeeper = true; // on évite d'aller plus profond innutilement, tous les enfants sont déjà enregistrés
			}
			if (!$dontGoDeeper) {
				$pReturn = (self::getChildren($children, $pSelectedFolders, $pKeepParent, $pReturn));
			}
		}
		return $pReturn;
	}
}
