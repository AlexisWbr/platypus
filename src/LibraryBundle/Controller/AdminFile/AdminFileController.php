<?php
namespace src\LibraryBundle\Controller\AdminFile;
use Platypus\Controller\Controller;

use src\LibraryBundle\Controller\FolderUtility;
use src\LibraryBundle\Entity\FileEntity;
use src\LibraryBundle\Entity\FolderEntity;

class AdminFileController extends Controller
{
	// ajouter une pagination
	// ajouter un tri par type
	// recadrer à l'hébergement

	use FolderUtility;

	/**
	 * Page d'index de l'administration de la bibliothèque de fichiers
	 */
	function indexAction()
	{
		$this->Tool['Secure']->generateCSRFToken('delFileToken', true);

		$fileModel		= $this->getModel('File');
		$folderModel	= $this->getModel('Folder');

		$fileList			= $fileModel->getManyByFolderId($this->getRequest()->getData('folder'), '*', "ORDER BY name");
		$folderList 		= $folderModel->getAllNested('*') ?? [];
		$currentFolderList	= $folderModel->getManyByParentId($this->getRequest()->getData('folder'));

		// breadcrumb
		$folderBreadCrumb = [];
		$walkInBreadCrumb = function($pNeedle, $tree, &$result) use(&$walkInBreadCrumb) {
			if (is_array($tree)) {
				foreach ($tree as $node) {
					if ($node['id'] == $pNeedle) {
						$result[$node['id']] = $node['name'];
						return true;
					} else if (!empty($node->children)) {
						if ($walkInBreadCrumb($pNeedle, $node->children, $result)){
						$result[$node['id']] = $node['name'];
						return true;
						}
					}
				}
			} else {
				if ($tree == $pNeedle) {
					$result[] = $tree;
					return true;
				}
			}
			return false;
		}; $walkInBreadCrumb($this->getRequest()->getData('folder'), $folderList, $folderBreadCrumb);

		//couleurs des dossiers
		$folderColors = [
			1 => 'Aucune',
			2 => 'Orange',
			3 => 'Rouge',
			4 => 'Violet',
			5 => 'Bleu foncé',
			6 => 'Bleu clair',
			7 => 'Vert Foncé',
			8 => 'Vert clair'
		];

		$this->addToPage('fileList', $fileList);
		$this->addToPage('folderList', $folderList);
		$this->addToPage('currentFolderList', $currentFolderList);
		$this->addToPage('folderBreadCrumb', array_reverse($folderBreadCrumb, true));
		$this->addToPage('folderColors', $folderColors);
	}


	/**
	 * POST - Ajouter un fichier
	 */
	function addFilepostAction()
	{
		$fileModel	= $this->getModel('File');
		$files		= $this->getRequest()->fileData('file');
		$files		= !is_null($files) ? $files : [];
		$filesOrd	= [];

		foreach ($files as $propName => $filesProp) {
			foreach ($filesProp as $key => $value) {
				$filesOrd[$key][$propName] = $value;
			}
		}
		
		foreach ($filesOrd as $file)
		{
			if ($this->Tool['Files']->verifAndUploadFile($file, ['ban' => array('php, dll')]))
			{
				$newFile = new FileEntity();
				$ext	 = strtolower(pathinfo($file['name'])['extension']);

				// on créé un nom de fichier sluguifié
				$newFile->setName($this->Tool['Text']->slugify(pathinfo($file['name'])['filename']));
				$newFile->setType($ext);
				$newFile->setFolder_id($_POST['folder_id']);

				if ($newFile->isValid())
				{
					// si existe déja un fichier du même nom on renomme en incrémentant le nouveau
					$nbrFilesWithSameName = $fileModel->countByName($newFile['name'].'.'.$ext);
					$inc = 1;
					while ($nbrFilesWithSameName != 0) {
						$inc++;
						$newFileNameIncremented = $newFile['name'].'-'.$inc;
						$nbrFilesWithSameName = $fileModel->countByName($newFileNameIncremented.'.'.$ext);
					}
					if ($inc != 1)
						$newFile->setName($newFileNameIncremented);

					// ajoute l'extension du fichier dans le nom
					$newFile->setName($newFile['name'].'.'.$ext);
					// enregistre les infos en bdd
					$fileModel->insertOne($newFile);

					// enregistre l'image sur le server
					$pathofUpload = __DIR__.'/../../../../web/uploads/library/'.$newFile['type'];
					$this->Tool['Files']->uploadFile($file, $pathofUpload, $newFile['name']);

					// créé une miniature si image
					if ($this->Tool['Files']->verifAndUploadFile($file, ['allow' => array('png', 'jpeg', 'jpg', 'gif')]))
					{
						if (!$this->Tool['Files']->resizeAndUploadImage($pathofUpload.'/'.$newFile['name'], 250, 250, 'thumb_'.$newFile['name'], $pathofUpload))
							$this->Tool['User']->setFlash('Aucune miniature n\'a pu être créée, vérifiez que l\'image soit d\'extension png jpg ou gif', 'error');
					}

					$this->Tool['User']->setFlash("Fichier uploadé avec succès", 'success');
				}
				else
				{
					foreach ($newFile->getErrors() as $key => $value)
						$this->Tool['User']->setFlash($value, 'error');

					if ($file['error'] != 0)
						$this->Tool['User']->setFlash("L'envoi du fichier a échoué", 'error');
				}
			}
			else
				$this->Tool['User']->setFlash('Envoi impossible, vérifiez que le fichier respecte les conditions', 'error');
		}

			if (!empty($_POST['folder_id']))
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index', [], ['folder' => $_POST['folder_id']]));
			else
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
	}


	/**
	 * POST - Ajouter un dossier
	 */
	function addFolderPOSTAction()
	{
		$folderModel = $this->getModel('Folder');

		$newFolder	 = new FolderEntity($_POST, true);

		if ($newFolder->isValid()) {
			$folderModel->insertOne($newFolder);
			if (!empty($_POST['parent_id']))
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index', [], ['folder' => $_POST['parent_id']]));
			else
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
		}
		else {
			foreach ($newFolder->getErrors() as $key => $value)
				$this->Tool['User']->setFlash($value, 'error');
		}
	}


	/**
	 * POST - Changer des fichiers de dossier
	 */
	function changeFilesFolderPOSTAction()
	{
		$fileModel		= $this->getModel('File');
		$folderModel	= $this->getModel('Folder');

		$folderList		= $folderModel->getAllNestedInArray('id, parent_id');
		$filestoMove	= $this->getRequest()->postData('filesSelected', []);
		$folderstoMove	= $this->getRequest()->postData('foldersSelected', []);
		$folderId		= $_POST['folderId'] == 0 ? null : $_POST['folderId'];
		$currentFolder	= empty($_POST['currentFolder']) ? null : $_POST['currentFolder'];

		if (!in_array($folderId, $this->Tool['Array']->flatten(self::getChildren($folderList, $folderstoMove, true), 'id'))) {
			if (!empty($filestoMove))
				$fileModel->updateFolderIdByIds($folderId, implode(',', $filestoMove));
			if (!empty($folderstoMove))
				$folderModel->updateParentIdByIds($folderId, implode(',', $folderstoMove));
		} else {
			$this->Tool['User']->setFlash('Un dossier ne peut pas être enfant de lui même', 'error');
		}

		if ($_POST['currentFolder'] != 0)
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index', [], ['folder' => $_POST['currentFolder']]));
		else
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
	}


	/**
	 * POST - Mettre à jour les infos d'un dossier
	 */
	function editFolderPOSTAction()
	{
		$folderModel = $this->getModel('Folder');

		$folder	 = new FolderEntity($_POST, true);

		if ($folder->isValid()) {
			$folderModel->updateOne($folder);
		}
		else {
			foreach ($folder->getErrors() as $key => $value)
				$this->Tool['User']->setFlash($value, 'error');
		}

		if ($this->getRequest()->postData('currentFolder', 0) != 0)
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index', [], ['folder' => $this->getRequest()->postData('currentFolder')]));
			else
				$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
	}
}
