<h1>Bibliothèque de fichiers</h1>

<div class="containerMain" style="position: relative">
	<h2>Ajouter un fichier</h2>

	<form class="u-form containerSimple dodgerblue containerUploadFiles popWaveContainer" method="POST" enctype="multipart/form-data" action="<?= $this->Helper['Routing']->url('libe_adminFile_addFilepost') ?>">
		<label class="u-w100" for="file" style="display: inline-block">
			<input type="file" name="file[]" accept="*" multiple required>
		</label><br />
		<input type="hidden" name="folder_id" value="<?= $this->getRequest()->getData('folder') ?>" />
		<button class="btnColor6 btnMedium u-none JSloaderOnClick popWaveTrigger" type="submit">Uploader</button>
		<p>Poids max : 2M</p>
		<div class="popWave"></div>
	</form>

	<div class="u-container-fluid">
		<div class="u-row">
			<!-- LISTE DES DOSSIERS SIDEBAR -->
			<div class="col-md-5 containerSimple folderList">
				<ul>
					<li class="<?= !isset($_GET['folder']) ? 'current' : '' ?>"><a href="<?= $this->Helper['Routing']->url('lib_adminFile_index') ?>">Racine</a>
						<ul>
							<?php $recursiveOutput = function($folderList,$html)use(&$recursiveOutput) { ?>
								<?php foreach($folderList as $folder) : ?>
									<li id="alist_<?= $folder['id'] ?>" class="<?= $this->getRequest()->getData('folder') == $folder['id'] ? 'current' : '' ?>">
											<a href="?folder=<?= $folder['id'] ?>"><?= $folder['name'] ?></a>
										<?php if (isset($folder->children)) : ?>
											<ul>
												<?php $recursiveOutput($folder->children,$html) ?>
											</ul>
										<?php endif ?>
									</li>
								<?php endforeach?>
							<?php } ?>
							<?php $recursiveOutput($folderList,$this) ?>
						</ul>
					</li>
				</ul>
			</div>
			<!-- /LISTE DES DOSSIERS SIDEBAR -->

			<!-- LISTE DES FICHIERS -->
			<div class="col-md-19 containerSimple">
				<!-- breadcrumb -->
				<div class="breadCrumb library">
					<span><a href="<?= $this->Helper['Routing']->url('lib_adminFile_index') ?>"><i class="fa fa-sitemap"></i></a></span>
					<?php foreach($folderBreadCrumb as $key => $crumb) : ?>
						<span><i class="fa fa-folder"></i> <a href="?folder=<?= $key ?>"><?= $crumb ?></a></span>
					<?php endforeach ?>
				</div>
				<!-- /breadcrumb -->
 				
 				<!-- boutons -->
				<?php if(!empty($fileList) || !empty($currentFolderList)) : ?>
					<button class="JSchangeActionForm btnColor3 btnGreyFade btnPictoLarge" type="submit"
						title="Supprimer les fichiers sélectionnés" data-formAction="<?= $this->Helper['Routing']->url('lib_adminFile_delFile') ?>" data-formId="form_manageFiles">
						<i class="fa fa-trash"></i>
					</button>
					<button class="modalTrigger btnColor1 btnGreyFade btnPictoLarge" data-modal="#changeFilesFolderModal" title="Déplacer les fichiers sélectionnés">
						<i class="fa fa-stack-overflow"></i>
					</button>
				<?php endif ?>
				<button class="modalTrigger btnColor1 btnGreyFade btnPictoLarge" data-modal="#addFolderModal" title="Ajouter un dossier ici">
					<i class="fa fa-folder-o"></i>
					<i class="fa fa-plus" style="float:right;font-size:11px;margin:0 1px;"></i>
				</button>
 				<!-- /boutons -->

				<?php if(!empty($fileList) || !empty($currentFolderList)) : ?>

					<form method="POST" action="" id="form_manageFiles" onSubmit="return confirm('Confirmer l\'action ?');">
						<!-- DOSSIERS -->
						<?php foreach($currentFolderList as $folder) : ?>
							<div class="fileContainer">
								<input class="checkbox" type="checkbox" name="foldersSelected[]" value="<?= $folder['id'] ?>" />
								<a href="?folder=<?= $folder['id'] ?>" title="<?= $folder['name'] ?>">
									<div class="fileContain">
										<div class="folder folderColor<?= $folder['color'] ?>"></div>
									</div>
								</a>
								<div class="fileName"><?= $folder['name'] ?></div>

								<span class="modalTrigger link l2 fa-flip-horizontal" data-modal="#editFolder<?= $folder['id'] ?>Modal" title="Modifier">
									<i class="fa fa-wrench"></i>
								</span>
							</div>
						<?php endforeach ?>
						<!-- /DOSSIERS -->

						<!-- FICHIERS -->
						<?php foreach($fileList as $file): ?>

							<?php if (in_array($file['type'], array('png','jpeg','jpg','gif'))) : ?>
								<?php $backgUrlThumb = $this->getRequest()->root().'/web/uploads/library/'.$file['type'].'/thumb_'.$file['name'] ?>
							<?php elseif(in_array($file['type'], array('bmp','ico'))): ?>
								<?php $backgUrlThumb = $this->getRequest()->root().'/web/uploads/library/'.$file['type'].'/'.$file['name'] ?>
							<?php else : ?>
								<?php $backgUrlThumb = false; ?>
							<?php endif ?>

							<div class="fileContainer">
								<input class="checkbox" type="checkbox" name="filesSelected[]" value="<?= $file['id'] ?>" />
								<a href="<?= $this->getRequest()->root() ?>/web/uploads/library/<?= $file['type'] ?>/<?= $file['name'] ?>" title="<?= $file['name'] ?>" target="_blank">
									<?php if ($backgUrlThumb) : ?>
										<div class="fileContain" style="background: url('<?= $backgUrlThumb ?>') 50% 50% no-repeat; background-size: cover;">
										</div>
									<?php else : ?>
										<div class="fileContain">
										<div>
											<?php if(in_array($file['type'], array('shar','iso','7z','jar','rar','war,','zip','zipx'))) : ?>
												<i class="fa fa-file-archive-o"></i>
											<?php elseif(in_array($file['type'], array('js','html','htm','json','xml','yml,','yaml','css'))) : ?>
												<i class="fa fa-file-code-o"></i>
											<?php elseif(in_array($file['type'], array('pdf'))) : ?>
												<i class="fa fa-file-pdf-o"></i>
											<?php elseif(in_array($file['type'], array('doc','docm','docx','dot','dotm','dotx'))) : ?>
												<i class="fa fa-file-word-o"></i>
											<?php elseif(in_array($file['type'], array('xls','xlsb','xlsm','xlsx','xlt','xltm','xltx'))) : ?>
												<i class="fa fa-file-excel-o"></i>
											<?php elseif(in_array($file['type'], array('pot','pps','ppt','pptx'))) : ?>
												<i class="fa fa-file-powerpoint-o"></i>
											<?php elseif(in_array($file['type'], array('txt'))) : ?>
												<i class="fa fa-file-text-o"></i>
											<?php elseif(in_array($file['type'], array('flac','cdr','mp3','ogg','wma'))) : ?>
												<i class="fa fa-file-audio-o"></i>
											<?php elseif(in_array($file['type'], array('avi','wmv','mp4','flv'))) : ?>
												<i class="fa fa-film"></i>
											<?php else : ?>
												<i class="fa fa-file-o"></i>
											<?php endif ?>
										</div>
										</div>
									<?php endif ?>
								</a>
								<div class="fileName"><?= $file['name'] ?></div>

								<span class="link JScopyOnClick" title="Copier le lien" value="<?= $this->getRequest()->root() ?>/web/uploads/library/<?= $file['type'] ?>/<?= $file['name'] ?>">
									<i class="fa fa-link"></i>
								</span>
								<!-- <span class="link l2 fa-flip-horizontal" title="Modifier">
									<i class="fa fa-wrench"></i>
								</span> -->
							</div>

						<?php endforeach ?>
						<!-- /FICHIERS -->

						<input type="hidden" name="currentFolder" value="<?= $this->getRequest()->getData('folder') ?>" />
						<input type="hidden" name="delFileToken" value="<?= $_SESSION['CRSF']['delFileToken']['token'] ?>" />
					</form>

				<?php else : ?>
					<div class="blockDashed">
						Aucun fichier n'est hébergé pour le moment
					</div>
				<?php endif ?>
			</div>
			<!-- /LISTE DES FICHIERS -->
		</div>


		<!-- MODALE AJOUTER UN DOSSIER -->
		<div id="addFolderModal" class="u-modalContainer">
			<div class="u-modalContain containerSimple">
				<h2>Ajouter un dossier</h2>
				<form method="POST" class="u-form" action="<?= $this->Helper['Routing']->url('lib_adminFile_addFolderPOST') ?>">
					<label for="name" class="u-w100">
						<span>Nom du dossier</span>
						<input type="text" name="name" id="name" required />
					</label>
					<?php foreach ($folderColors as $key => $name) : ?>
						<label for="color<?= $key ?>">
							<input type="radio" name="color" id="color<?= $key ?>" value="<?= $key ?>" <?= $key == 1 ? 'checked' : '' ?> />
							<span class="libraryFolderColor<?= $key ?>"></span>
							<?= $name ?>
						</label>
					<?php endforeach ?>

					<input type="hidden" name="parent_id" value="<?= $this->getRequest()->getData('folder') ?>" />
					<input type="submit" value="Enregistrer" class="btnSmall btnColor1" />
				</form>
			</div>
		</div>
		<!-- /MODALE AJOUTER UN DOSSIER -->

		<!-- MODALE CHANGER DES FICHIERS DE DOSSIER -->
		<div id="changeFilesFolderModal" class="u-modalContainer">
			<div class="u-modalContain containerSimple">
				<h2>Déplacer les fichiers</h2>
				<div class="u-form">
					<label class="u-align" for="folderId">
						<span>Déplacer dans</span>
						<select form="form_manageFiles" name="folderId" id="folderId">
							<option value="0">Racine</option>
							<?php function recursiveOutput($array, $actualItem, $i=0) { ?>
									<?php ++$i; ?>
									<?php foreach ($array as $item) : ?>
										<option value="<?= $item['id'] ?>">
											<?php for($j=1;$j<$i;$j++) echo "&nbsp;" ?>
											<?php if ($i!=1) echo "└" ?>
											<?= $item['name'] ?>
										</option>
										<?php if (isset($item->children)) : ?>
											<div>
												<?php recursiveOutput($item->children, $actualItem, $i) ?>
											</div>
										<?php endif ?>
									<?php endforeach ?>
									<?php $i=0; ?>
							<?php } ?>
							<?php recursiveOutput($folderList, $this->getRequest()->getData('folder')) ?>
						</select>
					</label>
				</div>

				<button class="JSchangeActionForm btnSmall btnColor1" type="submit"
					title="Déplacer les fichiers sélectionnés" data-formAction="<?= $this->Helper['Routing']->url('lib_adminFile_changeFilesFolderPOST') ?>" data-formId="form_manageFiles">
					Enregistrer
				</button>
			</div>
		</div>
		<!-- /MODALE CHANGER DES FICHIERS DE DOSSIER -->

		<!-- MODIFIER LES DETAILS D'UN DOSSIER -->
		<?php foreach($currentFolderList as $folder) : ?>
			<div id="editFolder<?= $folder['id'] ?>Modal" class="u-modalContainer">
				<div class="u-modalContain containerSimple">
					<h2>Modifier le dossier</h2>
					<form method="POST" class="u-form" action="<?= $this->Helper['Routing']->url('lib_adminFile_editFolderPOST') ?>">
						<label for="name" class="u-w100">
							<span>Nom du dossier</span>
							<input type="text" name="name" id="name" value="<?= $folder['name'] ?>" />
						</label>
						<label>
							<span>Couleur du dossier</span>
							<?php foreach ($folderColors as $key => $name) : ?>
								<label for="color2nd<?= $key ?>">
									<input type="radio" name="color" id="color2nd<?= $key ?>" value="<?= $key ?>" <?= $key == $folder['color'] ? 'checked' : '' ?> />
									<span class="libraryFolderColor<?= $key ?>"></span>
									<?= $name ?>
								</label>
							<?php endforeach ?>
						</label>

						<input type="hidden" name="id" value="<?= $folder['id'] ?>" />
						<input type="hidden" name="currentFolder" value="<?= $this->getRequest()->getData('folder') ?>" />
						<button type="input" class="btnSmall btnColor1">Enregistrer</button>
					</form>
				</div>
			</div>
		<?php endforeach ?>
		<!-- MODIFIER LES DETAILS D'UN DOSSIER -->
	</div>
</div>
