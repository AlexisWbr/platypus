<?php
namespace src\LibraryBundle\Controller\AdminFile;
use Platypus\Controller\Controller;

use src\LibraryBundle\Controller\FolderUtility;

class AjaxController extends Controller
{
	use FolderUtility;

	/**
	 * Supprimer un fichier
	 */
	function delFileAction()
	{
		if (!$this->Tool['Secure']->checkCSRFToken('delFileToken', 'POST', true))
		{
			$this->Tool['User']->setFlash("Impossible d'effectuer l'opération demandée, jeton expiré", 'error');
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
		}

		$fileModel		= $this->getModel('File');
		$folderModel	= $this->getModel('Folder');

		// formulaire
		$filesToDelete	 = $this->getRequest()->postData('filesSelected', []);
		$foldersToDelete = $this->getRequest()->postData('foldersSelected', []);

		// tous les dossiers existants de la bdd
		$folderList		 = !empty($foldersToDelete) ? $folderModel->getAllNestedInArray('id, parent_id') : [];

		// sauvegarde les dossiers enfants de ceux sélectionnés par l'utilisateur
		$folderListChildren = $this->Tool['Array']->flatten(self::getChildren($folderList, $foldersToDelete), 'id');

		// ajoute les fichiers issus des dossiers à supprimer, aux fichiers à supprimer sélectionnés par l'utilisateur
		$foldersIds = array_merge($foldersToDelete, $folderListChildren);
		if (!empty($foldersIds)) {
			$filesToDeleteFromFoldersO	= $fileModel->getAll('id', 'WHERE folder_id IN ('.implode(',', $foldersIds).')');
			$filesToDeleteFromFolders	= [];
			foreach ($filesToDeleteFromFoldersO as $file) {
				$filesToDeleteFromFolders[] = $file['id'];
			}
			$filesToDelete = array_merge($filesToDelete, $filesToDeleteFromFolders);
		}

		foreach ($filesToDelete as $id) {
			$oldFile			= $fileModel->getOneById($id);
			$pathofFile			= __DIR__.'/../../../../web/uploads/library/'.$oldFile['type'].'/'.$oldFile['name'];
			$pathofFileThumb	= __DIR__.'/../../../../web/uploads/library/'.$oldFile['type'].'/thumb_'.$oldFile['name'];

			if (file_exists($pathofFile))
				unlink($pathofFile);
			if (file_exists($pathofFileThumb))
				unlink($pathofFileThumb);
		}
		if (!empty($filesToDelete))
			$fileModel->deleteAll('WHERE id IN ('.implode(',', $filesToDelete).')');
		if (!empty($foldersToDelete))
			$folderModel->deleteAll('WHERE id IN ('.implode(',', $foldersToDelete).')');

		$this->Tool['User']->setFlash('Fichiers supprimés', 'success');
		if ($_POST['currentFolder'] != 0)
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index', [], ['folder' => $_POST['currentFolder']]));
		else
			$this->getResponse()->redirect($this->getRequest()->root().$this->getRouter()->generatePath('lib_adminFile_index'));
	}
}
