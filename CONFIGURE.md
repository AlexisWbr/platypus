Configurer Apache et PHP
========================

Utiliser PHP en console
-----------------------

cmd `php -v`

Si erreur

* paramètres système avancés `Démarrer > Panneau de configuration > Système et sécurité > Système > Paramètres système avancés`
* bouton `Variables d'environnement`
* panneau `Variables système`
* entrée `Path` - double clique
* Entrez le répertoire PHP à la fin, sans oublier le point-virgule ";" auparavant. C'est le répertoire dans lequel se trouve le fichier php.exe. Par exemple `;C:\wamp\bin\php\php5.5.12`
* Confirmer les changements puis redémarrer l'invite de commandes


Configurations d'Apache
-----------------------

Repertoire: `C:\wamp\bin\apache\apachex.x.x\bin` - `php.ini`

### Ajouter/modifier le xdebug :

    ; XDEBUG Extension

    zend_extension = "c:/wamp/bin/php/php5.5.12/zend_ext/php_xdebug-2.2.5-5.5-vc11.dll"
    ;
    [xdebug]
    xdebug.remote_enable = off
    xdebug.profiler_enable = off
    xdebug.profiler_enable_trigger = off
    xdebug.profiler_output_name = cachegrind.out.%t.%p
    xdebug.profiler_output_dir = "c:/wamp/tmp"
    xdebug.show_local_vars=0
    xdebug.max_nesting_level=500

### Pour enlever les limites des var_dump rajouter :

    xdebug.var_display_max_depth = -1 
    xdebug.var_display_max_children = -1
    xdebug.var_display_max_data = -1


Créer un VirtualHost
====================

Modifier le dossier d'index sur Apache
--------------------------------------

Repertoire: `C:\wamp\bin\apache\apachex.x.x\conf` - `httpd.conf`

    NameVirtualHost 127.0.0.1
    <VirtualHost 127.0.0.1> 
        DocumentRoot "C:\wamp\www"
        ServerName localhost
    </VirtualHost>

    <VirtualHost sitename>
        DocumentRoot "C:\wamp\www\sitename"
        ServerName sitename
        ServerAlias sitename.com
        ServerAlias www.sitename.com
        <Directory "C:\wamp\www\sitename">
            Options Indexes FollowSymLinks Includes ExecCGI
            AllowOverride All
            Order allow,deny
            Allow from all
       </Directory>
    </VirtualHost>

Modifier en conséquence `DocumentRoot` - `ServerName` - `ServerAlias` - `Directory`
Si erreur penser à décommenter/rajouter la ligne suivante :

`LoadModule rewrite_module modules/mod_rewrite.so`


Ajouter le site aux Hosts
-------------------------

Repertoire: `C:\Windows\System32\drivers\etc` - `hosts`

    127.0.0.1       sitename.com
    127.0.0.1       www.sitename.com
