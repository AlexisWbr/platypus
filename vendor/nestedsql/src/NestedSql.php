<?php

/**
 * (c) Anna Filina <anna@foolab.ca>
 * Modified by Aigzy <aigzy@codemixture.com>
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

class NestedSql
{
	/**
	 * @param $statement Executed sql
	 * @param $classes Array Classes where to store values
	 */
	static function nestsResultsRecursive($statement, $classes = [])
	{
		// Ids enable us to list columns on the right objects.
		$dictionary = [];

		$create_class = function($alias) use ($classes)
		{
			if (array_key_exists($alias, $classes)) {
				return new $classes[$alias]();
			}
			return new stdClass();
		};

		$add_row = function($row, &$parent, &$dictionary) use ($create_class)
		{
			foreach ($row as $alias => $value) {
				if ($value === null) {
					return;
				}

				$alias_parts = explode('__', $alias);
				if (count($alias_parts) < 2) {
					throw new Exception('Please format aliases according to the documentation: https://github.com/afilina/nestedsql/blob/master/README.md');
				}

				$id_alias = implode('__', array_slice($alias_parts, 0, -1)).'__id';
				$dict_key = "{$id_alias}/{$row[$id_alias]}";

				foreach ($alias_parts as $i => $alias_part) {
					// Create new item, register in dictionary and add to list.
					if ($alias_part == 'id') {
						$list_property = $alias_parts[count($alias_parts)-2];
						$parent_alias = implode('__', array_slice($alias_parts, 0, -2)).'__id';
						if ($parent_alias != '__id') {
							$parent_dict_key = "{$parent_alias}/{$row[$parent_alias]}";
							$list_parent = $dictionary[$parent_dict_key];
						} else {
							$list_parent = $parent;
						}
						if (!property_exists($list_parent, $list_property) || $list_parent->$list_property == null) {
							$list_parent->$list_property = [];
						}
						if (!array_key_exists($dict_key, $dictionary)) {
							$list_element = $dictionary[$dict_key] = $create_class($list_property);
						} else {
							$list_element = $dictionary[$dict_key];
						}
						$list = $list_parent->$list_property;
						$list[$row[$id_alias]] = $list_element;
						$list_parent->$list_property = $list;
					}
					// Assign properties to object
					if ($i == count($alias_parts) - 1) {
						if (get_class($dictionary[$dict_key]) == 'stdClass') {
							$dictionary[$dict_key]->$alias_part = $value;
						} else {
							$alias_part = 'set'.ucfirst($alias_part);
							$dictionary[$dict_key]->$alias_part($value);
						}
					}
				}
			}
		};

		$parent = new stdClass();
		while($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
			$add_row($row, $parent, $dictionary);
		}

		$statement->closeCursor();
		return $parent;
	}


	/**
	 * By Aigzy <aigzy@codemixture.com>
	 * @param $req Le retour de la requete sql
	 * @param String $idRef Nom de la clef de l'id des entrées
	 * @param String $parentIdRef Nom de la clef relative aux parents des entrées enfants
	 * @return Array Retourne un tableau associatif ordonné selon les clefs parents/enfants
	 */
	static function nestsResults($req, $idRef, $parentIdRef)
	{
		$nested = $map = $stack = array();	

		$update_data = function($string, &$nested, $row) use($idRef) {
			$ref = &$nested;
			foreach ($string as $name) {
				if($name != 'children') {
					$ref = &$ref[$name];
				} else {
					if(is_object($ref))
						$ref = &$ref->children;
					else
						$ref = &$ref['children'];
				}
			}
			if(is_object($ref))
				$ref->children[$row[$idRef]] = $row;
			else
				$ref['children'][$row[$idRef]] = $row;
		};

		$register = function(&$map, &$nested, $row) use($idRef, $parentIdRef, $update_data) {
			$update_data($map[$row[$parentIdRef]], $nested, $row);
			$map[$row[$idRef]]		= $map[$row[$parentIdRef]];
			$map[$row[$idRef]][]	= 'children';
			$map[$row[$idRef]][]	= $row[$idRef];
		};

		while($row = $req->fetch()) {
			// si pas de parent, on l'inscrit direct
			if(is_null($row[$parentIdRef])) {
				$nested[$row[$idRef]]	= $row;
				$map[$row[$idRef]][]	= $row[$idRef];
			}
			// si est un enfant
			else {
				// si le parent est bien déjà répertorié dans la map on enregistre
				if(isset($map[$row[$parentIdRef]]))
					$register($map, $nested, $row);
				else // sinon on met en attente la row
					$stack[$row[$idRef]]	= $row;
			}
		}

		$prevStack = $stack;
		while(!empty($stack)) {
			foreach ($stack as $key => $row) {
				if(isset($map[$row[$parentIdRef]])) {
					$register($map, $nested, $row);
					unset($stack[$key]);
				}
			}
				if ($prevStack === $stack) {
					$backTrace = debug_backtrace();
					throw new \LogicException('Infinite loop detected, one of the rows seems to be self parent - child produced by '.$backTrace[1]['file'].' on line '.$backTrace[1]['line'], 1);
				}
				$prevStack = $stack;
		}
		return $nested;
	}


	/**
	 * /!\ === DO NOT USE ===  /!\
	 * By Aleks Gekht aka Aleks G on Stackoverflow
	 * Modified by Aigzy <aigzy@codemixture.com>
	 */
	private function nestsResultsOLD($req, $idRef, $parentIdRef, $pObjectOrArray)
	{
		$elems = array();

		while($row = $req->fetch()) {
			$row['children'] = array();
			$vn = "row" . $row[$idRef];
			${$vn} = $row;
			if(!is_null($row[$parentIdRef])) {
				$vp = "parent" . $row[$parentIdRef];
				if(isset($data[$row[$parentIdRef]])) {
					${$vp} = $data[$row[$parentIdRef]];
				}
				else {
					${$vp} = array($idRef => $row[$parentIdRef], $parentIdRef => null, 'children' => array());
					$data[$row[$parentIdRef]] = &${$vp};
				}
				// if (!is_null($pObjectOrArray))
				if (is_object(${$vp}))
					${$vp}->children[] = &${$vn};
				else
					${$vp}['children'][] = &${$vn};
				$data[$row[$parentIdRef]] = ${$vp};
			}
			$data[$row[$idRef]] = &${$vn};
		}

		if (isset($data)) {
			$result = array_filter($data, function($elem)use($parentIdRef) { return is_null($elem[$parentIdRef]); });
			return $result;
		}
	}
}
