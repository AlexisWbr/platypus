<?php
require_once(__DIR__.'/../symfony/ClassLoader/ClassLoader.php');
require_once(__DIR__.'/../symfony/ClassLoader/Psr4ClassLoader.php');
require_once(__DIR__.'/../symfony/ClassLoader/MapClassLoader.php');

use Symfony\Component\ClassLoader\ClassLoader;
use Symfony\Component\ClassLoader\Psr4ClassLoader;
use Symfony\Component\ClassLoader\MapClassLoader;

/*=================================================================*/
/*========================PSR0 CLASS LOADER========================*/

/*=================================================================*/
/*========================PSR4 CLASS LOADER========================*/

$loader = new Psr4ClassLoader();
// Platypus
$loader->addPrefix('Platypus', __DIR__);
$loader->addPrefix('app', __DIR__.'/../../app');
$loader->addPrefix('src', __DIR__.'/../../src');
// vendor
$loader->addPrefix('Symfony\\Component\\Yaml\\', __DIR__.'/../symfony/yaml');
$loader->register();

/*=================================================================*/
/*=========================MAP CLASS LOADER========================*/

$mapping = array(
	// Mandrill
	'Mandrill' => __DIR__.'/../Mandrill/Mandrill/src/Mandrill.php',
	'Mandrill_Messages' => __DIR__.'/../Mandrill/Mandrill/src/Mandrill/Messages.php',
	'Mandrill_Error' => __DIR__.'/../Mandrill/Mandrill/src/Mandrill/Exceptions.php',
	// NestedSql
	'NestedSql' => __DIR__.'/../nestedsql/src/NestedSql.php'
);
$loader = new MapClassLoader($mapping);
$loader->register();
