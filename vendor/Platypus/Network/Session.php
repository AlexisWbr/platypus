<?php
namespace Platypus\Network;
use Platypus\Bundle\BundleComponent;

class Session
{
	function setAttr($pAttr, $pValue)
	{
		$_SESSION[$pAttr] = $pValue;
	}

	
	function getAttr($pAttr)
	{
		return isset($_SESSION[$pAttr]) ? $_SESSION[$pAttr] : null; // retourne null si non existant
	}


	function unsetAttr($pAttr)
	{
		unset($_SESSION[$pAttr]);
	}


	function isAuthenticated()
	{
		return isset($_SESSION['user']['id']);
	}
}
