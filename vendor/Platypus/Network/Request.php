<?php
namespace Platypus\Network;

class Request
{
	/**
	 * connaître l'URI demandée
	 */
	function requestURI()
	{
		return $_SERVER['REQUEST_URI'];
	}

	/**
	 * savoir d'où vient le client
	 */
	function referer()
	{
		return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];
	}

	/**
	 * Retourne l'host du serveur
	 */
	function host($pHttp = true)
	{
		$root = $pHttp ? 'http://' : '';
		return $root.$_SERVER['HTTP_HOST'];
	}

	/**
	 * Retourne l'url root
	 * @param bool $pHttp	: ajouter ou non http:// devant l'url
	 * @return string
	 */
	function root($pHttp = true)
	{
		$root = $pHttp ? 'http://' : '';
		return $root.$_SERVER['HTTP_HOST'].$GLOBALS['KERNEL_CONFIG']['base_url'];
	}

	/**
	 * connaître la méthode de transmission des données : GET ou POST ?
	 */
	function method()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

	/**
	 * Obtenir le status de la page
	 */
	function getStatus()
	{
		return http_response_code();
	}

	/**
	 * vérifier existance paramètre GET
	 */
	function getExists($pKey)
	{
		return isset($_GET[$pKey]);
	}

	/**
	 * récuperer paramètre GET
	 */
	function getData($pKey, $pDefaultReturn = null)
	{
		if (is_null($pDefaultReturn))
			return isset($_GET[$pKey]) ? $_GET[$pKey] : null;
		elseif (is_array($pDefaultReturn) && empty($pDefaultReturn))
			return isset($_GET[$pKey]) ? $_GET[$pKey] : [];
		else
			return isset($_GET[$pKey]) ? $_GET[$pKey] : $pDefaultReturn;
	}

	/**
	 * vérifier existance paramètre POST
	 */
	function postExists($pKey)
	{
		return isset($_POST[$pKey]);
	}

	/**
	 * récuperer paramètre POST
	 */
	function postData($pKey, $pDefaultReturn = null)
	{
		if (is_null($pDefaultReturn))
			return isset($_POST[$pKey]) ? $_POST[$pKey] : null;
		elseif (is_array($pDefaultReturn) && empty($pDefaultReturn))
			return isset($_POST[$pKey]) ? $_POST[$pKey] : [];
		else
			return isset($_POST[$pKey]) ? $_POST[$pKey] : $pDefaultReturn;
	}

	/**
	 * vérifier existance d'un cookie
	 */
	function cookieExists($pKey)
	{
		return isset($_COOKIE[$pKey]);
	}

	/**
	 * récuperer un cookie
	 */
	function cookieData($pKey, $pDefaultReturn = null)
	{
		if (is_null($pDefaultReturn))
			return isset($_COOKIE[$pKey]) ? $_COOKIE[$pKey] : null;
		elseif (is_array($pDefaultReturn) && empty($pDefaultReturn))
			return isset($_COOKIE[$pKey]) ? $_COOKIE[$pKey] : [];
		else
			return isset($_COOKIE[$pKey]) ? $_COOKIE[$pKey] : $pDefaultReturn;
	}

	/**
	 * vérifier l'existence d'un fichier envoyé
	 */
	function fileExists($pKey){
		return isset($pKey);
	}

	/**
	 * Récupérer un fichier envoyé
	 */
	function fileData($pKey, $pKeyVar = null)
	{
		if (is_null($pKeyVar))
			return isset($_FILES[$pKey]) ? $_FILES[$pKey] : null;
		else
			return isset($_FILES[$pKey]) ? $_FILES[$pKey][$pKeyVar] : null;
	}

	/**
	 * Récupérer l'ip du client
	 */
	function getIp($pMethode = 'server')
	{

		if ($pMethode == 'server')
		{
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
				$ipaddress = null;
			return $ipaddress;
		}

		elseif ($pMethode == 'getenv')
		{
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
				$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
				$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
				$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
				$ipaddress = getenv('REMOTE_ADDR');
			else
				$ipaddress = null;
			return $ipaddress;
		}

		else throw new \InvalidArgumentException("L'argument doit faire reference a la methode appelee, qui doit etre 'server' ou 'getenv'", 1);
	}
}
