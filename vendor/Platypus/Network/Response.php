<?php
namespace Platypus\Network;
use Platypus\Bundle\BundleComponent;
use Platypus\View\Page;
use Platypus\View\Tool\RoutingTool;

class Response extends BundleComponent
{
	public $statusCodes = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		102 => 'Processing',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		207 => 'Multi-Status',
		208 => 'Already Reported',
		226 => 'IM Used',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		308 => 'Permanent Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Payload Too Large',
		414 => 'URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Range Not Satisfiable',
		417 => 'Expectation Failed',
		422 => 'Unprocessable Entity',
		423 => 'Locked',
		424 => 'Failed Dependency',
		425 => 'Reserved for WebDAV advanced collections expired proposal',
		426 => 'Upgrade Required',
		428 => 'Precondition Required',
		429 => 'Too Many Requests',
		431 => 'Request Header Fields Too Large',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		506 => 'Variant Also Negotiates (Experimental)',
		507 => 'Insufficient Storage',
		508 => 'Loop Detected',
		510 => 'Not Extended',
		511 => 'Network Authentication Required',
	);





	/**
	 * permet de signifier le statut HTTP à envoyer - ex: header("HTTP/1.0 404 Not Found");
	 */
	function changeHeader($pHeader)
	{
		if (!is_string($pHeader))
			throw new \InvalidArgumentException("Le header doit être une chaine de caracteres");

		header($pHeader);
	}


	/**
	 * écrire un cookie avec la réponse
	 */
	function setcookie($pName, $pValue = "", $pExpire = 0, $pPath = "/", $pDomain = null, $pSecure = false, $pHttpOnly = true)
	{
		$pExpire = $pExpire == 0 ? 0 : time() + $pExpire;
		setcookie($pName, $pValue, $pExpire, $pPath, $pDomain, $pSecure, $pHttpOnly);
	}


	/**
	 * redirection
	 */
	function redirect($pLocation)
	{
		header('Location: '.$pLocation);
		exit;
	}


	/**
	 * Envoie la page
	 */
	function send(Page $pPage)
	{
		exit ($pPage->render());
	}


	/**
	 * Rediriger vers une page d'erreur. Le code est changeant selon l'envoi et irra chercher tout seul la bonne vue
	 * String $pErrorCode Le code d'erreur
	 * Page $pPageToSend Une potentielle page a envoyer si besoin
	 */
	function sendError($pErrorCode, $pPageToSend = null)
	{
		$page = is_null($pPageToSend) ? new Page($this->bundle) : $pPageToSend;
		$this->changeHeader("HTTP/1.0 ".$pErrorCode);
		exit ($page->renderError($pErrorCode));
	}
}
