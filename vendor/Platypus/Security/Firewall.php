<?php
namespace Platypus\Security;
use Platypus\Bundle\BundleComponent;
use Platypus\Cache\Cache;
use Platypus\Network\Request;
use Platypus\Network\Response;
use Platypus\Network\Session;
use Platypus\ORM\ModelCollector;
use Platypus\Routing\Router;
use Symfony\Component\Yaml\Parser;

class Firewall
{
	protected	$config = [];		// configs issues de app/config/firewall.yaml
	protected	$router;			// utilisé pour la fonction isAuthenticated(). A voir si possibilité de ne pas avoir à utiliser le Router entier juste pour ça
	protected	$request;
	protected	$response;
	protected	$session;

	function __construct(Router $pRouter, Request $pRequest, Response $pResponse, Session $pSession)
	{
		$this->router	= $pRouter;
		$this->request	= $pRequest;
		$this->response	= $pResponse;
		$this->session	= $pSession;

		// va chercher les configurations
		$cache_fileName = 'cache_Firewallconfig';
		if (!Cache::read($this->config, $cache_fileName, false))
		{
			$yamlParser	= new Parser();
			$config		= $yamlParser->parse(file_get_contents(__DIR__.'/../../../app/config/firewall.yaml'));
			foreach ($config as $key => $value) {
				$this->config[$key] = $value;
			}
			Cache::write($this->config, $cache_fileName);
		}
	}





	/**
	 * Si pas encore connecté on redirige vers le formulaire de connexion
	 * on ajoute le referer dans la session pour qu'il soit atteignable
	 * (autrement le referer est "effacé" donc impossible de rediriger sur la page d'où le firewall à stoppé l'utilisateur)
	 */
	private function isAuthenticated()
	{
		if (!$this->session->isAuthenticated()) {
			$_SESSION['referer'] = $this->request->referer();
			$this->response->redirect($this->request->root().$this->router->generatePath($this->config['loginPath']));
		}
	}


	/**
	 * @param String/Array $pRanks : Les rangs que doit posséder l'utilisateur pour que le retour soit true
	 * @param Array $pRanksExtended : Utiliser un tableau de rangs différents en plus de celui par défaut stocké en session
	 * @param Boolean $pExhaustive : Tous les rangs doivent être possédés par l'utilisateur
	 * @return Boolean $gotTheRank : Oui ou non l'utilisateur a les rangs demandés
	 */
	function hasRank($pRanks, Array $pRanksExtended = null, $pExhaustive = false)
	{
		$this->isAuthenticated();


		$pRanks		= is_array($pRanks) ? $pRanks : [0=>$pRanks];
		// détecte si utilise les clefs comme comparaison
		$useKey		= $pRanks === array_filter($pRanks, 'is_numeric');
		$userRanks	= is_array($_SESSION['user']['ranks']) ? $_SESSION['user']['ranks'] : [];
		$userRanks	= !is_null($pRanksExtended) ? array_merge($userRanks, $pRanksExtended) : $userRanks;
		if (!$useKey)
			$ownedAskingRanks	= array_intersect($pRanks, $userRanks);
		else
			$ownedAskingRanks	= array_intersect($pRanks, array_flip($userRanks));


		if ($pExhaustive) {
			$gotTheRank = $ownedAskingRanks == $pRanks;
		} else {
			$gotTheRank = !empty($ownedAskingRanks);
		}

		if ($gotTheRank) return true;
		else $this->response->sendError(403);
	}


	/**
	 * @param String/Array $pRanks : Les rangs que ne doit pas posséder l'utilisateur pour que le retour soit true
	 * @param Array $pRanksExtended : Utiliser un tableau de rangs différents en plus de celui par défaut stocké en session
	 * @param Boolean $pExhaustive : Aucun des rangs tous ensembles ne doivent être possédés par l'utilisateur
	 * @return Boolean $gotTheRank : Oui ou non l'utilisateur n'a pas les rangs demandés
	 */
	function hasNotRank($pRanks, Array $pRanksExtended = null, $pExhaustive = false)
	{
		$this->isAuthenticated();


		$pRanks		= is_array($pRanks) ? $pRanks : [0=>$pRanks];
		// détecte si utilise les clefs comme comparaison
		$useKey		= $pRanks === array_filter($pRanks, 'is_numeric');
		$userRanks	= is_array($_SESSION['user']['ranks']) ? $_SESSION['user']['ranks'] : [];
		$userRanks	= !is_null($pRanksExtended) ? array_merge($userRanks, $pRanksExtended) : $userRanks;
		if (!$useKey)
			$ownedAskingRanks	= array_intersect($pRanks, $userRanks);
		else
			$ownedAskingRanks	= array_intersect($pRanks, array_flip($userRanks));

		if ($pExhaustive) {
			$gotTheRank = $ownedAskingRanks != $pRanks;
		} else {
			$gotTheRank = empty($ownedAskingRanks);
		}

		if ($gotTheRank) return true;
		else $this->response->sendError(404);
	}
}
