<?php
namespace Platypus\Bundle;

// Grace à cette classe, toute class héritant d'elle aura accès aux variables du Bundle, ce qui permet d'avoir accès aux objets
// Request, Response, Page etc...
abstract class BundleComponent
{
	protected	$bundle;

	function __construct(Bundle $pBundle){
		$this->bundle = $pBundle;
	}





	function getBundle(){
		return $this->bundle;
	}
	
	/**
	* Raccourcits pour éviter un $this->bundle->getRequest()->doSometing() dans un Controller ou une Page
	* Devient $this->getRequest()->doSometing()
	*/
	function getRequest(){
		return $this->bundle->getRequest();
	}

	function getResponse(){
		return $this->bundle->getResponse();
	}

	function getSession(){
		return $this->bundle->getSession();
	}

	function getRouter(){
		return $this->bundle->getRouter();
	}
}
