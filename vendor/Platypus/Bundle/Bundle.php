<?php
namespace Platypus\Bundle;
use Platypus\Network\Request;
use Platypus\Network\Response;
use Platypus\Network\Session;
use Platypus\Routing\Router;
use Platypus\Routing\Route;

abstract class Bundle
{
	protected	$name;
	protected	$route;
	protected	$router;
	protected	$request;
	protected	$response;
	protected	$session;

	function __construct(Router $pRouter, Route $pRoute){
		$this->name = $pRoute->getBundleName();
		$this->route = $pRoute;
		$this->router = $pRouter;

		$this->request = new Request();
		$this->response = new Response($this);
		$this->session = new Session();
	}





	abstract function run();


	/**
	 * Va chercher automatiquement le bon Controller associé à la Route ou va en chercher un particulier si demandé
	 * @param String $pControllerName
	 * @return Controller $controller
	 */
	function getController($pControllerName = null)
	{
		// === si pas de demande particulière de Controller alors on récupère automatiquement le bon controller ===
		if ($pControllerName == null){
			$controllerName = ucfirst($this->route->getController());

			$potentialSubcontrollerName = !is_null($this->route->getSubController()) ? ucfirst($this->route->getSubController()) : ucfirst($this->route->getController());

			$actionName = lcfirst($this->route->getAction());
		}
		// === si demande d'un Controller particulier on utilise le nom renseigné ===
		else {
			$controllerName = ucfirst($this->route->getController());
			$potentialSubcontrollerName = preg_match('#(\w+)\.(\w+)#', $pControllerName, $matches_controllerName) ? ucfirst($matches_controllerName[2]) : ucfirst($this->route->getController());
			$actionName = null;
		}
		
		$controllerPath = '\\src\\'.$this->name.'\\Controller\\'.$controllerName.'\\'.$potentialSubcontrollerName.'Controller';

		// on tente déjà d'instancier la classe
		if (class_exists($controllerPath))
			$controller = new $controllerPath($this, $controllerName, $potentialSubcontrollerName, $actionName);
		else
			throw new \RuntimeException("Controller introuvable (fichier non cree, classe non existante ou erreur dans le chemin type : [BundleName]Bundle/Controller/[ControllerName]/[ControllerName]Controller.php ) - ".$controllerPath);

		return $controller;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}

	function getRoute(){
		return $this->route;
	}

	function getRouter(){
		return $this->router;
	}

	function getRequest(){
		return $this->request;
	}
	
	function getResponse(){
		return $this->response;
	}

	function getSession(){
		return $this->session;
	}
}
