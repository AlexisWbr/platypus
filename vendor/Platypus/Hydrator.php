<?php
namespace Platypus;

trait Hydrator
{
	/**
	 * Hydrate certaines variables d'un objet selon les entrées d'un tableau de données
	 */
	public function hydrate(array $pDonnees)
	{
		foreach ($pDonnees as $key => $value)
		{
			$methode = 'set'.ucfirst($key);

			if (is_callable([$this, $methode]))
			{
				$this->$methode($value);
			}
		}
	}


	/**
	 * Hydrate TOUTES les variables d'un objet.
	 * Si l'entrée du tableau existe on hydrate la variable de l'objet associée
	 * Si l'entrée du tableau n'existe pas et qu'on a définit une valeur par défaut pour une variable de l'objet, cette variable garde cette valeur
	 */
	public function hydrateAll(array $pDonnees)
	{
		foreach (get_object_vars($this) as $var_name => $value) {
			$methode = 'set'.ucfirst($var_name);

			if (is_callable([$this, $methode]))
			{
				if (array_key_exists($var_name, $pDonnees))
					$this->$methode($pDonnees[$var_name]);
				elseif (!array_key_exists($var_name, $pDonnees) && is_null($this->$var_name))
					$this->$methode(null);
			}
		}
	}
}
