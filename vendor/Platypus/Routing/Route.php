<?php
namespace Platypus\Routing;

class Route
{
	protected	$bundleName;	// nom du Bundle
	protected	$name;			// nom de la route
	protected	$controller;	// nom du Controller
	protected	$subController;	// nom d'un potentiel Controller subsidiaire
	protected	$action;		// nom de l'Action
	protected	$patterns = [];	// tableau contenant en premier le pattern issu du yaml en dur, puis le pattern assemblé en regex
	protected	$vars;			// variables choppées dans l'url depuis la regex
	protected	$method;		// la route est accessible via une requete GET ou POST

	function __construct($pBundleName, $pName = null, $pController = null, $pSubController = null, $pAction = null, $pPatterns = null, $pVars = null, $pMethod = 'get'){
		$this->setBundleName($pBundleName);
		$this->setName($pName);
		$this->setController($pController);
		$this->setSubController($pSubController);
		$this->setAction($pAction);
		$this->setPatterns($pPatterns);
		$this->setVars($pVars);
		$this->setMethod($pMethod);
	}




	/**
	 * Vérifier si la Route correspond à un pattern. Si oui, on en profite pour ressortir les vars présentent dans l'URL
	 * @param String $pUrl
	 */
	function doesItMatch($pUrl)
	{
		if (preg_match('#^'.$this->patterns[0].'(?:\?.+)*$#', $pUrl, $matches) && (is_null($this->method) || strtoupper($_SERVER['REQUEST_METHOD']) == strtoupper($this->method)))
			return $matches;
		else
			return false;
	}


	/**
	 * Vérifier si la Route commence par un préfixe particulier
	 * @param String $pString
	 * @return Boolean
	 */
	function hasPrefixe($pString)
	{
		if (preg_match('#^'.$pString.'#', $this->patterns[1]))
			return true;
		else
			return false;
	}


	// =================================================================
	// =============================SETTERS=============================
	
	function setBundleName($pBundleName){
		$this->bundleName = $pBundleName;
	}

	function setName($pName){
		$this->name = $pName;
	}
	
	function setController($pController){
		$this->controller = $pController;
	}

	function setSubController($pSubController){
		$this->subController = $pSubController;
	}
	
	function setAction($pAction){
		$this->action = $pAction;
	}
	
	function setPatterns($pPatterns){
		$this->patterns = $pPatterns;
	}

	function setVars($pVars){
		$this->vars = $pVars;
	}

	function addVar($pKey, $pValue){
		$this->vars[$pKey] = $pValue;
	}

	function setMethod($pMethod){
		$this->method = $pMethod;
	}
	

	// =================================================================
	// =============================GETTERS=============================
	
	function getBundleName(){
		return $this->bundleName;
	}
	
	function getName(){
		return $this->name;
	}
	
	function getController(){
		return $this->controller;
	}

	function getSubController(){
		return $this->subController;
	}
	
	function getAction(){
		return $this->action;
	}

	function getPatterns(){
		return $this->patterns;
	}

	function getVars(){
		return $this->vars;
	}
	
	function getMethod(){
		return $this->method;
	}
}
