<?php
namespace Platypus\Routing;
use Platypus\Cache\Cache;
use Platypus\Network\Request;
use Symfony\Component\Yaml\Parser;

class Router
{
	protected	$imports = [];
	protected	$routes = [];

	/**
	 * On se contente de remplir le tableau $imports avec les noeuds du fichier imports.yaml
	 */
	function __construct(){
		$cache_fileName = 'cache_routesImports';

		if (!Cache::read($this->imports, $cache_fileName, false))
		{
			$yamlParser = new Parser();
			$routing = $yamlParser->parse(file_get_contents(__DIR__.'/../../../app/config/routing.yaml'));
			
			$this->imports = $routing['import'];

			Cache::write($this->imports, $cache_fileName);
		}
	}





	/**
	 * Remplit le router avec les routes récoltées depuis un fichier yaml
	 */
	function fillRouter()
	{
		$cache_fileName = 'cache_routes';
		
		// si cache non existant ou périmé ou le construit
		if (!Cache::read($this->routes, $cache_fileName, false))
		{
			// === Récupère chaque fichier routing.yaml spécifié dans les imports ===
			// =====================================================================

			foreach ($this->imports as $import)
			{
				$yamlParser				 = new Parser();
				$routingYamlFileLocation = __DIR__.'/../../../src/'.$import;

				// sert juste à récupérer le nom du Bundle où se situe les routes
				preg_match('#([a-zA-Z]+Bundle)#', $import, $matches_bundleName);

				if (file_exists($routingYamlFileLocation))
				{
					$routing						= $yamlParser->parse(file_get_contents(__DIR__.'/../../../src/'.$import));

					// récupère le pattern du prefixe
					if (array_key_exists('prefix', $routing)) {
						$routePrefix_rawPattern		= $routing['prefix']['pattern'];
						$routePrefix_requirements	= array_key_exists('requirements', $routing['prefix']) ? $routing['prefix']['requirements'] : [];
						$routePrefix_pattern		= $this->formatPattern($routePrefix_rawPattern, $routePrefix_requirements);
					} else {
						$routePrefix_rawPattern		= '';
						$routePrefix_pattern		= array('pattern' => '', 'vars' => []);
					}
					
					// pour chaque route spécifiée dans le fichier routing.yaml
					foreach ($routing['routes'] as $routeName => $route)
					{
						$routeRawPattern			= $route['pattern'];
						$routeRequirements			= array_key_exists('requirements', $route) ? $route['requirements']: [];
						// récupère le pattern de la route
						$routePattern				= $this->formatPattern($routeRawPattern, $routeRequirements);
						// on assemble le pattern avec les regex correspondantes à chaque variable
						$rawPattern					= $routePrefix_rawPattern.$routeRawPattern;
						$formatedPattern['pattern'] = $routePrefix_pattern['pattern'].$routePattern['pattern'];
						$formatedPattern['vars']	= array_merge($routePrefix_pattern['vars'], $routePattern['vars']);

						// on regarde si on demande un subController
						if (is_array($route['connect'][0])) {
							$controllerName		= $route['connect'][0][0];
							$subControllerName	= $route['connect'][0][1];
						} else {
							$controllerName		= $route['connect'][0];
							$subControllerName	= null;
						}
						
						$actionName				= $route['connect'][1];

						// si précisé ajoute la méthode GET ou POST
						$method = array_key_exists('method', $route) ? $route['method'] : null;

						$this->addRoute(new Route(	$matches_bundleName[1], // provient de l'import "preg_matché" plus haut
													$routeName,
													$controllerName,
													$subControllerName,
													$actionName,
													array($formatedPattern['pattern'], $rawPattern),
													$formatedPattern['vars'],
													$method
						));
					}
				}
			}

			Cache::write($this->routes, $cache_fileName);
		}
	}


	/**
	 * 	Ajouter une route au tableau des Routes
	 */
	function addRoute(Route $pRoute)
	{
		$this->routes[$pRoute->getName()] = $pRoute;
	}


	/**
	 * Si besoin : Dans le pattern remplace le nom des variables par la regex. Récupère aussi le nom des variables
	 * @param String $pRawPattern	: le pattern de la route possédant encore le nom des variables au lieu des regexs
	 * @param Array $pRequirements	: tableau des regex requirements
	 * @return Array $array			: Retourne un tableau composé d'une entrée pour le pattern formé et une pour les variables
	 */
	function formatPattern($pRawPattern, $pRequirements)
	{
		// enlève les slashs en trop
		$pRawPattern = preg_replace('#/+#','/', $pRawPattern);
		$array['vars'] = [];

		// remplace le nom des variables présents dans le pattern par les regex notifiées
		// retourne en même temps le nom des variables
		foreach ($pRequirements as $name => $regex) {
			$pRawPattern = preg_replace('#\{('.$name.')\}#', $regex, $pRawPattern);
			$array['vars'][] = $name;
		}

		$array['pattern'] = $pRawPattern;

		return $array;
	}


	/**
	 * Génère une URL avec un raw pattern d'une Route
	 * @param String $pPattern rawpattern d'une Route
	 * @param Array $pVars Variables a rentrer dans l'url à générer
	 * @param Array $pVarsGet Variables en plus a placer dans en GET de l'url à générer
	 * @return String $formatedUrl L'URL formatée
	 */
	function formatUrl($pPattern, array $pVars, array $pVarsGet = null)
	{
		$formatedUrl = preg_replace_callback('#\{.+\}#U', function() use (&$pVars) {
			return array_shift($pVars);
		}, $pPattern);

		$numGetVars = count($pVarsGet);
		$i = 0;

		if (!is_null($pVarsGet))
		{
			$formatedUrl .='?';
			foreach ($pVarsGet as $key => $value) {
				$formatedUrl .= $key.'='.$value;
				
				if(++$i !== $numGetVars)
					$formatedUrl .='&';
			}
		}

		return $formatedUrl;
	}


	/**
	 * Essaie de retourner une route correspondante à l'url
	 * @param String $pUrl
	 * @return Route $pRoute : si un Route de matchée
	 * @return RuntimeException : si pas de Route matchée
	 */
	function catchRoute($pUrl)
	{
		foreach ($this->routes as $route)
		{
			// si une route est matchée elle renvoie ses matches de l'url
			$matches = $route->doesItMatch($pUrl);
			if ($matches != false)
			{
				// on place les matches dans le tableau des vars de la Route
				foreach ($matches as $key => $match) {
					if ($key != 0)
					{
						$vars = $route->getVars();
						$array[$vars[$key-1]] = $match;
					}
				}
				if (isset($array)) // si la route n'a de base pas de variable on ne peut rien y changer. C'est la vie
					$route->setVars($array);

				// enfin on ajoute ces variables au tableau de GET
				$_GET = array_merge($_GET, $route->getVars());

				return $route;
			}
		}

		// si on sort de la boucle c'est que aucune route n'a été matchée. Donc erreur 404
		throw new \RuntimeException("Aucune route de trouvee", 404);
	}


	/**
	 * Retourne l'url d'une route selon son nom
	 * @param String $pRouteName	: nom de la Route
	 * @return String $pRoute		: l'url de la route matchée
	 * @return RuntimeException		: si la référence ne mène à aucune Route
	 */
	function generatePath($pRouteName, array $pVars = [], array $pVarsGet = null)
	{
		if (array_key_exists($pRouteName, $this->routes)) {
			$pattern = $this->routes[$pRouteName]->getPatterns()[1];
			return $this->formatUrl($pattern, $pVars, $pVarsGet);
		}
	}
}
