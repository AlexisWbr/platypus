<?php
namespace Platypus\View\Helper;
use Platypus\View\Helper;

class SecureHelper extends Helper
{
	/**
	 * @param String/Array $pRanks : Les rangs que doit posséder l'utilisateur pour que le retour soit true
	 * @param Boolean $pExhaustive : Tous les rangs doivent être possédés par l'utilisateur
	 * @return Array $pRanksExtended : Utiliser un tableau de rangs différents en plus de celui par défaut stocké en session
	 * @return Boolean $gotTheRank : Oui ou non l'utilisateur a les rangs demandés
	 */
	function hasRank($pRanks, Array $pRanksExtended = null, $pExhaustive = false)
	{
		$pRanks				= is_array($pRanks) ? $pRanks : [0=>$pRanks];
		// détecte si utilise les clefs comme comparaison
		$useKey = $pRanks === array_filter($pRanks, 'is_numeric');
		$userRanks			= isset($_SESSION['user']['ranks']) && is_array($_SESSION['user']['ranks']) ? $_SESSION['user']['ranks'] : [];
		$userRanks			= !is_null($pRanksExtended) ? array_merge($userRanks, $pRanksExtended) : $userRanks;
		if (!$useKey)
			$ownedAskingRanks	= array_intersect($pRanks, $userRanks);
		else
			$ownedAskingRanks	= array_intersect($pRanks, array_flip($userRanks));

		if ($pExhaustive) {
			$gotTheRank = $ownedAskingRanks == $pRanks;
		} else {
			$gotTheRank = !empty($ownedAskingRanks);
		}

		return $gotTheRank;
	}


	/**
	 * @param String/Array $pRanks : Les rangs que ne doit pas posséder l'utilisateur pour que le retour soit true
	 * @param Boolean $pExhaustive : Aucun des rangs tous ensemblent ne doivent être possédés par l'utilisateur
	 * @return Boolean $gotTheRank : Oui ou non l'utilisateur n'a pas les rangs demandés
	 */
	function hasNotRank($pRanks, Array $pRanksExtended = null, $pExhaustive = false)
	{
		$pRanks				= is_array($pRanks) ? $pRanks : [0=>$pRanks];
		$useKey = $pRanks === array_filter($pRanks, 'is_numeric');
		// détecte si utilise les clefs comme comparaison
		$userRanks			= isset($_SESSION['user']['ranks']) && is_array($_SESSION['user']['ranks']) ? $_SESSION['user']['ranks'] : [];
		$userRanks			= !is_null($pRanksExtended) ? array_merge($userRanks, $pRanksExtended) : $userRanks;
		if (!$useKey)
			$ownedAskingRanks	= array_intersect($pRanks, $userRanks);
		else
			$ownedAskingRanks	= array_intersect($pRanks, array_flip($userRanks));

		if ($pExhaustive) {
			$gotTheRank = $ownedAskingRanks != $pRanks;
		} else {
			$gotTheRank = empty($ownedAskingRanks);
		}

		return $gotTheRank;
	}
}
