<?php
namespace Platypus\View\Helper;
use Platypus\Routing\Router;
use Platypus\View\Helper;

class RoutingHelper extends Helper
{
	protected	$router;

	/**
	 * Utilisé pour instancier et remplir le router une seule fois
	 */
	protected function fillRouter()
	{
		if (!isset($this->router))
		{
			$this->router = new Router;
			$this->router->fillRouter();
		}
	}


	/**
	 * Retourne une url relative depuis une Route grâce au nom de la Route
	 * @param $pRouteName	: le nom de la route ou bien  [BundleName]Bundle_[ControllerName]_[ActionName]
	 * @param $pVars		: Des variables
	 */
	function path($pRouteName, array $pVars = [], array $pVarsGet = null)
	{
		$this->fillRouter();

		$url = $this->router->generatePath($pRouteName, $pVars, $pVarsGet);
		return $url;
	}


	/**
	 * Retourne une url absolue depuis une Route grâce au nom de la Route
	 * @param $pReference	: le nom de la route ou bien  [BundleName]Bundle_[ControllerName]_[ActionName]
	 * @param $pVars		: Des variables
	 */
	function url($pReference, array $pVars = [], array $pVarsGet = null)
	{
		$this->fillRouter();

		$url = $this->router->generatePath($pReference, $pVars, $pVarsGet);
		return 'http://'.$_SERVER['HTTP_HOST'].$GLOBALS['KERNEL_CONFIG']['base_url'].$url;
	}


	/**
	 * Retourne une .classe exploitable pour le css pour indiquer dans une menu de navigation la page actuellement visité
	 * @param Page $pPage		: La Page
	 * @param String $pElement	: L'élément issue de la Page à controller : 'controller' = controller name / 'subController' = SubController name
	 * 							 / 'action' = Action name / 'name' = route name
	 * @param String $pString	: La valeur a comparer
	 * @param String $pClassName: La classse à retourner
	 * @return String $pClassName
	 */
	function currentPage(&$pPage, $pElement, $pString, $pClassName = 'current')
	{
		$element = 'get'.$pElement;
		if ($pPage->getBundle()->getRoute()->$element() == $pString)
			return $pClassName;
	}
}
