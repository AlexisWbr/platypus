<?php
namespace Platypus\View\Helper;
use Platypus\Bundle\Bundle;
use Platypus\Cache\Cache;
use Platypus\View\Helper;

abstract class Helperbox
{
	protected static	$helpersName = ['Date', 'Routing', 'Secure','Text', 'User'];





	/**
	 * Créé automatiquement les instances des outils dans la Cell/Page
	 * @param $pInstanceObject Instance de la Cell ou de la Page dans lequel les instances des outils vont être stockés.
	 * @param Bundle $pBundle Instance du Bundle dont ont besoin les outils pour être instanciés (à cause de l'utilisation de Network)
	 */
	static function fillTheBox(&$pInstanceObject, Bundle $pBundle)
	{
		// tableau qui contiendra les instances des outils
		$box = [];

			foreach (self::$helpersName as $helperName) {
				$helperPath = "Platypus\View\Helper\\".ucfirst($helperName)."Helper";
				$box[$helperName] = new $helperPath($pBundle);
			}

		// Passe au Controller le tableau d'outils
		foreach ($box as $helperName => $helper) {
			$pInstanceObject->Helper[$helperName] = $helper;
		}
	}
}
