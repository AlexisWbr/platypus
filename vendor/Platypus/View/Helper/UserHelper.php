<?php
namespace Platypus\View\Helper;
use Platypus\View\Helper;

class UserHelper extends Helper
{
	/**
	 * Vérifier l'existance d'au moins un Flash
	 * @param String $pKey La clef du flash
	 */
	function hasFlash($pKey = null)
	{
		if (is_null($pKey)) return isset($_SESSION['flash']);
		else return isset($_SESSION['flash'][$pKey]);
	}


	/**
	 * Récupérer tous les Flash et les supprimer aussitôt
	 */
	function getAllFlash()
	{
		$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : [];
		unset($_SESSION['flash']);

		return $flash;
	}


	/**
	 * Récupérer les Flash d'une catégorie et les supprimer aussitôt
	 */
	function getFlash($pKey)
	{
		$flash = isset($_SESSION['flash'][$pKey]) ? $_SESSION['flash'][$pKey] : [];
		unset($_SESSION['flash'][$pKey]);

		return $flash;
	}


	/**
	 * Récupérer un Flash précis
	 */
	function getFlashByTag($pKey, $pTag)
	{
		$flash = isset($_SESSION['flash'][$pKey][$pTag]) ? $_SESSION['flash'][$pKey][$pTag] : null;
		unset($_SESSION['flash'][$pKey][$pTag]);

		return $flash;
	}
}
