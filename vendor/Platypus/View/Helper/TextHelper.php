<?php
namespace Platypus\View\Helper;
use Platypus\View\Helper;

class TextHelper extends Helper
{
	function truncate($pString, $pLength)
	{
		$string = strip_tags($pString);

		if (strlen($string) > $pLength) {
			// truncate string
			$stringCut = substr($string, 0, $pLength);
			// make sure it ends in a word
			$string = substr($stringCut, 0, strrpos($stringCut, ' ')); 
		}

		return $string;
	}
}
