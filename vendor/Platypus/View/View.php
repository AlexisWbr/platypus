<?php
namespace Platypus\View;

trait View
{
	protected	$vars = [];
	protected	$htmlLocation_view;





	/**
	 * Ajouter une variable à la page qui pourra l'exploiter pour l'afficher en son sein
	 * @param String $pKey : le nom de la variable a ajouter à la page
	 * @param Miscelaneous $pVar
	 */
	function addVar($pKey, $pVar)
	{
		if (empty($pKey) || !is_string($pKey))
			throw new \InvalidArgumentException("Clef nulle, non spécifiée ou de type non String");
		$this->vars[$pKey] = $pVar;
	}


	// =================================================================
	// =============================SETTERS=============================
	
	/**
	 * Une méthode appelée à chaque modification du nom de la vue du Controller
	 * @param String $pHtmlLocation_view : chemin ou est stocké le fichier view
	 */
	function setHtmlLocation_view($pHtmlLocation_view){
		$this->htmlLocation_view = $pHtmlLocation_view;
	}


	// =================================================================
	// =============================GETTERS=============================
	
	function getVars(){
		return $this->vars;
	}

	function getVar($pKey){
		return $this->vars[$pKey];
	}
}
