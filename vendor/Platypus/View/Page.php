<?php
namespace Platypus\View;
use Platypus\Bundle\Bundle;
use Platypus\Bundle\BundleComponent;
use Platypus\Cache\Cache;
use Platypus\View\Cell;
use Platypus\View\Helper\Helperbox;

class Page extends BundleComponent
{
	use View;

	protected	$htmlLocation_app;
	protected	$htmlLocation_bundle;
	public		$Helper = [];

	function __construct(Bundle $pBundle)
	{
		parent::__construct($pBundle);

		Helperbox::fillTheBox($this, $this->bundle);
	}





	/**
	 * Donner à la page l'endroit où aller chercher le layout de l'App générale et celui du Bundle
	 * @param String $pNameFile_a : chemin du .html de l'App
	 * @param String $pNameFile_b : chemin du .html du Bundle
	 */
	function compose($pNameFile_a, $pNameFile_b)
	{
		$this->htmlLocation_app = __DIR__.'/../../../app/views/'.$pNameFile_a.'.php';
		$this->htmlLocation_bundle = __DIR__.'/../../../src/'.$this->bundle->getName().'/Ressources/views/'.$pNameFile_b.'.php';
	}


	/**
	 * Assemble les différents layouts et vues afin d'obtenir la page finale
	 */
	function render()
	{
		extract($this->vars);

		ob_start();
			require $this->htmlLocation_view;
		$contentView = ob_get_clean();

		ob_start();
			require $this->htmlLocation_bundle;
		$contentBundle = ob_get_clean();
		
		ob_start();
			require $this->htmlLocation_app;
		return ob_get_clean();
	}


	/**
	 * Assemble le layout par défault définit dans le app/config.yaml et la vue Error afin d'obtenir la page d'Erreur finale
	 */
	function renderError($pErrorCode)
	{
		extract($this->vars);

		$this->htmlLocation_app = __DIR__.'/../../../app/views/'.$GLOBALS['KERNEL_CONFIG']['errorLayout'].'.php';
		$this->htmlLocation_bundle = __DIR__.'/../../../app/views/Error/'.$pErrorCode.'Error.php';

		ob_start();
			require $this->htmlLocation_bundle;
		$contentBundle = ob_get_clean();

		ob_start();
			require $this->htmlLocation_app;
		return ob_get_clean();
	}


	/**
	 * instancie la bonne Cell en fonction des paramètres reçus
	 * @param String $pName : le nom de la Cell à aller chercher. Si on précise le nom du Bundle, on ira chercher la Cell dans le Bundle demandé
	 * Autrement on ira la chercher dans l'app
	 */
	function cell($pName, $pVars = [], $pCacheConfig = [])
	{
		$pCache_fileName = !isset($pCacheConfig[0]) ? false : $pCacheConfig[0];
		$pDateExpiry = !isset($pCacheConfig[1]) ? false : (int)$pCacheConfig[1];


		// si cache non existant ou périmé ou le construit
		if ($pCache_fileName === false || (!Cache::read($cellRendered, $pCache_fileName, $pDateExpiry)))
		{
			preg_match('#(\w+){1}:{0,1}(\w+){0,1}(?:::[\w]+)?#', $pName, $matches_cellEmplacement);
			preg_match('#.+::([\w]+)$#', $pName, $matches_cellAction);
			
			// si pas de 2nd entrée c'est qu'aucun Bundle n'a été précisé
			if (!isset($matches_cellEmplacement[2])) {
				$bundleName = $this->bundle->getName();
				$cellName = $matches_cellEmplacement[1];
				$cellPath = '\\app\\views\\Cell\\'.$cellName.'\\'.$cellName.'Cell';
			}
			// si 2nd entrée c'est qu'un Bundle a été précisé
			else {
				$bundleName = $matches_cellEmplacement[1];
				$cellName = $matches_cellEmplacement[2];
				$cellPath = $cellPath = '\\src\\'.$bundleName.'\\Ressources\\views\\Cell\\'.$cellName.'\\'.$cellName.'Cell';
			}

			// si une Action de demandée
			if (isset($matches_cellAction[1]))
				$cellAction = lcfirst($matches_cellAction[1]);
			else
				$cellAction = 'display';

			$cell = new $cellPath($this->bundle, $cellName, $cellAction, $pVars);
			$cellRendered = $cell->render();

			if ($pCache_fileName !== false)
				Cache::write($cellRendered, $pCache_fileName);
		}

		return $cellRendered;
	}
}
