<?php
namespace Platypus\View;
use Platypus\Bundle\Bundle;

class Helper
{
	protected	$request;
	protected	$session;

	function __construct(Bundle $pBundle)
	{
		$this->request = $pBundle->getRequest();
		$this->session = $pBundle->getSession();
	}





	// =================================================================
	// =============================GETTERS=============================
	
	function getRequest(){
		return $this->request;
	}
	
	function getSession(){
		return $this->session;
	}
}
