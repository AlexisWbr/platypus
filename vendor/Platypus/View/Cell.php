<?php
namespace Platypus\View;
use app\config\PDOConnect;
use Platypus\Bundle\Bundle;
use Platypus\Bundle\BundleComponent;
use Platypus\Controller\Tool\Toolbox;
use Platypus\ORM\ModelCollector;
use Platypus\ORM\Shortcut\GetModelTrait;
use Platypus\View\Helper\Helperbox;

// instancié par un objet Page
abstract class Cell extends BundleComponent
{
	use GetModelTrait;
	use View;

	protected	$name;				// String nom de la Cell
	protected	$modelCollector;	// liste des Models appelés par le codeur à utiliser dans la Cell
	public		$Tool = [];
	public		$Helper = [];

	final function __construct(Bundle $pBundle, $pCellName, $pCellAction, array $pVars = []){
		parent::__construct($pBundle);

		$this->name = $pCellName;
		$this->modelCollector = new ModelCollector(PDOConnect::mySqlConnect(), $this->bundle->getName());

		$cellPath = dirname((new \ReflectionClass(get_class($this)))->getFileName());
		$contentView = $cellPath.'/views/'.$pCellAction.'.php';
		$this->vars = $pVars;
		$this->setHtmlLocation_view($contentView);

		Toolbox::fillTheBox($this, $this->bundle);

		// on lance la Cell
		$pCellAction = ($pCellAction != 'display') ?$pCellAction.'Action' : $pCellAction;
		$this->$pCellAction();

		Helperbox::fillTheBox($this, $this->bundle);
	}





	abstract function display();

	final function render()
	{
		extract($this->vars);

		ob_start();
			require $this->htmlLocation_view;
		return ob_get_clean();
	}

	/**
	 * Ajouter une variable à la page qui pourra l'exploiter pour l'afficher en son sein
	 * @param String $pKey : le nom de la variable a ajouter à la page
	 * @param Miscelaneous $pVar
	 */
	function addToPage($pKey, $pVar)
	{
		$this->addVar($pKey, $pVar);
	}


	// =================================================================
	// =============================SETTERS=============================
	
	function setView($pViewName){
		// si on édite la vue il faut mettre à jour le bon contenu de la page
		$this->setHtmlLocation_view(dirname((new \ReflectionClass($this))->getFileName()).'/views/'.$pViewName.'.php');
	}

	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getVars(){
		return $this->vars;
	}
	
	function getView(){
		return $this->view;
	}
}
