<?php
namespace Platypus;
use Platypus\Routing\Route;
use Platypus\Routing\Router;
use Platypus\Bundle\Bundle;
use Platypus\Cache\Cache;
use Symfony\Component\Yaml\Parser;

class Kernel
{
	protected	$kernel_config = [];

	function __construct()
	{
		$yamlParser = new Parser();
		$configs = $yamlParser->parse(file_get_contents(__DIR__.'/../../app/config/config.yaml'));

		foreach ($configs as $key => $value) {
			$this->kernel_config[$key] = $value;
		}

		$GLOBALS['KERNEL_CONFIG'] = $this->kernel_config;
	}





	function launch()
	{
		// désactiver toute erreur si tourne sur une App en production ou en test
		if ($this->kernel_config['environment'] == 'prod') {
			error_reporting(0);
		} else {
			error_reporting(E_ALL);
			if ($this->kernel_config['environment'] == 'dev')
				Cache::clear();
		}
		// définit le timezone par défaut
		date_default_timezone_set($this->kernel_config['date_timezone']);

		$router = new Router;
		$router->fillRouter();

		// si pas en maintenance ou que ip autorisée
		if ($this->kernel_config['maintenance']['activated'] == false || in_array($_SERVER['REMOTE_ADDR'], $this->kernel_config['maintenance']['ipAllowed']))
		{
			try {
				$base_url			= $this->kernel_config['base_url'];
				$bootstrapLocation	= $this->kernel_config['bootstrap'];
				$url = preg_split('#'.$base_url.'/'.$bootstrapLocation.'#', $_SERVER['REQUEST_URI'],2) ;

				$url = empty($url[1]) ? '/' : '/'.$url[1];

				$route = $router->catchRoute($url);
				$bundlePath = '\\src\\'.$route->getBundleName().'\\'.$route->getBundleName();
			}
			// si jamais aucune route ne ressort c'est qu'on tombe sur une erreur 404. Dans ce cas là on créé un Bundle spécial
			catch (\RuntimeException $e) {
				$route = new Route('ErrorBundle', '404');
				// on donne la possiblité "d'overrider" le Bundle Error
				$bundlePath = class_exists('\src\ErrorBundle\ErrorBundle') ? '\src\ErrorBundle\ErrorBundle' : '\Platypus\Error\ErrorBundle';
			}
		}
		// si en maintenance
		else
		{
			$route = new Route('ErrorBundle', '503');
			// on donne la possiblité "d'overrider" le Bundle Error
			$bundlePath = class_exists('\src\ErrorBundle\ErrorBundle') ? '\src\ErrorBundle\ErrorBundle' : '\Platypus\Error\ErrorBundle';
		}
		
		session_start();

		$bundle = new $bundlePath($router, $route);
		$bundle->run();
	}
}
