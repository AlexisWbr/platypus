<?php
namespace Platypus\Controller;
use Platypus\Bundle\Bundle;
use Platypus\Controller\Controller;

class Tool
{

	protected	$request;
	protected	$response;
	protected	$session;
	protected	$page;			// la Page du Controller. Utilisée pour pouvoir y accéder depuis un Tool.
								// Dans un Cell cette variable est null puisqu'une Cell ne possède pas de Page mais directement un trait View

	function __construct(Bundle $pBundle, $pPage){
		$this->request = $pBundle->getRequest();
		$this->response = $pBundle->getResponse();
		$this->session = $pBundle->getSession();
		$this->page = $pPage;
	}





	/**
	 * Ajouter une variable à la page qui pourra l'exploiter pour l'afficher en son sein
	 * @param String $pKey : le nom de la variable a ajouter à la page
	 * @param Miscelaneous $pVar
	 */
	function addToPage($pKey, $pVar)
	{
		$this->page->addVar($pKey, $pVar);
	}

	
	// =================================================================
	// =============================GETTERS=============================
	
	function getRequest(){
		return $this->request;
	}
	
	function getResponse(){
		return $this->response;
	}
	
	function getSession(){
		return $this->session;
	}
}
