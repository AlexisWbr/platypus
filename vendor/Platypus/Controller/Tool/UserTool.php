<?php
namespace Platypus\Controller\Tool;
use Platypus\Bundle\Bundle;
use Platypus\Controller\Tool;
use Platypus\Security\Security;

class UserTool extends Tool
{
	protected	$security;

	function __construct(Bundle $pBundle, $pPage){
		parent::__construct($pBundle, $pPage);
	}





	/**
	 * Modifie/Ajoute un Flash
	 * @param String $pKey La clef du flash
	 * @param String $pValue Le message contenue dans le flash
	 */
	function setFlash($pValue, $pKey, $pTag = null)
	{
		if (!is_null($pTag))
			$_SESSION['flash'][$pKey][$pTag] = $pValue;
		else
			$_SESSION['flash'][$pKey][] = $pValue;
	}


	/**
	 * Vérifier l'existance d'au moins un Flash
	 * @param String $pKey La clef du flash
	 */
	function hasFlash($pKey = null)
	{
		if (is_null($pKey)) return isset($_SESSION['flash']);
		else return isset($_SESSION['flash'][$pKey]);
	}


	/**
	 * Change le statut d'authentification d'un User
	 */
	function setAuthenticated($pAuthenticated = true)
	{
		if (!is_bool($pAuthenticated))
		{
			throw new \InvalidArgumentException('La valeur spécifiée à la méthode setAuthenticated() doit être un boolean');
		}

		$_SESSION['authenticated'] = $pAuthenticated;
	}
}
