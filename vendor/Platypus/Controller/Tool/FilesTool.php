<?php
namespace Platypus\Controller\Tool;
use Platypus\Controller\Tool;

class FilesTool extends Tool
{
	/**
	 * Supprimer un dossier et son contenu ou supprimer un fichier seul
	 * Function Static car utilisée par le Cache\Cachey
	 */
	static function deleteFolder($path)
	{
		if (is_dir($path) === true)
		{
			$files = array_diff(scandir($path), array('.', '..'));

			foreach ($files as $file)
			{
				self::deleteFolder(realpath($path) . '/' . $file);
			}

			return rmdir($path);
		}

		else if (is_file($path) === true)
		{
			return unlink($path);
		}

		return false;
	}


	/**
	 * Vérifier que tout est ok avant d'uploader un fichier quelconque. Possibilité de ne pas uploader directement.
	 * @param String $pFile Le fichier issu de $_FILES
	 * @param Array $pExtensions Un tableau comportant les extensions interdites ou autorisées selon une clef 'allow' ou 'ban' : ['allow' => 'png'] // ['ban' => 'php']
	 * @param int $pMaxSize Taille max
	 * @param bool $pDirectUpload Uploader une fois la vérification ok
	 * @param String $pDestination Destination du fichier
	 * @param String $pFilename Nom du fichier AVEC son extension
	 */
	function verifAndUploadFile($pFile, $pExtensions = null, $pMaxSize = null, $pDirectUpload = false, $pDestination = null, $pFileNewName = null)
	{
		//Test0: on a déjà un fichier d'arrivé
		if (is_null($pFile)) return false;
		//Test1: fichier correctement uploadé
		if (!isset($pFile) || $pFile['error'] != 0) return false;
		//Test2-a: extensions autorisées
		elseif (isset($pExtensions['allow']) && !in_array(strtolower(pathinfo($pFile['name'])['extension']), $pExtensions['allow'])) return false;
		//Test2-b: extensions interdites
		elseif (isset($pExtensions['ban']) && in_array(strtolower(pathinfo($pFile['name'])['extension']), $pExtensions['ban'])) return false;
		//Test3: taille limite
		elseif (!is_null($pMaxSize) && $pFile['size'] > $pMaxSize) return false;

		if ($pDirectUpload == false) return true;
		else
			$this->uploadFile($pFile, $pDestination, $pFileNewName);
	}


	/**
	 * Uploader un fichier
	 * voir verifAndUploadFile pour les var
	 */
	function uploadFile($pFile, $pDestination, $pFileNewName = null)
	{
		$fileNewName = is_null($pFileNewName) ? md5(uniqid(rand(), true)).'.'.pathinfo($pFile['name'])['extension'] : $pFileNewName;

		if (!file_exists($pDestination)) mkdir($pDestination, 0755, true);

		move_uploaded_file($pFile['tmp_name'], $pDestination.'/'.$fileNewName);
	}


	/**
	 * Redimensionner image
	 * @param String $pFile L'image à redimensionner
	 * @param int $pNewWidth Largeur de la nouvelle image 
	 * @param int $pNewHeight Hauteur de la nouvelle image 
	 * @param string $pUploadPath Dossier d'upload de la nouvelle image
	 */
	function resizeAndUploadImage($pFile, $pNewWidth, $pNewHeight, $pName, $pUploadPath)
	{
		list($original_w, $original_h, $original_type) = getimagesize($pFile);
		// $name = is_null($pName) ? $pNewWidth.'_'.$pNewHeight.'_'.pathinfo($pFile['name'])['basename'] : $pName ;

		// si pas possible de resize car par image
		if (!in_array($original_type, [1,2,3]))
			return false;

		// nouvelles dimensions de l'image
		// si horizontale ou 1/1
		if (($original_w / $original_h ) >= 1) {
			$ratio	= round(($original_w / $pNewWidth), 2);
			$width	= ($original_w / $ratio);
			$height	= ($original_h / $ratio);
		}
		// si image verticale
		if (($original_w / $original_h ) < 1) {
			$ratio	= round(($original_h / $pNewHeight), 2);
			$width	= ($original_w / $ratio);
			$height	= ($original_h / $ratio);
		}

		// redimensionne largeur
		if ($width > $pNewWidth) {
			$ratio	= round(($width / $pNewWidth), 2);
			$width	= ($width / $ratio);
			$height	= ($height / $ratio);
		}
		// redimensionne hauteur
		if ($height > $pNewHeight) {
			$ratio	= round(($height / $pNewHeight), 2);
			$width	= ($width / $ratio);
			$height	= ($height / $ratio);
		}

		$new_w		= $width;
		$new_h		= $height;

		switch ($original_type) {
			case 1:
				$imgt = "ImageGIF";
				$imgcreatefrom = "imagecreatefromgif";
				break;
			case 2:
				$imgt = "ImageJPEG";
				$imgcreatefrom = "imagecreatefromjpeg";
				break;
			case 3:
				$imgt = "ImagePNG";
				$imgcreatefrom = "imagecreatefrompng";
				break;
			default:
				return false;
				break;
		}

		$oldImage	= $imgcreatefrom($pUploadPath.'/'.pathinfo($pFile)['basename']);
		$newImage	= imagecreatetruecolor($new_w, $new_h);

		// si l'image est un png ou un gif avec transparence
		if (in_array($original_type, [1, 3])) {
			imagealphablending($newImage, false);
			imagesavealpha($newImage, true);
			$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
			imagefilledrectangle($newImage, 0, 0, $new_w, $new_h, $transparent);
		}

		imagecopyresampled($newImage, $oldImage, 0, 0, 0, 0, $new_w, $new_h, $original_w, $original_h);
		$imgt($newImage, $pUploadPath.'/'.$pName);

		return true;
	}
}
