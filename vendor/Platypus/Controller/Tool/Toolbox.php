<?php
namespace Platypus\Controller\Tool;
use Platypus\Bundle\Bundle;
use Platypus\Cache\Cache;
use Platypus\Controller\Controller;
use Platypus\Controller\Tool;

abstract class Toolbox
{
	protected static	$toolsName = ['Array', 'Cache', 'Files', 'Paginate', 'Secure', 'Text', 'User'];





	/**
	 * Créé automatiquement les instances des outils dans le Controller
	 * @param $pController Instance du Controller dans lequel les instances des outils vont être stockés. Peut être un Controller ou une Cell
	 * @param Bundle $pBundle Instance du Bundle dont ont besoin les outils pour être instanciés (à cause de l'utilisation de Network)
	 */
	static function fillTheBox(&$pController, Bundle $pBundle, $pPage = null)
	{
		// tableau qui contiendra les instances des outils
		$box = [];

			foreach (self::$toolsName as $toolName) {
				$toolPath = "Platypus\Controller\Tool\\".ucfirst($toolName)."Tool";
				$box[$toolName] = new $toolPath($pBundle, $pPage);
			}

		// Passe au Controller le tableau d'outils
		foreach ($box as $toolName => $tool) {
			$pController->Tool[$toolName] = $tool;
		}
	}
}
