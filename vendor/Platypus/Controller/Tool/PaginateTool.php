<?php
namespace Platypus\Controller\Tool;
use Platypus\Controller\Tool;

class PaginateTool extends Tool
{
	/**
	 * Ressort des infos utilisées par un Controlleur pour paginer des entités
	 * @param int $pEntitiesPerPage		le nbr d'entités apparaissant par page
	 * @param int $pCurrentPage			la page de la pagination actuellement utilisée
	 * @param int $pTotalEntities		le nbr total d'entités ressorties de la bdd
	 * @return Array $paginate
	 * 				entitiesPerPage		le nbr d'entités apparaissant par page
	 *				currentPage			la page de la pagination actuellement utilisée
	 *				entryBegin			la valeur pour le LIMIT de la requete SQL
	 *				totalPage			le nombre total de pages de la pagination
	 */
	function paginate($pEntitiesPerPage, $pTotalEntities, $pCurrentPage)
	{
		// nbr d'entité à afficher par page
		$paginate['entitiesPerPage'] = (int)$pEntitiesPerPage;

		// numéro de pagination actuel
		$paginate['currentPage'] = (int)$pCurrentPage;
		$paginate['currentPage'] = ($paginate['currentPage'] < 1) ? 1 : $paginate['currentPage'];

		// on calcule la LIMIT de la liste à récupérer en fonction de la pagination
		$paginate['entryBegin'] = $paginate['entitiesPerPage'] * ($paginate['currentPage']-1);
		$paginate['entryBegin'] = ($paginate['entryBegin'] < 0) ? 0 : (int)$paginate['entryBegin'];

		// on calcule le nombre de pages pour la pagination
		$paginate['totalPage'] = (int)ceil($pTotalEntities/$paginate['entitiesPerPage']);

		$this->addToPage('currentPage', $paginate['currentPage']);
		$this->addToPage('totalPage', $paginate['totalPage']);

		return $paginate;
	}
}
