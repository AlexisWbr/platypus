<?php
namespace Platypus\Controller\Tool;
use Platypus\Controller\Tool;

class SecureTool extends Tool
{
	/**
	 * Créé un token de sécurité stocké dans une $_SESSION
	 * @param String $pTokenName Nom du token
	 * @param Bool $pReuse Réutilier ou non le token à chaque chargement de page
	 * @param Int $pExpiry Temps avant expiration en seconde
	 */
	function generateCSRFToken($pTokenName, $pReuse = false, $pExpiry = 1800)
	{
		// si le token n'existe pas encore ou que l'on ne demande pas de réutilisation on en recréé un à chaque fois
		// si on demande une réutilisation alors on garde le précédent
		if (!isset($_SESSION['CRSF'][$pTokenName]) || $pReuse === false || $_SESSION['CRSF'][$pTokenName]['expiry']+$_SESSION['CRSF'][$pTokenName]['created'] < time())
		{
			$_SESSION['CRSF'][$pTokenName]=array(
					'name' => $pTokenName,
					'token' => uniqid(rand(), true),
					'created' => time(),
					'expiry' => $pExpiry, // par défaut on donne un délai de 30 minutes avant expiration du jeton
					'reuse' => $pReuse
				);
		}

		if ($pReuse === true)
			$_SESSION['CRSF'][$pTokenName]['reuse'] = true;
	}


	/**
	 * Vérifier un token de sécurité
	 * @param String $pTokenName Nom du token 
	 * @param String $pMethod Récupérer le token depuis $_GET ou $_POST
	 * @param Bool $pKeep Réutiliser ou non le token après l'avoir utilisé
	 */
	function checkCSRFToken($pTokenName, $pMethod = 'get', $pKeep = false)
	{
		// on place le jeton récupéré dans une variable
		$token = isset($_SESSION['CRSF'][$pTokenName]) ? $_SESSION['CRSF'][$pTokenName] : NULL;
		// savoir si utiliser getData() ou postData()
		$methodData = strtolower($pMethod).'Data';

		// on jette le token si pas demande de le garder
		if (!$pKeep) unset($_SESSION['CRSF'][$pTokenName]);

		// on retourne si le jeton est valide (correspondance jeton reçu + temps non dépassé)
		return ($token['token'] == self::getRequest()->$methodData($pTokenName) && $token['expiry']+$token['created'] > time());
	}


	/**
	 * Modifier l'expiration d'un cookie
	 */
	function setExpiry($pTokenName, $pValue)
	{
		$_SESSION['CRSF'][$pTokenName]['expiry'] = $pValue;
	}
}
