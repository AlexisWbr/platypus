<?php
namespace Platypus\Controller\Tool;
use Platypus\Cache\Cache;

/**
 * Cette Class n'est pas une Class Tool a proprement parler, elle sert uniquement d'accesseur à la class Cache à travers
 * les Controllers ou les Cells pour conserver une certaine unité d'appel.
 */
class CacheTool extends Cache{}
