<?php
namespace Platypus\Controller\Tool;
use Platypus\Controller\Tool;

class ArrayTool extends Tool
{
	/**
	 * Change les clefs d'un array par une de ses valeurs
	 */
	function orderArray($pArray, $pKey)
	{
		$array = [];
		foreach ($pArray as $value) {
			$array[$value[$pKey]] = $value;
		}
		return $array;
	}


	/**
	 * Supprime une entrée depuis une valeur
	 */
	function unsetByValue(&$pArray, $pDel_val)
	{
		if(($key = array_search($pDel_val, $pArray)) !== false) {
			unset($pArray[$key]);
		}
	}


	/**
	 * "Aplatit" un tableau multidimensionnel
	 * @param Array $pArray Le tableau à aplatir
	 * @param String $pKey Les valeurs d'une clef à récupérer seulement - par défaut récupère toute les clefs
	 */
	function flatten(Array $pArray,  $pKey = null) {
		$return = array();
		array_walk_recursive($pArray, function($a, $k) use (&$return, $pKey) { if(is_null($pKey) || $pKey == $k) $return[] = $a; });
		return $return;
	}
}
