<?php
namespace Platypus\Controller;
use app\component\MainController;
use app\config\PDOConnect;
use Platypus\Bundle\Bundle;
use Platypus\Bundle\BundleComponent;
use Platypus\Controller\Tool\Toolbox;
use Platypus\Security\Firewall;
use Platypus\ORM\ModelCollector;
use Platypus\View\Page;
use Platypus\ORM\Shortcut\GetModelTrait;

class Controller extends BundleComponent
{
	use MainController;
	use GetModelTrait;

	protected	$name;				// nom du Controller
	protected	$subName;			// nom du Controller si Controller subsidiaire. Autrement, même valeur que $this->name
	protected	$action;			// nom de la méthode action du Controller
	protected	$modelCollector;	// liste des Models appelés par le codeur à utiliser dans le Controller
	protected	$page;				// objet page associée au Controller qui contient les vars et la vue à afficher
	protected	$view;				// nom de la vue. A chaque édition de cette var, le fichier dans la Page est mis à jour
	protected	$firewall;			// le parefeu utilisé pour sécuriser des éléments de l'application
	public		$Tool = [];

	function __construct(Bundle $pBundle, $pControllerName, $pSubControllerName, $pActionName)
	{
		parent::__construct($pBundle);

		$this->name = $pControllerName;
		$this->subName = $pSubControllerName;
		$this->action = $pActionName;
		$this->modelCollector = new ModelCollector(PDOConnect::mySqlConnect(), $this->bundle->getName());
		$this->firewall = new Firewall($this->getRouter(), $this->getRequest(), $this->getResponse(), $this->getSession());
		
		$this->page = new Page($pBundle);
		$this->setView($pActionName);

		Toolbox::fillTheBox($this, $this->bundle, $this->page);
	}





	/**
	 * Execute la bonne function du Controller en fonction de sa variable $action
	 * @param String $pActionName : le nom de l'action en 'dur' (sans le suffixe 'Action')
	 */
	function execute($pActionName = null)
	{
		// on execute toutes les fonctions présentes dans le maincontroller
		foreach ($this->executeTheseFunctions as $functionToExe) {
			if (is_callable([$this, $functionToExe]))
				$this->$functionToExe();
		}

		// === si pas de demande particulière de Controller alors on récupère automatiquement le bon controller ===
		if ($pActionName == null){
			$actionName = lcfirst($this->getAction());
		}
		// === si demande d'un Controller particulier on utilise le nom + l'action renseignés ===
		else {
			$actionName = lcfirst($pActionName);
		}

		$action = $actionName.'Action';

		if (is_callable([$this, 'before']))
			$this->before();

		if (is_callable([$this, $action]))
			$this->$action();
		else
			throw new \RuntimeException("Action '".$action."' non definie sur le controller ".$this->getName().". Attention
				a bien preciser une action si le Controller n'est pas le Controller natife de la Route.");
	}


	/**
	 * Ajouter une variable à la page qui pourra l'exploiter pour l'afficher en son sein
	 * @param String $pKey : le nom de la variable a ajouter à la page
	 * @param Miscelaneous $pVar
	 */
	function addToPage($pKey, $pVar)
	{
		$this->page->addVar($pKey, $pVar);
	}


	// =================================================================
	// =============================SETTERS=============================
	
	function setName($pName){
		$this->name = $pName;
	}
	
	function setSubName($pSubName){
		$this->subName = $pSubName;
	}
	
	function setAction($pAction){
		$this->action = $pAction;
	}
	
	function setModelCollector($pModelCollector){
		$this->modelCollector = $pModelCollector;
	}

	function setView($pView){
		$this->view = $pView;
		// si on édite la vue il faut mettre à jour le bon contenu de la page
		$this->page->setHtmlLocation_view(__DIR__.'/../../../src/'.$this->bundle->getName().'/Controller/'.$this->name.'/views/'.lcfirst($this->subName).'_'.$this->view.'.php');
	}
	

	// =================================================================
	// =============================GETTERS=============================
	
	function getName(){
		return $this->name;
	}
	
	function getSubName(){
		return $this->subName;
	}
	
	function getAction(){
		return $this->action;
	}
	
	function getModelCollector(){
		return $this->modelCollector;
	}

	function getPage(){
		return $this->page;
	}

	function getFirewall(){
		return $this->firewall;
	}
}
