<?php
namespace Platypus\Error;
use Platypus\Bundle\Bundle;

class ErrorBundle extends Bundle
{
	function run()
	{
		$controller = new ErrorController($this, 'Error', '', 'throwError');
		$controller->execute();

		$this->getResponse()->sendError($this->route->getName(), $controller->getPage());
	}
}
