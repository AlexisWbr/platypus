<?php
namespace Platypus\Error;
use Platypus\Controller\Controller;

class ErrorController extends Controller
{
	function throwErrorAction()
	{
		$statusCode		= $this->bundle->getRoute()->getName();
		$statusMessage	= $this->bundle->getResponse()->statusCodes[$statusCode];

		$this->addToPage('pageTitle', $statusMessage);

		// $this->getResponse()->sendError($this->bundle->getRoute()->getName());
	}
}
