<?php
namespace Platypus\Cache;

abstract class Cache
{
	/**
	 * Utiliser un fichier de cache
	 * @param Miscellaneous $pWhatToGetBack l'entité à récupérer
	 * @param String $pCache_filename le nom du fichier cache a utiliser
	 * @param int $pDateExpiry Nombre de secondes avant que le fichier de cache ne soit considéré comme périmé
	 * @param boolean $pDateExpiry Si false, pas de date d'expiration
	 * @return boolean true Si entité de récupérée | false Si aucun fichier de récupéré
	 */
	static function read(&$pWhatToGetBack, $pCache_fileName, $pDateExpiry = 1800)
	{
		$cache_fileLocation = __DIR__.'/../../../app/cache/'.$pCache_fileName.'.php';

		// si fichier existant et que non périmé
		if (in_array($GLOBALS['KERNEL_CONFIG']['environment'], array('test','prod'), true ) && //mod prod
			file_exists($cache_fileLocation) &&	// fichier existant
			($pDateExpiry === false || time() < filemtime($cache_fileLocation) + $pDateExpiry) // date non expirée
			)
		{
			$cache_file_r = fopen($cache_fileLocation,'r');
			$recoveryCache = fread($cache_file_r, filesize($cache_fileLocation));
			fclose($cache_file_r);

			$pWhatToGetBack = unserialize($recoveryCache);

			return true;
		}

		else
			return false;
	}


	/**
	 * Ajouter/Créer/Modifier un fichier de cache
	 * @param Miscellaneous $pWhatToCache Entité à mettre en cache
	 * @param String $pCache_filename Nom du fichier de cache
	 */
	static function write($pWhatToCache, $pCache_fileName)
	{
		if (in_array($GLOBALS['KERNEL_CONFIG']['environment'], array('test','prod'), true ))
		{
			// if (!file_exists($pDestination)) mkdir($pDestination, 0777, true);
			$cache_fileLocation = __DIR__.'/../../../app/cache/'.$pCache_fileName.'.php';
			$dirname = dirname($cache_fileLocation);

			if (!is_dir($dirname)){
				mkdir($dirname, 0755, true);
			}

			$cache_file_w = fopen($cache_fileLocation,'w');
			fwrite($cache_file_w, serialize($pWhatToCache));
			fclose($cache_file_w);
		}
	}


	/**
	 * Supprimer les fichiers de cache
	 */
	static function clear()
	{
		$files = glob(__DIR__.'/../../../app/cache/*');

		foreach($files as $file) {
			if (is_dir($file)) {
				\Platypus\Controller\Tool\FilesTool::deleteFolder($file);
			} elseif (is_file($file)) {
				unlink($file);
			}
		}
	}
}
