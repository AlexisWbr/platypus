<?php
namespace Platypus\ORM\Shortcut;

trait GetModelTrait
{
	/**
	 * Petit raccourcit pour la methode getModel de ModelCollector qui évite un Class->getModelCollector->getModel
	 * du coup on a juste Class->getModel
	 */
	function getModel($pModelName, $pBundleName = null)
	{
		return ($this->modelCollector->getModel($pModelName, $pBundleName));
	}
}
