<?php
namespace Platypus\ORM;

class ORMException extends \Exception 
{
	function __construct($message, $code = 0, Exception $previous = null){
		parent::__construct($message, $code, $previous);
	}
}
