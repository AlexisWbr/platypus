<?php
namespace Platypus\ORM;

abstract class Model
{
	protected	$db;

	function __construct($pDb){
		$this->db = $pDb;
	}





	/**
	 * Compter le nbr d'entrées
	 */
	protected function orm_count($pReqsql, $pVarsToBind = [])
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);
		
		$entities = $req->fetch(\PDO::FETCH_NUM);

		$req->closeCursor();

		return $entities[0];
	}


	/**
	 * Récupérer une entrée
	 */
	protected function orm_getOne($pReqsql, $pEntityClass = null, $pVarsToBind = [])
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);
		
		if (!is_null($pEntityClass))
			$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $pEntityClass);
		else
			$req->setFetchMode(\PDO::FETCH_ASSOC);
		$entity = $req->fetch();

		$req->closeCursor();

		return $entity;
	}


	/**
	 * Récupérer plusieurs entrées
	 */
	protected function orm_getMany($pReqsql, $pEntityClass = null, $pVarsToBind = [])
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);
		
		if (!is_null($pEntityClass))
			$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $pEntityClass);
		else
			$req->setFetchMode(\PDO::FETCH_ASSOC);
		$entities = $req->fetchAll();

		$req->closeCursor();

		return $entities;
	}


	/**
	 * Récupérer plusieurs entrées et les classer en arbre
	 * A l'avantage de ne pas être récursif et de peupler facilement une liaison inter-objets sur une même table
	 * A le désavantage de ne pas permettre de lier plusieurs objets
	 */
	function orm_getManyNested($pReqsql, $pEntityClass = null, $pVarsToBind = [], $pIdRef, $pParentIdRef)
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);

		if (!is_null($pEntityClass))
			$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $pEntityClass);
		else
			$req->setFetchMode(\PDO::FETCH_ASSOC);
		$entities = \NestedSql::nestsResults($req, $pIdRef, $pParentIdRef, $pEntityClass);

		$req->closeCursor();

		return $entities;
	}


	/**
	 * Récupérer plusieurs entrées de plusieurs tables différentes et les classer en arbre de manière récursive
	 * A l'avantage de pouvoir utiliser plusieurs entités distinctes
	 * A le désavantage de ne pas pouvoir aller en profondeur pour les flat table relationnelles + la récursivité lourde pour les grands tableaux
	 */
	protected function orm_getManyNestedRecursive($pReqsql, $pEntityClass = [], $pVarsToBind = [])
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);

		$entities = \NestedSql::nestsResultsRecursive($req, $pEntityClass);

		$req->closeCursor();

		return $entities;
	}


	/**
	 * Exécuter une action sur une table
	 */
	protected function orm_action($pReqsql, $pVarsToBind = [], $pReturnLastInsertId = false)
	{
		$req = $this->db->prepare($pReqsql);

		foreach ($pVarsToBind as $varName => $value)
			$req->bindValue(':'.$varName, $value[0], constant('\PDO::PARAM_'.strtoupper($value[1])));

		$this->tryToExecute($req);

		$exec	= $req->rowCount();
		$lastId	= $this->db->lastInsertId();
		$return	= $pReturnLastInsertId ? array($req->rowCount(), $lastId) : $req->rowCount();

		$req->closeCursor();

		return $return;
	}


	// =================================================================
	// ==============================PRIVATE============================

	/**
	 * Execute la requete SQL en attrapant une potentielle erreur pour indiquer la vraie ligne d'erreur
	 */
	private function tryToExecute($pReq)
	{
		try {
			$pReq->execute();
		}
		catch(\PDOException $e) {
			$backTrace = debug_backtrace();
			throw new ORMException($e->getMessage().' produced by '.$backTrace[1]['file'].' on line '.$backTrace[1]['line']);
		}
	}
}
