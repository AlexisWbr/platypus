<?php
namespace Platypus\ORM;

abstract class Entity implements \ArrayAccess
{
	use \Platypus\Hydrator;

	protected	$id;
	protected	$errors = [];

	// le constructeur ne sert qu'à hydrater un potentiel tableau de donnees
	function __construct(array $pDonnees = [], $pHydrateAll = false)
	{
		if (!empty($pDonnees) && $pHydrateAll === false) {
			$this->hydrate($pDonnees); //utilise le trait Hydrator
		}
		elseif (!empty($pDonnees) && $pHydrateAll === true) {
			$this->hydrateAll($pDonnees); //utilise le trait Hydrator
		}
	}





	function isNew()
	{
		return empty($this->id);
	}


	function isValid()
	{
		return empty($this->errors);
	}


	//======================================================================
	// =======================REDEF DE \ArrayAccess=========================

	function offsetExists($pVar)
	{
		return isset($this->$pVar) && is_callable([$this, $pVar]);
	}

	function offsetSet($pVar, $pValue)
	{
		$method = 'set'.ucfirst($pVar);

		if (isset($this->$pVar) && is_callable([$this, $method]))
		{
			$this->$method($pValue);
		}
	}

	function offsetGet($pVar)
	{
		$method = 'get'.ucfirst($pVar);

		if (isset($this->$pVar) && is_callable([$this, $method]))
		{
			return $this->$method();
		}
	}

	function offsetUnset($pVar)
	{
		throw new \Exception('Impossible de supprimer une quelconque valeur');
	}


	// =================================================================
	// =============================SETTERS=============================
	
	function setId($pId){
		$this->id = (int)$pId;
	}
	

	// =================================================================
	// =============================GETTERS=============================
	
	function getId(){
		return $this->id;
	}

	function getErrors(){
		return $this->errors;
	}
}
