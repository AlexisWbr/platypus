<?php
namespace Platypus\ORM;

/**
 * Cette classe permet de stocker les différents Models demandés et qui seront ensuite utilisables directement dans un Controller.
 */
class ModelCollector
{
	protected	$location;	// nom du bundle d'où est instancé le ModelCollector
	protected	$db;
	protected	$modelCollect = [];

	function __construct($pDb, $pBundleName){
		$this->location = $pBundleName;
		$this->db = $pDb;
	}





	/**
	 * Ajoute le model dans $modelCollect[] s'il n'existe pas puis return cet Array.
	 * @param String $pModelName
	 * @param String $pBundleName
	 * @return Array Model $this->modelCollect
	 */
	function getModel($pModelName, $pBundleName = null)
	{
		if (empty($pModelName) || !is_string($pModelName))
			throw new \InvalidArgumentException("Le module spécifié doit être une chaine de caracteres non vide.");
		$pModelName = ucfirst($pModelName);

		if ($pBundleName == null)
			$bundleName = $this->location;
		else
			$bundleName = $pBundleName;


		// si le manager n'existe pas déjà dans $managers[]
		if (!isset($this->modelCollect['$pModelName'])) {
			$manager = '\\src\\'.$bundleName.'\\Model\\'.$pModelName.'Model';
			$this->modelCollect[$pModelName] = new $manager($this->db);
		}

		return $this->modelCollect[$pModelName];
	}
}
