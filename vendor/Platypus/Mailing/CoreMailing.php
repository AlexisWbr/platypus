<?php
namespace Platypus\Mailing;
use Symfony\Component\Yaml\Parser;

abstract class CoreMailing
{
	protected	$htmlMIMEVersion = "MIME-Version: 1.0";
	protected	$htmlContentType = "Content-type: text/html; charset=iso-8859-1";
	protected	$fromName;
	protected	$fromMail;
	protected	$replyTo;
	protected	$returnPath;
	protected	$cc = [];
	protected	$bcc = [];
	protected	$to = [];
	protected	$subject;
	protected	$message;

	function __construct(){
		$yamlParser = new Parser();
		$configs = $yamlParser->parse(file_get_contents(__DIR__.'/../../../app/config/mailing.yaml'));
		$this->fromName		= (string) $configs['fromName'];
		$this->fromMail		= (string) $configs['fromMail'];
		$this->replyTo		= (string) $configs['replyto'];
		$this->returnPath	= (string) $configs['returnpath'];
	}




	abstract function send();


	function htmlMIMEVersion($value)
	{
		$this->htmlMIMEVersion = $value;
		return $this;
	}

	function htmlContentType($value)
	{
		$this->htmlContentType = $value;
		return $this;
	}

	function fromName($value)
	{
		$this->fromName = $value;
		return $this;
	}

	function fromMail($value)
	{
		$this->fromMail = $value;
		return $this;
	}

	function replyTo($value)
	{
		$this->replyTo = $value;
		return $this;
	}

	function returnPath($value)
	{
		$this->returnPath = $value;
		return $this;
	}

	function cc($value)
	{
		$this->cc = $value;
		return $this;
	}

	function bcc($value)
	{
		$this->bcc = $value;
		return $this;
	}

	function to($value)
	{
		$this->to = $value;
		return $this;
	}

	function subject($value)
	{
		$this->subject = $value;
		return $this;
	}

	function message($value)
	{
		$this->message = $value;
		return $this;
	}
}
