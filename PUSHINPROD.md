CONFIGURATIONS
==============

Connexion SQL
-------------

Repertoire: `app/config/PDOConnext.php`

* Modifier les infos de connexion à la BDD
* Mettre en place la bdd sql, le fichier se trouve dans `app`


Configurations serveur
----------------------

Repertoire: `app/config/config.yaml`

* Passer en prod
* Lancer le monde maintenance ou non (toute les pages passeront en http 403)
* Changer en conséquence `base_url`
* Changer en conséquence `bootstrap` (en général toujours vide puisque le .htaccess réécrit l'url)
* Pour passer en "maintenance de surface" dans le .htaccess, décommenter la ligne dans "BOOT SUR LE BOOTSTRAP.PHP"


Configurer les mails
--------------------

Repertoire: `app/config/mailing.yaml`

Modifier en conséquence les infos de mailing par défaut. Utilisé principalement pour la page contact par défaut.


FICHIERS
========

* Supprimer le cache dans `app/cache` (pas le dossier racine, seulement son contenu)
* Supprimer le fichier console dans `app`
* Supprimer le fichier de la bdd sql dans `app`
* Supprimer les fichier CONFIGURE.md, PUSHINPROD.md dans le dossier racine
