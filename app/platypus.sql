-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 07 Mars 2016 à 05:39
-- Version du serveur :  5.7.9
-- Version de PHP :  7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `platypus`
--

-- --------------------------------------------------------

--
-- Structure de la table `lib_file`
--

DROP TABLE IF EXISTS `lib_file`;
CREATE TABLE IF NOT EXISTS `lib_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `folder_id` (`folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lib_folder`
--

DROP TABLE IF EXISTS `lib_folder`;
CREATE TABLE IF NOT EXISTS `lib_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `color` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `redac_news`
--

DROP TABLE IF EXISTS `redac_news`;
CREATE TABLE IF NOT EXISTS `redac_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `display` tinyint(4) NOT NULL DEFAULT '0',
  `date_release` datetime DEFAULT CURRENT_TIMESTAMP,
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `editor_id` (`editor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `redac_page`
--

DROP TABLE IF EXISTS `redac_page`;
CREATE TABLE IF NOT EXISTS `redac_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'page/lien',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_slug` varchar(510) COLLATE utf8_unicode_ci NOT NULL,
  `name_alternative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `position` smallint(6) NOT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '0',
  `rights_edit` tinyint(1) NOT NULL DEFAULT '0',
  `rights_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `parent_id` (`parent_id`),
  KEY `author_id` (`author_id`),
  KEY `editor_id` (`editor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `redac_page_cat`
--

DROP TABLE IF EXISTS `redac_page_cat`;
CREATE TABLE IF NOT EXISTS `redac_page_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `rights_add` tinyint(1) DEFAULT '0',
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `redac_slider`
--

DROP TABLE IF EXISTS `redac_slider`;
CREATE TABLE IF NOT EXISTS `redac_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'img/txt',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `redac_slider_slide`
--

DROP TABLE IF EXISTS `redac_slider_slide`;
CREATE TABLE IF NOT EXISTS `redac_slider_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `url_attached` text COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(44) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ranks_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_date` datetime NOT NULL,
  `autologin_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `ranks_id`, `last_ip`, `registration_date`, `autologin_key`, `activation_key`) VALUES
(11, 'Admin', 'mail@mail.com', '$2y$11$5Byi12HsN5XSKpEsJWzns.ho6z5XQ0XNdgXdgGJGGbG4N8gRlIzmy', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', '::1', '2016-02-19 19:34:46', '018fd64e3db0755d2e38adc8e8694ca7', NULL),
(12, 'Membre', 'mail@mail.com', '$2y$11$zWViBINn.dAKeUykezV7DOGlVL2u3EeFUR0EuaCrvgs3aBWP6B2.G', 'a:1:{i:0;s:1:"2";}', '::1', '2016-02-22 10:20:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_banlist`
--

DROP TABLE IF EXISTS `user_banlist`;
CREATE TABLE IF NOT EXISTS `user_banlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `until` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_banned_ip`
--

DROP TABLE IF EXISTS `user_banned_ip`;
CREATE TABLE IF NOT EXISTS `user_banned_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `until` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_extended`
--

DROP TABLE IF EXISTS `user_extended`;
CREATE TABLE IF NOT EXISTS `user_extended` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_connexion` datetime NOT NULL,
  `birthday` datetime NOT NULL,
  `twitter_account` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_rank`
--

DROP TABLE IF EXISTS `user_rank`;
CREATE TABLE IF NOT EXISTS `user_rank` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name_tag` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `own` text COLLATE utf8_unicode_ci,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 spéciale / 1 normal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_rank`
--

INSERT INTO `user_rank` (`id`, `name`, `name_tag`, `own`, `type`) VALUES
(1, 'ADMIN', 'Administrateur', NULL, 0),
(2, 'MEMBRE', 'Utilisateur', NULL, 0),
(3, 'BIDON', 'Un rang bidon', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_rank_acl`
--

DROP TABLE IF EXISTS `user_rank_acl`;
CREATE TABLE IF NOT EXISTS `user_rank_acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rank_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `rank_id` (`rank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_rank_acl`
--

INSERT INTO `user_rank_acl` (`id`, `user_id`, `rank_id`) VALUES
(1, 11, 1),
(2, 11, 2),
(3, 12, 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `lib_file`
--
ALTER TABLE `lib_file`
  ADD CONSTRAINT `lib_file_ibfk_1` FOREIGN KEY (`folder_id`) REFERENCES `lib_folder` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `lib_folder`
--
ALTER TABLE `lib_folder`
  ADD CONSTRAINT `lib_folder_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `lib_folder` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `redac_news`
--
ALTER TABLE `redac_news`
  ADD CONSTRAINT `redac_news_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `redac_news_ibfk_2` FOREIGN KEY (`editor_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION;

--
-- Contraintes pour la table `redac_page`
--
ALTER TABLE `redac_page`
  ADD CONSTRAINT `redac_page_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `redac_page_cat` (`id`),
  ADD CONSTRAINT `redac_page_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `redac_page` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `redac_page_ibfk_3` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `redac_page_ibfk_4` FOREIGN KEY (`editor_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `redac_slider_slide`
--
ALTER TABLE `redac_slider_slide`
  ADD CONSTRAINT `redac_slider_slide_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `redac_slider` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_banlist`
--
ALTER TABLE `user_banlist`
  ADD CONSTRAINT `user_banlist_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_extended`
--
ALTER TABLE `user_extended`
  ADD CONSTRAINT `user_extended_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user_rank_acl`
--
ALTER TABLE `user_rank_acl`
  ADD CONSTRAINT `user_rank_acl_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_rank_acl_ibfk_2` FOREIGN KEY (`rank_id`) REFERENCES `user_rank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
