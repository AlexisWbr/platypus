<?php
namespace app\config;

abstract class PDOConnect
{
	static function mySqlConnect()
	{
		try {
			$db = new \PDO('mysql:host=localhost; charset=utf8; dbname=platypus', 'root', '');
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
		catch (\Exception $e) {
			die('Erreur : ' . $e->getMessage());
		}

		return $db;
	}
}
