<?php
namespace app\component;
use Platypus\Controller\Controller;

use Platypus\Cache\Cache;
use src\UserBundle\Controller\Log\LogController;

trait MainController
{
	protected	$executeTheseFunctions = ['superadminRole', 'autologin', 'refreshUserSession'];





	/**
	 * Passer en superadmin
	 */
	function superadminRole()
	{
		if (isset($_GET['superadmin-on'])) {
			if ($this->getSession()->isAuthenticated() && in_array('ADMIN', $_SESSION['user']['ranks']))
				$_SESSION['superadminrole'][0] = 'SUPERADMIN';
		} elseif (isset($_GET['superadmin-off']) && isset($_SESSION['superadminrole'])) {
			unset($_SESSION['superadminrole']);
		}
	}


	/**
	 * Connexion automatique s'il existe un cookie et que pas encore connecté
	 */
	private function autologin()
	{
		// si pas authentifié mais existance d'un cookie d'autolog
		if (!$this->getSession()->isAuthenticated() && isset($_COOKIE['atlg'])) {
			$modelLog	= $this->getModel('Log', 'UserBundle');
			$user		= $modelLog->loginByCookie(md5($_COOKIE['atlg'].LogController::$autologinCookieSalt));

			if ($user != false && !$this->getModel('Banlist', 'UserBundle')->getOneById($user['id'], 'user_id', 'AND until > NOW()')) {
				$userRankACLModel	= $this->getModel('UserRankACL', 'UserBundle');
				$rankModel			= $this->getModel('Rank', 'UserBundle');
				$user->ranks		= \src\UserBundle\Controller\UserRanksUtility::getOwnedRanks($user['id'], $userRankACLModel, $rankModel);

				LogController::createLoginSession($user);
				// si redirigé par le firewall
				if (isset($_SESSION['referer']))
					$this->getResponse()->redirect($_SESSION['referer']);
				// si connexion classique
				else
					$this->getResponse()->redirect($this->getRequest()->referer());
			} else {
				$this->getResponse()->setcookie('atlg');
			}
		}
	}


	/**
	 * Appelé que si existe une session
	 * Si l'id d'un user est présent dans le cache, besoin de rafraichir la session avec les bonnes informations
	 */
	private function refreshUserSession($pId = null)
	{
		if (is_null($pId)) {
			if ($this->getSession()->isAuthenticated())
				$this->refreshUserSession($_SESSION['user']['id']);
		} else {
			$cache_fileName		= 'usersToRefreshList';

			// va chercher les utilisateurs dont la session à besoin d'une mise à jour
			$usersToRefreshList	= [];
			Cache::read($usersToRefreshList, $cache_fileName);

			// si l'utilisateur courant à besoin de cette MAJ (id présent dans la liste ou env en dev)
			if (array_key_exists($_SESSION['user']['id'], $usersToRefreshList) || $GLOBALS['KERNEL_CONFIG']['environment'] =='dev')
			{
				$userRankACLModel	= $this->getModel('UserRankACL', 'UserBundle');
				$rankModel			= $this->getModel('Rank', 'UserBundle');

				// update les rangs
				$_SESSION['user']['ranks'] = \src\UserBundle\Controller\UserRanksUtility::getOwnedRanks($pId, $userRankACLModel, $rankModel);

				// on retire l'utilisateur de ceux ayant besoin d'une MAJ
				unset($usersToRefreshList[$_SESSION['user']['id']]);
				Cache::write($usersToRefreshList, $cache_fileName);
			}
		}
	}
}
