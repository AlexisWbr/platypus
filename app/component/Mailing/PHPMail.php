<?php
namespace app\component\Mailing;
use Platypus\Mailing\CoreMailing;

class PHPMail extends CoreMailing
{
	function send()
	{
		/*=== HEADERS ===*/
		/*===============*/
		$headers =	$this->htmlMIMEVersion."\r\n";
		$headers .=	$this->htmlContentType."\r\n";

		$headers .=	"To: ";
		foreach ($this->to as $name => $emailAddress) {
			$headers .= $name." ";
			$headers .= "<".$emailAddress.">";
			if ($emailAddress !== end($this->to)) $headers .= ", ";
		}
		$headers .= "\r\n";

		$headers .=	"From: ".$this->fromName."<".$this->fromMail.">\r\n";
		$headers .=	"Reply-To: ".$this->replyTo."\r\n";
		$headers .=	"Return-Path: ".$this->returnPath."\r\n";

		$headers .=	"CC: ";
		foreach ($this->cc as $element) {
			$headers .= $element;
			if ($element !== end($this->cc)) $headers .= ",";
		}
		$headers .= "\r\n";

		$headers .=	"BCC: ";
		foreach ($this->bcc as $element) {
			$headers .= $element;
			if ($element !== end($this->bcc)) $headers .= ",";
		}
		$headers .= "\r\n";


		/*=== RECIPIENT ===*/
		/*=================*/
		$to = "";
		$to .=	"To: ";
		foreach ($this->to as $value) {
			$to .= $value;
			if ($value !== end($this->to)) $to .= ", ";
		}
		$to .= "\r\n";


		/*=== SEND ===*/
		/*============*/

		mail($to, $this->subject, $this->message, $headers);
	}
}
