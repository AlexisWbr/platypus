<?php
namespace app\component\Mailing;
use Platypus\Mailing\CoreMailing;

class MandrillMail extends CoreMailing
{
	function send()
	{
		try {
			if ($GLOBALS['KERNEL_CONFIG']['environment'] == 'prod') {
				$mandrill = new \Mandrill('_IBglFRqtGs7HkdYM3sfig'); // Julien PROD
				// $mandrill = new \Mandrill('XAZ-nYbyhJ3oVlsBLyXpNw'); // Alexis PROD
			}
			else{
				$mandrill = new \Mandrill('-ugt1bR2FNCZppJCG0cC5Q'); // Alexis dev-testmode
			}

			$message = new \Mandrill_Messages($mandrill);

			$mandrill_message = array(
				"html"=> $this->message,
				"subject"=> $this->subject,
				"from_email"=> $this->fromMail,
				"from_name"=> $this->fromName,
				"headers" => array("Reply-To" => $this->replyTo)
			);

			foreach ($this->to as $name => $emailAddress) {
				$mandrill_message["to"][] = ["email" => $emailAddress, "name" => $name,"type" => "to"];
			}

			foreach ($this->cc as $element) {
				$mandrill_message["to"][] = ["email" => $element, "type" => "cc"];
			}
			
			foreach ($this->bcc as $element) {
				$mandrill_message["to"][] = ["email" => $element, "type" => "bcc"];
			}

			$message->send($mandrill_message, true);

		} catch(\Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}
	}
}

/*
Dépendances:
	vendor\Mandrill
*/
