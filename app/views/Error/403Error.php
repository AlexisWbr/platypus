<div id="error403">
	<span class="code">403</span>
	<p>Vous n'êtes pas autorisé à accéder à cette page.</p>

	<?php if(!preg_match('#(?:http://'.$this->getRequest()->host().'){0,1}'.$this->getRequest()->requestURI().'#', $this->getRequest()->referer())) : ?>
		<a href="<?= $this->getRequest()->referer() ?>">Retour page précédente</a>
	<?php endif ?>
	<a href="<?= $this->getRequest()->root() ?>">Revenir à l'accueil</a>
</div>
