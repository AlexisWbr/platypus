<div id="error404">
	<span class="code">404</span>
	<p>Cette page n'existe pas ou plus.</p>

	<?php if(!preg_match('#(?:http://'.$this->getRequest()->host().'){0,1}'.$this->getRequest()->requestURI().'#', $this->getRequest()->referer())) : ?>
		<a href="<?= $this->getRequest()->referer() ?>">Retour page précédente</a>
	<?php endif ?>
	<a href="<?= $this->getRequest()->root() ?>">Revenir à l'accueil</a>
</div>
