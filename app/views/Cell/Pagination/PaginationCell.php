<?php
namespace app\views\Cell\Pagination;
use Platypus\View\Cell;

class PaginationCell extends Cell
{
	function display()
	{
		$currentPgto = $this->vars[0];
		$totalPgto	 = $this->vars[1];
		$link		 = $this->vars[2];
		$buttons	 = $this->vars[3];

		$beginPgto	 = ($currentPgto-5 < 1) ? 1 : $currentPgto-5+1;

		$this->addToPage('beginPgto', $beginPgto);
		$this->addToPage('currentPgto', $currentPgto);
		$this->addToPage('totalPgto', $totalPgto);
		$this->addToPage('link', $link);
		$this->addToPage('buttons', $buttons);
	}
}
