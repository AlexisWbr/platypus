<ul>
	<li><a href="<?= $link ?>?p=1" class="<?php if(!($currentPgto > 1 && $currentPgto <= $totalPgto)) echo 'disabled'; ?>"><?= $buttons[0] ?></a></li>
	<li><a href="<?= $link ?>?p=<?= $currentPgto-1 ?>" class="<?php if(!($currentPgto > 1 && $currentPgto <= $totalPgto)) echo 'disabled'; ?>"><?= $buttons[1] ?></a></li>
		<?php for ($i=$beginPgto; $i < $currentPgto; $i++): ?>
			<?php if($i > $totalPgto) break; ?>
			<li><a href="<?= $link ?>?p=<?= $i ?>"><?= $i ?></a></li>
		<?php endfor ?>

		<li><a class="current" href="<?= $link ?>?p=<?= $currentPgto ?>"><?= $currentPgto ?></a></li>
		
		<?php for ($i=$currentPgto+1; $i < $currentPgto+5; $i++): ?>
			<?php if($i > $totalPgto) break; ?>
			<li><a href="<?= $link ?>?p=<?= $i ?>"><?= $i ?></a></li>
		<?php endfor ?>

	<li><a href="<?= $link ?>?p=<?= $currentPgto+1 ?>" class="<?php if($currentPgto >= $totalPgto) echo 'disabled'; ?>"><?= $buttons[2] ?></a></li>
	<li><a href="<?= $link ?>?p=<?= $totalPgto ?>" class="<?php if($currentPgto >= $totalPgto) echo 'disabled'; ?>"><?= $buttons[3] ?></a></li>
</ul>
