<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= isset($pageTitle)?$pageTitle.' - ':'' ?><?= $GLOBALS['KERNEL_CONFIG']['websiteName'] ?></title>
		<style type="text/css">
			html, body{
				height: 100%;
				margin: 0;
				font-family: sans-serif;
			}
			body{
				background: #ffffff;
				background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod…BoZWlnaHQ9IjEwMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
				background: -moz-radial-gradient(center, ellipse cover, #ffffff 0%, #ffffff 40%, #d6d6d6 100%);
				background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#ffffff), color-stop(40%,#ffffff), color-stop(100%,#d6d6d6));
				background: -webkit-radial-gradient(center, ellipse cover, #ffffff 0%,#ffffff 40%,#d6d6d6 100%);
				background: -o-radial-gradient(center, ellipse cover, #ffffff 0%,#ffffff 40%,#d6d6d6 100%);
				background: -ms-radial-gradient(center, ellipse cover, #ffffff 0%,#ffffff 40%,#d6d6d6 100%);
				background: radial-gradient(ellipse at center, #ffffff 0%,#ffffff 40%,#d6d6d6 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#d6d6d6',GradientType=1 );
				text-align: center;
			}
			#error302 ,
			#error403 ,
			#error404 {
				margin-top: 50px;
			}

			#error302 .code ,
			#error403 .code ,
			#error404 .code {
				font-size: 110px;
				color: rgb(74, 74, 74);
				text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.24);
			}

			#error302 p,
			#error403 p,
			#error404 p{
				text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.34);
				font-size: 25px;
			}

			#error302 a,
			#error403 a,
			#error404 a{
				text-decoration: underline;
				cursor: pointer;
				margin: 20px 10px;
				display: inline-block;
				color: #4E4E4E;
			}
		</style>
	</head>
	<body>

			<?= $contentBundle ?>

	</body>
</html>
