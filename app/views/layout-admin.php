<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= isset($pageTitle)?$pageTitle.' - ':'' ?>Espace Administration</title>
		<link rel="stylesheet" type="text/css" href="<?= $this->getRequest()->root() ?>/web/vendor/gommette/css/gommette.css">
		<link rel="stylesheet" type="text/css" href="<?= $this->getRequest()->root() ?>/web/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $this->getRequest()->root() ?>/web/vendor/tinymce/tinymce.min.js">
		<link rel="stylesheet" type="text/css" href="<?= $this->getRequest()->root() ?>/web/vendor/datetimepicker-2.4.7/jquery.datetimepicker.css"/ >
		<link rel="stylesheet" type="text/css" href="<?= $this->getRequest()->root() ?>/web/css/adminpanel.css">
		<!--[if lt IE 10 ]>
			<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/vendor/selectivizr.js"></script>
			<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/vendor/respond.js"></script>
			<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/vendor/html5shiv/html5shiv.js"></script>
			<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/vendor/html5shiv/html5shiv-printshiv.js"></script>
			<span class="warning-IE8">Votre navigateur est obsolète et ne respecte plus les normes utilisées par les sites internet modernes.
			Ceux-ci peuvent apparaître déformés et vous faire courir un risque de sécurité.</span>
		<![endif]-->
		<noscript><span class="warning-noscript">Ho :'( Javascript est désactivé, certaines fonctionnalité de ce site seront inaccessibles. Pour y accéder pleinement, activez Javascript sur ce domaine.</span></noscript>
	</head>
	<body>
		<div id="popupContainer">
			<?php foreach ($this->Helper['User']->getFlash('error') as $key => $value) : ?>
				<div class="popup popupColor2 JSdelOnClick"><?= $value ?></div>
			<?php endforeach ?>
			<br />
			<?php foreach ($this->Helper['User']->getFlash('success') as $key => $value) : ?>
				<div class="popup popupColor1 JSdelOnClick"><?= $value ?></div>
			<?php endforeach ?>
			<br />
		</div>

		<div class="u-wrapper u-containSidebar">
			<header id="headerMain">
				<nav id="navMain" class="u-nav clearfix" role="navigation">
					<div class="u-nav-header"><button class="u-toggle fa fa-bars" type="button"><span>Navigation</span></button></div>
					<ul class="toLeft">
						<li><a href="<?= $this->Helper['Routing']->url('admin_home_index') ?>">Espace Administration</a></li>
						<li><a href="<?= $this->getRequest()->root() ?>" title="Voir mon site"><i class="fa fa-sign-out"></i> Voir mon site</a></li>
					</ul>
					<ul class="toRight">
						<li><a href="<?= $this->Helper['Routing']->url('user_log_logout') ?>" title="Se déconnecter"><i class="fa fa-power-off"></i> Déconnexion</a></li>
					</ul>
				</nav>
			</header>
			<nav id="sidebarMain" class="u-sidebar" role="navigation">
				<button type="button" class="u-toggle fa fa-bars"><span>Réafficher le menu latéral</span></button>
				<ul class="u-sidebar-nav">
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'name', 'admin_home_index') ?>">
						<a href="<?= $this->Helper['Routing']->url('admin_home_index') ?>"><i class="fa fa-home fa-fw"></i> Accueil</a>
					</li>
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminPage') ?>">
						<a href="<?= $this->Helper['Routing']->url('redac_adminPage_index') ?>"><i class="fa fa-file-text fa-fw"></i> Pages</a>
					</li>
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminNews') ?>">
						<a href="<?= $this->Helper['Routing']->url('redac_adminNews_index') ?>"><i class="fa fa-newspaper-o fa-fw"></i> Actualités</a>
					</li>
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminSlider') ?>">
						<a href="<?= $this->Helper['Routing']->url('redac_adminSlider_index') ?>"><i class="fa fa-object-ungroup fa-fw"></i> Sliders</a>
					</li>
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminFile') ?>">
						<a href="<?= $this->Helper['Routing']->url('lib_adminFile_index') ?>"><i class="fa fa-files-o fa-fw"></i> Bibliothèque</a>
					</li>
					<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminUser') ?>">
						<a href="<?= $this->Helper['Routing']->url('user_adminUser_index') ?>"><i class="fa fa-users fa-fw"></i> Utilisateurs</a>
						<ul>
							<li class="<?= $this->Helper['Routing']->currentPage($this, 'controller', 'AdminRank') ?>">
								<a href=""><a href="<?= $this->Helper['Routing']->url('rank_adminRank_index') ?>"><i class="fa fa-graduation-cap fa-fw"></i> Rangs</a></a>
							</li>
						</ul>
					</li>
					<li class="u-toggle">
						<a>
							<i class="fa fa-bars fa-fw"></i> Réduire le menu
						</a>
					</li>
				</ul>
			</nav>
			
			<div class="u-nextToSidebar-wrapper">
				<?php if($this->Helper['Secure']->hasRank(['SUPERADMIN'], $this->getSession()->getAttr('superadminrole'))) : ?>
					<div class="blockWarning">
						<i class="fa fa-exclamation-triangle"></i> ATTENTION  <i class="fa fa-exclamation-triangle"></i>
						<br>L'utilisation du mode superadmin peut entraîner des dommages irréversibles à votre site. N'effectuez des modifications que si vous savez pertinemment ce que vous faites --
						<a style="text-decoration: underline" href="?superadmin-off">Quitter le mode superadmin</a>
					</div>
				<?php endif ?>
				
				<?= $contentBundle ?>

				<footer id="footerMain" class="u-footer">
					<?php printf("Page générée en %.2fs\n", microtime(true) - $GLOBALS['microtime']) ?>
					<span style="float: right"><a href="?superadmin-on" title="Déclencher le mode superadmin"><i class="fa fa-rocket"></i></a></span>
				</footer>
			</div>
		</div>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/vendor/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/nestedSortable/jquery.mjs.nestedSortable.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/datetimepicker-2.4.7/build/jquery.datetimepicker.full.min.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/vendor/gommette/js/gommette.min.js"></script>
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/js/adminpanel.js"></script>
		<!-- INSTANCES -->
		<script type="text/javascript" src="<?= $this->getRequest()->root() ?>/web/js/adminpanel_instances.js"></script>
		<script type="text/javascript">
			tinymce.init({
				content_css : "<?= $this->getRequest()->root() ?>/web/vendor/gommette/css/gommette.css, <?= $this->getRequest()->root() ?>/web/css/adminpanel_tinymce.css",
				selector: ".tinymceTextarea",
				height : 300,
				language: 'fr_FR',
				relative_urls : true,
				document_base_url : "<?= $this->getRequest()->root() ?>/",
				plugins: "image,link,code,textcolor,colorpicker,fullscreen,template",
				style_formats: [
					{title: 'Titre niveau 1', block: 'h2'},
					{title: 'Titre niveau 2', block: 'h3'},
					{title: 'Titre niveau 3', block: 'h4'}
				],
				templates: [
					{title: '2 colonnes de textes', description: 'Deux colonnes alignées horizontalement puis verticalement quand l\'écran est trop petit', url: '<?= $this->getRequest()->root() ?>/src/RedactionBundle/Ressources/templatesTinyMCE/page_gommette_columns1.html'},
					{title: 'Texte à gauche - contenu à droite', description: 'Deux colonnes alignées horizontalement puis verticalement quand l\'écran est trop petit', url: '<?= $this->getRequest()->root() ?>/src/RedactionBundle/Ressources/templatesTinyMCE/page_gommette_columns2.html'},
					{title: 'Texte à droite - contenu à gauche', description: 'Deux colonnes alignées horizontalement puis verticalement quand l\'écran est trop petit', url: '<?= $this->getRequest()->root() ?>/src/RedactionBundle/Ressources/templatesTinyMCE/page_gommette_columns3.html'},
					{title: '2 colonnes en dessous - contenu en haut', description: 'Deux colonnes alignées horizontalement puis verticalement quand l\'écran est trop petit', url: '<?= $this->getRequest()->root() ?>/src/RedactionBundle/Ressources/templatesTinyMCE/page_gommette_columns4.html'},
					{title: 'Citation', description: 'Un block de citation', url: '<?= $this->getRequest()->root() ?>/src/RedactionBundle/Ressources/templatesTinyMCE/page_blockquote.html'}
				],

				menu : {
					file: {title: 'File', items: 'newdocument'},
					edit: {title: 'Edit', items: 'undo redo | cut copy past | selectall'},
					insert: {title: 'Insert', items: 'link image template'},
					view: {title: 'View', items: 'visualaid fullscreen'},
					format: {title: 'Format', items: 'formats | template bold italic underline strikethrough superscript subscript | removeformat'},
					tools: {title: 'Tools', items: 'code'}
				},
				menubar: 'file,edit,insert,view,format,tools',
				toolbar: 'undo redo | bold italic removeformat | alignleft aligncenter alignright alignjustify template | forecolor fontselect fontsizeselect | bullist numlist | outdent indent | link image | fullscreen'
			});
		</script>
	</body>
</html>
