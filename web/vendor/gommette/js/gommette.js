/*!
* Gommette
*
* TODO :
*/

$(window).load(function(){
$(function () {

	//==============================================================
	//                           TOOLS
	//==============================================================

	//============================================================
	//================WAIT FOR THE END OF A REZING================

	var idResize;
	$(window).bind('resize orientationchange', function(){
		clearTimeout(idResize);
		idResize = setTimeout(doneResizing, 500);
	});

	var wScreen = $(window).width();
	function doneResizing() {
		wScreen = $(window).width();
	}





	//==============================================================
	//                     MAGIK STICKY FOOTER
	//==============================================================

	var hFooter = $('.u-footer').outerHeight();

	if(!$('.u-fixFooter').length)
		$('<div class="u-fixFooter"></div>').insertBefore('.u-footer');
	$('.u-fixFooter').css('padding-bottom', hFooter);





	// =============================================================
	//                            MODAL BOX
	// =============================================================

	var modalTrigger	= ('.modalTrigger');
	var modalContainer	= ('.u-modalContainer');
	var modalContain	= ('.u-modalContain');
	var modalBlurBackg	= ('u-modalOverlay');
	var modalBlock		= (modalContainer+' '+modalContain);

	// Apparition de la modale
	$(modalTrigger).on('click', function(){
		var modalId		= $(this).attr('data-modal');

		$(modalId+modalContainer).toggleClass('unfolded');

		if ($(modalId+modalContainer).hasClass('unfolded')) {
			if($('.'+modalBlurBackg).length == 0)
				$('body').prepend('<div class="'+modalBlurBackg+'"></div>');
			$('.'+modalBlurBackg).removeClass('u-none');
		}
		else {
			$('.'+modalBlurBackg).addClass('u-none');
		}
	});

	// Disparition de la modale
	$(document).on('click', '.'+modalBlurBackg, function(){
		$(modalContainer).removeClass('unfolded');
		$('.'+modalBlurBackg).addClass('u-none');
	});





	//==============================================================
	//                           SIDEBAR
	//==============================================================

	if($('.u-sidebar').length)
	{
		var wSidebar = $('.u-sidebar').outerWidth();

		$('.u-sidebar .u-toggle').on('click', function(){
			toggleSidebar();
		});

		function toggleSidebar()
		{
			$('.u-sidebar').toggleClass('undisplay');
			$('.u-sidebar button.u-toggle').toggleClass('display');
			$('.u-sidebar .u-sidebar-nav').toggleClass('undisplay');
			$('.u-wrapper').toggleClass('sidebarFolded');
		}
	}





	//==============================================================
	//                       NAVIGATION BAR
	//==============================================================

	var wResponsiveNav = 768;														// width from which the navbar get her responsive form
	
	var nav = $('.u-nav');															// navbar name
	var togglebtn = $('.u-toggle');												// toggle button navbar name

	var firstDropdown = nav.find('> ul > li > ul, > * > ul > li > ul').parent();	// 1st level dropdowns
	var firstDropdownLink = firstDropdown.find('> a');								// 1st level dropdown links
	var dropdown = $('li ul').parent();											// all dropdowns
	var dropdownLink = dropdown.find('> a');										// all dropdown links


	// ======================================================
	// =====================AUTOMATION=======================

	// automatically add a .dropdown to symplify the HTML
	dropdown.addClass('dropdown');
	// automatically add a navigation and aria-expanded tag
	nav.attr('role', 'navigation');
	dropdownLink.attr('aria-expanded', 'false');
	// remove tab possibility by default when menu is not unrolled
	dropdownLink.next('ul').find('a').attr('tabIndex',-1);


	// ======================================================
	// =====================FUNCTIONS========================

	function openMenu(pThis)
	{
		pThis.next('ul').addClass('unroll'); // unroll the submenu
		pThis.next('ul').find('> li > a').attr('tabIndex',0); // permit to tab in all link in submenu unrolled
		pThis.parent('li').addClass('u-active'); // add a class to indicate which dropdown is open
		pThis.attr('aria-expanded', 'true'); // indicate the menu is now open
		// console.log('openMenu');
	}
	function closeMenu(pThis)
	{
		pThis.next('ul').removeClass('unroll'); // hide the submenu
		pThis.next('ul').find('.unroll').removeClass('unroll'); // hide all potential unrolled deep submenu
		pThis.next('ul').find('li > a').attr('tabIndex',-1); // remove tab possibility
		pThis.parent('li').removeClass('u-active'); // remove current .u-active
		pThis.next('ul').find('.u-active').removeClass('u-active'); // remove current all sub-dropdowns .u-active
		pThis.next('ul').find('.dropdown > a').attr('aria-expanded', 'false'); // indicate the menu is now closed
		// console.log('CloseMenu');
	}
	function closeAll(pThis)
	{
		pThis.find('> * > ul > li ul, > ul > li ul').removeClass('unroll');
		pThis.find('> * > ul > li ul, > ul > li ul').find('> li > a').attr('tabIndex',-1);
		pThis.find('.u-active').removeClass('u-active');
		pThis.find('.dropdown > a').attr('aria-expanded', 'false');
		// console.log('CloseAll');
	}
	function stickyOrNot(pNavPlacement)
	{
		if (wScreen > wResponsiveNav)
		{
			if ($(document).scrollTop() > pNavPlacement)
				$(document).find(nav).css('position', 'fixed');
			if ($(document).scrollTop() < pNavPlacement)
				$(document).find(nav).css('position', 'relative');
		}
		else
		{
			$(document).find(nav).css('position', 'relative');
		}
	}


	// ======================================================
	// =======================LOGIC==========================

	// === FOR OnCLICK NAVIGATION ===
	// ==============================

	// if open a submenu in nav (1st level) then close all the over
	firstDropdownLink.on('click', function(e){
		if ($(this).closest(nav).hasClass('u-onClick'))
		{
			e.preventDefault();
			if (!$(this).next('ul').hasClass('unroll'))
				closeAll(nav);
		}
	});

	// if want to open/close a submenu
	dropdownLink.on('click', function(e){
		if ($(this).closest(nav).hasClass('u-onClick'))
		{
			if ($(this).parent().hasClass('dropdown'))
				e.preventDefault();
			if (!$(this).next('ul').hasClass('unroll'))
				openMenu($(this));
			else
				closeMenu($(this));
		}
	});

	// === FOR KEYBOARD NAVIGATION ===
	// ===============================

	// if open a submenu in nav (1st level) then close all the over
	firstDropdownLink.on('keydown', function(e){
		if ((e.which == 13 || e.which == 38 || e.which == 40) && !$(this).next('ul').hasClass('unroll'))
		{
			e.preventDefault();
			closeAll(nav);
		}
	});

	// if want to open/close a submenu
	dropdownLink.on('keydown', function(e){
		if (e.which == 13 || e.which == 38 || e.which == 40)
		{
			e.preventDefault();
			// if already opened then close it
			if ($(this).next('ul').hasClass('unroll')){
				closeMenu($(this));
			}
			// if closed then open it
			else{
				openMenu($(this));
			}
		}
	});

	// === CLOSE ALL DROPDOWNS ===
	// ===========================
	// if hover the navbar or click outside the navbar then hide every potential unrolled submenu

	nav.hover(function(){
		// if not onClick navbar
		if (!$(this).hasClass('u-onClick'))
		{
			if (wScreen > wResponsiveNav && nav.find('.unroll').length)
				closeAll($(this));
		}
	});
	$(document).click(function(e){
		// if not onClick navbar
		if (!$(e.target).closest(nav).hasClass('u-onClick'))
		{
			if (wScreen > wResponsiveNav && nav.find('.unroll').length)
				closeAll(nav);
		}
	});
	$(document).on('keydown', function(e){
		if (e.which == 27 && wScreen > wResponsiveNav && nav.find('.unroll').length)
			closeAll(nav);
	});

	// === FOR RESPONSIVE TOUCH SCREEN NAVBAR ===
	// ==========================================

	// toggle navbar with button
	$('.u-nav .u-toggle').on('click', function(e){
		$(this).closest(nav).toggleClass('unroll');
		// if closing the menu then close all submenus
		if (!$(this).closest(nav).hasClass('unroll'))
			closeAll($(this).closest(nav));
	});

	dropdownLink.on('click', function(e){
		if (wScreen < wResponsiveNav)
		{
			e.preventDefault();

			if (!$(this).closest(nav).hasClass('u-onClick'))
			{
				// if already opened then close it
				if ($(this).next('ul').hasClass('unroll')){
					closeMenu($(this));
				}
				// if closed then open it
				else{
					openMenu($(this));
				}
			}
		}
	});

	// === SCROLL STICKY NAVBAR ===
	// ============================

	// event listener only if necessary
	if ($(document).find(nav).hasClass('u-sticky'))
	{
		// save the actual placement of the navbar relative to the top of document
		navPlacement = $(document).find('.u-nav').offset().top;

		// if resize then re-save the real placement of the navbar
		$(window).on('resize orientationchange', function(){
			$(document).find('.u-nav').css('position', 'relative');
			navPlacement = $(document).find('.u-nav').offset().top;
		});

		// when load of page is down, verificate if the navbar has to be sticky or not
		stickyOrNot(navPlacement);

		// when scroll, verificate when sticky navbar's breakpoint is triggered
		$(window).on('scroll resize orientationchange', function(){
			stickyOrNot(navPlacement);
		});
	};


});
});
