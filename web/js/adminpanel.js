$(document).ready(function(){

	/*=================================================================*/
	/*                          LIBRARY FILES                          */
	/*=================================================================*/

	$('.containerUploadFiles input[type="file"]').on('change', function(){
		if($(this).val()) {
			$(this).parents('form').find('button[type="submit"]').removeClass('u-none');
		} else {
			$(this).parents('form').find('button[type="submit"]').addClass('u-none');
		}
	});


	/*=================================================================*/
	/*                              TOOLS                              */
	/*=================================================================*/

	$('.JScopyOnClick').on('click', function(){
		window.prompt("Appuyez sur Ctrl+C pour copier dans le presse-papier", $(this).attr('value'));
	});

	$('.JSloaderOnClick:not(.btnDisabled)').on('click', function(){
		$(this).addClass('btnDisabled');
		$(this).append(' <i class="fa fa-circle-o-notch fa-spin"></i>');
	});

	$(document).on('click', '.JSdelOnClick', function(){
		$(this).fadeOut("fast", function() {
			$(this).remove();
		});
	});

	$('.JSchangeActionForm').on('click', function(e){
		var idForm = $(this).attr('data-formId');
		var actionForm = $(this).attr('data-formAction');
		$('form#'+idForm).attr('action', actionForm);
		$('form#'+idForm).submit();
	});

	$('.popWaveContainer').each(function(){
		var popWave = $(this).find('.popWave');

		if ($(this).width() > $(this).height()) {
			popWave.css('height', popWave.width());
		} else {
			popWave.css('width', popWave.height());
		}

		$('.popWaveTrigger').on('click', function(e) {
			$(this).parents('.popWaveContainer').find('.popWave').addClass('animate');
		});
	});

});
