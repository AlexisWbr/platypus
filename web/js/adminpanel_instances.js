$(document).ready(function(){

	// =======================================================
	// ==================NESTED SORTABLE TREE=================

	/*=== PAGES ===*/
	/*=============*/

	var ns = $('ul.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		listType: 'ul',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 20,
		tolerance: 'pointer',
		toleranceElement: '> div',
		isTree: true,
		expandOnHover: 700,
		startCollapsed: false
	});
	$('ul.sortable').css('height', $('ul.sortable').height());


	$('form#ajaxSendNestedTree').on('submit', function(e){
		// on remplit l'input avec l'ordre des pages
		$('form#ajaxSendNestedTree input#listPagesOrder').val($('ul.sortable').nestedSortable('serialize'));
		var serialized = $('form#ajaxSendNestedTree input#listPagesOrder').val();

		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data : serialized
		}).done(function(response){
			$('#popupContainer').append('<div class="popup popupColor1 JSdelOnClick">Changements enregistrés</div>');
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			$('#popupContainer').append('<div class="popup popupColor2 JSdelOnClick">Impossible d\'effectuer les changements, une erreur s\'est produite</div>');
		});
	});

	/*=== SLIDERS ===*/
	/*===============*/

	var ns = $('ul.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		listType: 'ul',
		items: 'li',
		maxLevels: 1,
		placeholder: 'placeholder',
		tolerance: 'pointer',
		toleranceElement: '> div',
		isTree: true
	});
	$('ul.sortable').css('height', $('ul.sortable').height());


	$('form#ajaxSendSliderPosition').on('submit', function(e){
		// on remplit l'input avec l'ordre des pages
		$('form#ajaxSendSliderPosition input#listSlidersOrder').val($('ul.sortable').nestedSortable('serialize'));
		var serialized = $('form#ajaxSendSliderPosition input#listSlidersOrder').val();

		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data : serialized
		}).done(function(response){
			$('#popupContainer').append('<div class="popup popupColor1 JSdelOnClick">Changements enregistrés</div>');
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			$('#popupContainer').append('<div class="popup popupColor2 JSdelOnClick">Impossible d\'effectuer les changements, une erreur s\'est produite</div>');
		});
	});

	/*=== SLIDES ORDER ===*/
	/*====================*/

	var ns = $('ul.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		listType: 'ul',
		items: 'li',
		opacity: .6,
		maxLevels: 1,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 20,
		tolerance: 'pointer',
		toleranceElement: '> div',
		isTree: true,
		expandOnHover: 700,
		startCollapsed: false,
		protectRoot: true
	});
	$('ul.sortable').css('height', $('ul.sortable').height());


	$('form#ajaxSendSlidePosition').on('submit', function(e){
		// on remplit l'input avec l'ordre des pages
		$('form#ajaxSendSlidePosition input#listSlideOrder').val($('ul.sortable').nestedSortable('serialize'));
		var serialized = $('form#ajaxSendSlidePosition input#listSlideOrder').val();

		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data : serialized
		}).done(function(response){
			$('#popupContainer').append('<div class="popup popupColor1 JSdelOnClick">Changements enregistrés</div>');
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			$('#popupContainer').append('<div class="popup popupColor2 JSdelOnClick">Impossible d\'effectuer les changements, une erreur s\'est produite</div>');
		});
	});


	// =======================================================
	// ====================DATE TIME PICKER===================

	$.datetimepicker.setLocale('fr');
	$('.datetimepicker').datetimepicker({
		// format:'d.M.Y à H:i'
	});

});
